#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_CONTROLS.h"
#include "DC_LED.h"
#include "DC_LIB_BMW.h"
//#include "DC_CAN_LIBRARIES.h"

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80
#define BIT8 0x0100
#define BIT9 0x0200
#define BIT10 0x0400
#define BIT11 0x0800
#define BIT12 0x1000
#define BIT13 0x2000
#define BIT14 0x4000
#define BIT15 0x8000

extern uint8_t CANActivity;
extern uint8_t uBaudSelector;
extern uint8_t SecondCANActive;


extern enum Module_State
{
	STARTUP,
	NOT_LOCKED,
	BAUD_LOCKED,	
	LOCKED,
	SLEEP,
	ERROR
}State, Previous_State;

extern uint32_t Odometer;	//Bytes3-5
/*
KMs
*/
extern uint8_t RPM;				//Byte6
/*
Data = RPM / 50
*/
extern uint8_t RoadSpeed;		//Byte7
/*
KpH
*/
extern uint16_t FuelLevel; 	//Bytes8-9
/*
Data = Litres x 10 
*/
extern uint16_t FuelFlowCount;//Bytes10-11
extern uint8_t	Ignition;		//Byte12
/*
BIT0 - Key In
BIT1 - Accessory Position
BIT2 - Full Ignition
BIT3 - Cranking
BIT4-7 Unused
*/
extern uint8_t Accelerator;		//Byte13
/*
Percentage of full travel
*/
extern uint8_t Brake;			//Byte14
/*
Percetnrage of full travel
*/
extern uint16_t Lights;		//Byte15-16
/*
BIT0 - Sidelights
BIT1 - Running Lights
BIT2 - Dipped Beam
BIT3 - MAin Beam
BIT4 - Front Fogs
BIT5 - Rear Fogs
BIT6 - Brake Lights
BIT7 - Reverse Lights
BIT8 - Left Indicator
BIT9 - Right Indicator
BIT10 - Hazard Lighs
*/
extern uint8_t Wipers;			//Byte17
/*
BIT0-2 - 0(Off), 1(Int1), 2(Int2), 3(Speed1), 4(Speed2), 5(Auto)
BIT3 - Manual
BIT4 - Rear Wipers
BIT5 - Front Wash
BIT6 - Rear Wash
*/
extern uint8_t CoolantTemp;		//Byte18
/*
Data = Temp(C) + 40
*/
extern uint8_t AmbientTemp;
/*
Data = Temp(C) + 40
*/
extern uint16_t SteeringWheel;
/*
BIT0-14 - Degrees from Centre
BIT15 - Direction 1-Right
*/
extern uint8_t Controls;	
/*
BIT0 - Hanbrake
BIT1 - Clutch
BIT2 - Horn
BIT3 - ECO Mode
BIT4 - Auto Start/Stop Mode
*/
extern uint8_t AutoBox;			//Byte23
/*
BIT0 - Park
BIT1 - Reverse
BIT2 - Neutral
BIT3 - Drive
BIT4 - Manual
BIT5 - Gear Up
BIT6 - Gear Down
BIT7 - 
*/
extern uint8_t ManualBox;		//Byte24
/*
BIT0-2 - Gear (0 Neutral - 6th gear 7 = reverse 
BIT3-5 - Gear Advised 
BIT6 - Shift Up Advice
BIT7 - Shift Down Advice
*/
extern uint8_t Doors;			//Byte25
/*
BIT0 - OSF Door Open
BIT1 - NSF Door Open
BIT2 - OSR Door Open
BIT3 - NSR Door Open
BIT4 - Tailgate/Boot/Rear Door
BIT5 - Bonnet Open
BIT6 - 
BIT7 - 
*/
extern uint8_t SeatBelts;		//Byte26
/*
BIT0 - OSF Belt Used
BIT1 - NSF Belt Used
BIT2 - OSR Belt USed
BIT3 - NSR Belt Used
BIT4 - Raar Middle Used
BIT5 - 
BIT6 - 
BIT7 - 
*/
extern uint8_t Occupancy;		//Byte27
/*
BIT0 - NSF Seat Occupied
BIT1 - OSR Seat Occupied
BIT2 - NSR Seat Occupied
BIT3 - Rear Middle Occupied
*/
extern uint8_t MILLights;		//Byte28
/*
BIT0 - Engine MIL
BIT1 - Oil Level
BIT2 - Brake System
BIT3 - Oil Temp
BIT4 - Low Fuel
BIT5 - Coolant Temp
BIT6 - Oil Warning
BIT7 - Emissions MIL
*/

extern uint8_t VehicleType;		

extern uint8_t VIN[17];			//Bytes6-22
//Checksum byte 23
//End Packet byte 24

extern uint32_t FILTER0_VOL;
extern uint32_t FILTER1_VOL;
extern uint32_t FILTER2_VOL;
extern uint32_t FILTER3_VOL;
extern uint32_t FILTER4_VOL;
extern uint32_t FILTER5_VOL;
extern uint32_t FILTER6_VOL;
extern uint32_t FILTER7_VOL;
extern uint32_t FILTER8_VOL;
extern uint32_t FILTER9_VOL;
extern uint32_t FILTER10_VOL;
extern uint32_t FILTER11_VOL;
extern uint32_t FILTER12_VOL;
extern uint32_t FILTER13_VOL;
extern uint32_t FILTER14_VOL;
extern uint32_t FILTER15_VOL;

extern BOOL	FILTER0_EN;
extern BOOL	FILTER1_EN;
extern BOOL	FILTER2_EN;
extern BOOL	FILTER3_EN;
extern BOOL	FILTER4_EN;
extern BOOL	FILTER5_EN;
extern BOOL	FILTER6_EN;
extern BOOL	FILTER7_EN;
extern BOOL	FILTER8_EN;
extern BOOL	FILTER9_EN;
extern BOOL	FILTER10_EN;
extern BOOL	FILTER11_EN;
extern BOOL	FILTER12_EN;
extern BOOL	FILTER13_EN;
extern BOOL	FILTER14_EN;
extern BOOL	FILTER15_EN;

extern uint32_t FILTER2_0_VOL;
extern uint32_t FILTER2_1_VOL;
extern uint32_t FILTER2_2_VOL;
extern uint32_t FILTER2_3_VOL;
extern uint32_t FILTER2_4_VOL;
extern uint32_t FILTER2_5_VOL;
extern uint32_t FILTER2_6_VOL;
extern uint32_t FILTER2_7_VOL;
extern uint32_t FILTER2_8_VOL;
extern uint32_t FILTER2_9_VOL;
extern uint32_t FILTER2_10_VOL;
extern uint32_t FILTER2_11_VOL;
extern uint32_t FILTER2_12_VOL;
extern uint32_t FILTER2_13_VOL;
extern uint32_t FILTER2_14_VOL;
extern uint32_t FILTER2_15_VOL;

extern BOOL	FILTER2_0_EN;
extern BOOL	FILTER2_1_EN;
extern BOOL	FILTER2_2_EN;
extern BOOL	FILTER2_3_EN;
extern BOOL	FILTER2_4_EN;
extern BOOL	FILTER2_5_EN;
extern BOOL	FILTER2_6_EN;
extern BOOL	FILTER2_7_EN;
extern BOOL	FILTER2_8_EN;
extern BOOL	FILTER2_9_EN;
extern BOOL	FILTER2_10_EN;
extern BOOL	FILTER2_11_EN;
extern BOOL	FILTER2_12_EN;
extern BOOL	FILTER2_13_EN;
extern BOOL	FILTER2_14_EN;
extern BOOL	FILTER2_15_EN;

extern uint8_t Hold_Counter;

extern CANRxMessageBuffer * message;
uint32_t SID;

void Check_BMWF15Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == BMW_CAR_1_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == BMW_CAR_1_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == BMW_CAR_1_CAN1_FILTER4)
		Check[2] = 1;
	if (SID == BMW_CAR_1_CAN1_FILTER7)
		Check[3] = 1;
	if (SID == BMW_CAR_1_CAN1_FILTER8)
		Check[4] = 1;
	if (SID == BMW_CAR_1_CAN1_FILTER9)
		Check[5] = 1;
	if (SID == BMW_CAR_1_CAN1_FILTER10)
		Check[6] = 1;
	if (SID == BMW_CAR_1_CAN1_FILTER12)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = BMW_CAR_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = BMW_CAR_1_CAN1_FILTER0;
		FILTER1_VOL = BMW_CAR_1_CAN1_FILTER1;
		FILTER2_VOL = BMW_CAR_1_CAN1_FILTER2;
		FILTER3_VOL = BMW_CAR_1_CAN1_FILTER3;
		FILTER4_VOL = BMW_CAR_1_CAN1_FILTER4;
		FILTER5_VOL = BMW_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = BMW_CAR_1_CAN1_FILTER6;
		FILTER7_VOL = BMW_CAR_1_CAN1_FILTER7;
		FILTER8_VOL = BMW_CAR_1_CAN1_FILTER8;
		FILTER9_VOL = BMW_CAR_1_CAN1_FILTER9;
		FILTER10_VOL = BMW_CAR_1_CAN1_FILTER10;
		FILTER11_VOL = BMW_CAR_1_CAN1_FILTER11;
		FILTER12_VOL = BMW_CAR_1_CAN1_FILTER12;
		FILTER13_VOL = BMW_CAR_1_CAN1_FILTER13;
		FILTER14_VOL = BMW_CAR_1_CAN1_FILTER14;
		FILTER15_VOL = BMW_CAR_1_CAN1_FILTER15;

		FILTER0_EN = BMW_CAR_1_CAN1_FILTER0_EN;
		FILTER1_EN = BMW_CAR_1_CAN1_FILTER1_EN;
		FILTER2_EN = BMW_CAR_1_CAN1_FILTER2_EN;
		FILTER3_EN = BMW_CAR_1_CAN1_FILTER3_EN;
		FILTER4_EN = BMW_CAR_1_CAN1_FILTER4_EN;
		FILTER5_EN = BMW_CAR_1_CAN1_FILTER5_EN;
		FILTER6_EN = BMW_CAR_1_CAN1_FILTER6_EN;
		FILTER7_EN = BMW_CAR_1_CAN1_FILTER7_EN;
		FILTER8_EN = BMW_CAR_1_CAN1_FILTER8_EN;
		FILTER9_EN = BMW_CAR_1_CAN1_FILTER9_EN;
		FILTER10_EN = BMW_CAR_1_CAN1_FILTER10_EN;
		FILTER11_EN = BMW_CAR_1_CAN1_FILTER11_EN;
		FILTER12_EN = BMW_CAR_1_CAN1_FILTER12_EN;
		FILTER13_EN = BMW_CAR_1_CAN1_FILTER13_EN;
		FILTER14_EN = BMW_CAR_1_CAN1_FILTER14_EN;
		FILTER15_EN = BMW_CAR_1_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_BMWF45Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == BMW_CAR_3_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == BMW_CAR_3_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == BMW_CAR_3_CAN1_FILTER4)
		Check[2] = 1;
	if (SID == BMW_CAR_3_CAN1_FILTER7)
		Check[3] = 1;
	if (SID == BMW_CAR_3_CAN1_FILTER8)
		Check[4] = 1;
	if (SID == BMW_CAR_3_CAN1_FILTER9)
		Check[5] = 1;
	if (SID == BMW_CAR_3_CAN1_FILTER10)
		Check[6] = 1;
	if (SID == BMW_CAR_3_CAN1_FILTER12)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = BMW_CAR_3_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = BMW_CAR_3_CAN1_FILTER0;
		FILTER1_VOL = BMW_CAR_3_CAN1_FILTER1;
		FILTER2_VOL = BMW_CAR_3_CAN1_FILTER2;
		FILTER3_VOL = BMW_CAR_3_CAN1_FILTER3;
		FILTER4_VOL = BMW_CAR_3_CAN1_FILTER4;
		FILTER5_VOL = BMW_CAR_3_CAN1_FILTER5;
		FILTER6_VOL = BMW_CAR_3_CAN1_FILTER6;
		FILTER7_VOL = BMW_CAR_3_CAN1_FILTER7;
		FILTER8_VOL = BMW_CAR_3_CAN1_FILTER8;
		FILTER9_VOL = BMW_CAR_3_CAN1_FILTER9;
		FILTER10_VOL = BMW_CAR_3_CAN1_FILTER10;
		FILTER11_VOL = BMW_CAR_3_CAN1_FILTER11;
		FILTER12_VOL = BMW_CAR_3_CAN1_FILTER12;
		FILTER13_VOL = BMW_CAR_3_CAN1_FILTER13;
		FILTER14_VOL = BMW_CAR_3_CAN1_FILTER14;
		FILTER15_VOL = BMW_CAR_3_CAN1_FILTER15;

		FILTER0_EN = BMW_CAR_3_CAN1_FILTER0_EN;
		FILTER1_EN = BMW_CAR_3_CAN1_FILTER1_EN;
		FILTER2_EN = BMW_CAR_3_CAN1_FILTER2_EN;
		FILTER3_EN = BMW_CAR_3_CAN1_FILTER3_EN;
		FILTER4_EN = BMW_CAR_3_CAN1_FILTER4_EN;
		FILTER5_EN = BMW_CAR_3_CAN1_FILTER5_EN;
		FILTER6_EN = BMW_CAR_3_CAN1_FILTER6_EN;
		FILTER7_EN = BMW_CAR_3_CAN1_FILTER7_EN;
		FILTER8_EN = BMW_CAR_3_CAN1_FILTER8_EN;
		FILTER9_EN = BMW_CAR_3_CAN1_FILTER9_EN;
		FILTER10_EN = BMW_CAR_3_CAN1_FILTER10_EN;
		FILTER11_EN = BMW_CAR_3_CAN1_FILTER11_EN;
		FILTER12_EN = BMW_CAR_3_CAN1_FILTER12_EN;
		FILTER13_EN = BMW_CAR_3_CAN1_FILTER13_EN;
		FILTER14_EN = BMW_CAR_3_CAN1_FILTER14_EN;
		FILTER15_EN = BMW_CAR_3_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_BMW5erGType(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == BMW_CAR_4_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == BMW_CAR_4_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == BMW_CAR_4_CAN1_FILTER4)
		Check[2] = 1;
	if (SID == BMW_CAR_4_CAN1_FILTER7)
		Check[3] = 1;
	if (SID == BMW_CAR_4_CAN1_FILTER8)
		Check[4] = 1;
	if (SID == BMW_CAR_4_CAN1_FILTER9)
		Check[5] = 1;
	if (SID == BMW_CAR_4_CAN1_FILTER10)
		Check[6] = 1;
	if (SID == BMW_CAR_4_CAN1_FILTER12)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = BMW_CAR_4_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = BMW_CAR_4_CAN1_FILTER0;
		FILTER1_VOL = BMW_CAR_4_CAN1_FILTER1;
		FILTER2_VOL = BMW_CAR_4_CAN1_FILTER2;
		FILTER3_VOL = BMW_CAR_4_CAN1_FILTER3;
		FILTER4_VOL = BMW_CAR_4_CAN1_FILTER4;
		FILTER5_VOL = BMW_CAR_4_CAN1_FILTER5;
		FILTER6_VOL = BMW_CAR_4_CAN1_FILTER6;
		FILTER7_VOL = BMW_CAR_4_CAN1_FILTER7;
		FILTER8_VOL = BMW_CAR_4_CAN1_FILTER8;
		FILTER9_VOL = BMW_CAR_4_CAN1_FILTER9;
		FILTER10_VOL = BMW_CAR_4_CAN1_FILTER10;
		FILTER11_VOL = BMW_CAR_4_CAN1_FILTER11;
		FILTER12_VOL = BMW_CAR_4_CAN1_FILTER12;
		FILTER13_VOL = BMW_CAR_4_CAN1_FILTER13;
		FILTER14_VOL = BMW_CAR_4_CAN1_FILTER14;
		FILTER15_VOL = BMW_CAR_4_CAN1_FILTER15;

		FILTER0_EN = BMW_CAR_4_CAN1_FILTER0_EN;
		FILTER1_EN = BMW_CAR_4_CAN1_FILTER1_EN;
		FILTER2_EN = BMW_CAR_4_CAN1_FILTER2_EN;
		FILTER3_EN = BMW_CAR_4_CAN1_FILTER3_EN;
		FILTER4_EN = BMW_CAR_4_CAN1_FILTER4_EN;
		FILTER5_EN = BMW_CAR_4_CAN1_FILTER5_EN;
		FILTER6_EN = BMW_CAR_4_CAN1_FILTER6_EN;
		FILTER7_EN = BMW_CAR_4_CAN1_FILTER7_EN;
		FILTER8_EN = BMW_CAR_4_CAN1_FILTER8_EN;
		FILTER9_EN = BMW_CAR_4_CAN1_FILTER9_EN;
		FILTER10_EN = BMW_CAR_4_CAN1_FILTER10_EN;
		FILTER11_EN = BMW_CAR_4_CAN1_FILTER11_EN;
		FILTER12_EN = BMW_CAR_4_CAN1_FILTER12_EN;
		FILTER13_EN = BMW_CAR_4_CAN1_FILTER13_EN;
		FILTER14_EN = BMW_CAR_4_CAN1_FILTER14_EN;
		FILTER15_EN = BMW_CAR_4_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}


void Check_BMWE84Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == BMW_CAR_2_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == BMW_CAR_2_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == BMW_CAR_2_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == BMW_CAR_2_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == BMW_CAR_2_CAN1_FILTER4)
		Check[4] = 1;
	if (SID == BMW_CAR_2_CAN1_FILTER10)
		Check[5] = 1;
	if (SID == BMW_CAR_2_CAN1_FILTER8)
		Check[6] = 1;
	if (SID == BMW_CAR_2_CAN1_FILTER7)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = BMW_CAR_2_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = BMW_CAR_2_CAN1_FILTER0;
		FILTER1_VOL = BMW_CAR_2_CAN1_FILTER1;
		FILTER2_VOL = BMW_CAR_2_CAN1_FILTER2;
		FILTER3_VOL = BMW_CAR_2_CAN1_FILTER3;
		FILTER4_VOL = BMW_CAR_2_CAN1_FILTER4;
		FILTER5_VOL = BMW_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = BMW_CAR_2_CAN1_FILTER6;
		FILTER7_VOL = BMW_CAR_2_CAN1_FILTER7;
		FILTER8_VOL = BMW_CAR_2_CAN1_FILTER8;
		FILTER9_VOL = BMW_CAR_2_CAN1_FILTER9;
		FILTER10_VOL = BMW_CAR_2_CAN1_FILTER10;
		FILTER11_VOL = BMW_CAR_2_CAN1_FILTER11;
		FILTER12_VOL = BMW_CAR_2_CAN1_FILTER12;
		FILTER13_VOL = BMW_CAR_2_CAN1_FILTER13;
		FILTER14_VOL = BMW_CAR_2_CAN1_FILTER14;
		FILTER15_VOL = BMW_CAR_2_CAN1_FILTER15;

		FILTER0_EN = BMW_CAR_2_CAN1_FILTER0_EN;
		FILTER1_EN = BMW_CAR_2_CAN1_FILTER1_EN;
		FILTER2_EN = BMW_CAR_2_CAN1_FILTER2_EN;
		FILTER3_EN = BMW_CAR_2_CAN1_FILTER3_EN;
		FILTER4_EN = BMW_CAR_2_CAN1_FILTER4_EN;
		FILTER5_EN = BMW_CAR_2_CAN1_FILTER5_EN;
		FILTER6_EN = BMW_CAR_2_CAN1_FILTER6_EN;
		FILTER7_EN = BMW_CAR_2_CAN1_FILTER7_EN;
		FILTER8_EN = BMW_CAR_2_CAN1_FILTER8_EN;
		FILTER9_EN = BMW_CAR_2_CAN1_FILTER9_EN;
		FILTER10_EN = BMW_CAR_2_CAN1_FILTER10_EN;
		FILTER11_EN = BMW_CAR_2_CAN1_FILTER11_EN;
		FILTER12_EN = BMW_CAR_2_CAN1_FILTER12_EN;
		FILTER13_EN = BMW_CAR_2_CAN1_FILTER13_EN;
		FILTER14_EN = BMW_CAR_2_CAN1_FILTER14_EN;
		FILTER15_EN = BMW_CAR_2_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

//Also cover 2 Series F45 type with diff steering wheel message
//also used for G platform
void DecodeDataBMWF15Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
//	static uint8_t FuelFlow_Last;
//	static uint8_t LeftCtr;
//	static uint8_t RightCtr;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

//Template - CheckSet8(ByteNum, BITxx, Controls, BITxx);
//Template - CheckSet8Inv
	//RPM
	if (SID == 0x0A5)
	{
		Tempu16 = message->data[6] & 0x7F;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[5];
		//RPM Data is 4 x  actual value
		if (Tempu16 != 0x7FFF)
		{
			Tempu16 = Tempu16 / 4;

			//Holds RPM data - no scaling yet.
			Tempu16 = (Tempu16 + 25) / 50;	

			RPM = Tempu16 & 0xFF;
		}
		else RPM = 0x00;
	}

	//Accelerator 
	if (SID == 0x0D9)
	{
			Tempu16 = (message->data[3]) & 0xF;
			Tempu16 = Tempu16 << 8;
			Tempu16 |= message->data[4];


			TempDouble = (double)Tempu16 / 40.73;
		
			Tempu8 = (uint8_t)(TempDouble + 0.5);		

			Accelerator = Tempu8;
	}


	if (SID == 0x3F9)
	{
		Tempu8 = message->data[4];
//Data is temp + 48 (we send temp + 8)
		if (Tempu8 == 0xFF) CoolantTemp = 0xFF;
		else
			CoolantTemp = Tempu8 - 8;
	}

	//Brake Pedal
	if (SID == 0x0EF)
	{
		Tempu8 = message->data[3];

		Tempu8 = 0x7D - Tempu8;

		TempDouble = (double)Tempu8 * 1.33;

		Brake = (uint8_t)TempDouble;

	}

	//Speed
	if (SID == 0x1A1)
	{
		Tempu16 = message->data[3] & 0xFF;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[2];
	
		if (Tempu16 != 0xFFFF)	RoadSpeed = Tempu16 / 64;
	}
	//Odometer
	//Fuel
	if (SID == 0x330)
	{
			Tempu32 = message->data[2];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[1];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[0];
		
			if (Tempu32 != 0xFFFFFF) Odometer = Tempu32;

			Tempu8 = message->data[3];

			if ((Ignition & BIT2) == BIT2)
			{
				FuelLevel = Tempu8 * 10;

				if (FuelLevel < 80)
					MILLights = MILLights | BIT4;
				else
					MILLights = MILLights & ~BIT4;	

				MILLights = MILLights & 0b00010000;
			}
	}

//Wipers
	if (SID == 0x2A6)
	{

		
		Wipers = 0;
		
		switch (message->data[0] & 0x03)
		{
			case 1:
				Wipers = Wipers | 0x05;
				break;
			case 2:
				Wipers = Wipers | 0x03;
				break;
			case 3:
				Wipers = Wipers | 0x04;
				break;
		}
		//Single Wiper
		CheckSet8(DATA[0], BIT3, &Wipers, BIT3);
		//Front Wash
		CheckSet8(DATA[0], BIT4, &Wipers, BIT5);
		//Rear Wiper
		CheckSet8(DATA[0], BIT6, &Wipers, BIT4);
		//Rear Wash
		CheckSet8(DATA[0], BIT7, &Wipers, BIT6);		

	}
	//Ambient Temp
	if (SID == 0x2CA)
	{
		Tempu8 = DATA[0];

		if (Tempu8 != 0xFF)	AmbientTemp = Tempu8 / 2;
	}

	//Ignition
	if ((SID == 0x12F)&&(VehicleType ==  BMW_CAR_4_TYPE))
	{
		
		Tempu8 = DATA[2]&0xF;
		
		if ((Tempu8 > 0x6) && (Tempu8 < 0xF)) Ignition |= BIT2;
			else Ignition &= ~BIT2;
		if ((Tempu8 == 0x09)) Ignition |= BIT3;	
			else Ignition &= ~BIT3;
	
	}
	else if (SID == 0x12F)
	{
		
		Tempu8 = DATA[2]&0xF;
		
		if ((Tempu8 > 0x9) && (Tempu8 < 0xF)) Ignition |= BIT2;
			else Ignition &= ~BIT2;
		if ((Tempu8 > 0xB) && (Tempu8 < 0xF)) Ignition |= BIT3;	
			else Ignition &= ~BIT3;
	
	}
	//G PLatform only
	if (SID == 0x2B5)
	{
		CheckSet8(DATA[3], BIT0, &Controls, BIT0);
	}
		
	//Indicator
	if (SID == 0x1F6)
	{
		//Left
		CheckSet16(DATA[0], BIT4, &Lights, BIT8);
		//Right
		CheckSet16(DATA[0], BIT5, &Lights, BIT9);

	}

	//Lights
	if (SID == 0x21A)
	{
		//Main Beam
		CheckSet16(DATA[0], BIT1, &Lights, BIT3);
		//Rear Fogs
		CheckSet16(DATA[0], BIT6, &Lights, BIT5);
		//Front Fogs
		CheckSet16(DATA[0], BIT5, &Lights, BIT4);
		//Reverse Lights
		CheckSet16(DATA[1], BIT0, &Lights, BIT7);
		//Sidelights
		CheckSet16(DATA[0], BIT2, &Lights, BIT0);
		//Dipped Beam
		CheckSet16(DATA[0], BIT0, &Lights, BIT2);
		//Brake Lights
		CheckSet16(DATA[0], BIT7, &Lights, BIT6);

	}
	//Doors
	if (SID == 0x2FC)
	{
		//OSF
		CheckSet8(DATA[1], BIT0, &Doors, BIT0);
		//NSF
		CheckSet8(DATA[1], BIT2, &Doors, BIT1);
		//NSR
		CheckSet8(DATA[1], BIT6, &Doors, BIT3);
		//OSR
		CheckSet8(DATA[1], BIT4, &Doors, BIT2);		
		//Tailgate
		CheckSet8(DATA[2], BIT0, &Doors, BIT4);
	}



	//Steering Wheel
	if ((SID == 0x18D)&&(VehicleType == BMW_CAR_1_TYPE))
	{
		Tempu16 = message->data[1] & 0x7F;
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[0];

		//0x47FE Centre
		//0x478F Right
		//0x4870 Left

		if (Tempu16 < 0x4900)
		{
			if (Tempu16 > 0x47FE)
			{
				TempDouble = (double)Tempu16 - 0x47FE;
				TempDouble = TempDouble;

				SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
				SteeringWheel = SteeringWheel & ~BIT15;
			}
			else if (Tempu16 > 0x478E)
			{
				TempDouble = 0x47FE - (double)Tempu16;
				TempDouble = TempDouble;

				SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
				SteeringWheel = SteeringWheel | BIT15;
			}

		}	

	}

	//Steering Wheel
	if ((SID == 0x301)&&((VehicleType == BMW_CAR_3_TYPE)||(VehicleType == BMW_CAR_4_TYPE)))
	{
		Tempu16 = message->data[3] & 0xFF;
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[2];

		//0x8000 Centre
		//0x8FE5 Right 180
		//0x6FDB Left 180


		if (Tempu16 > 0x8000)
		{
			TempDouble = (double)Tempu16 - 0x8000;
			TempDouble = TempDouble/22.75;

			SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
			SteeringWheel = SteeringWheel & ~BIT15;
		}
		else
		{
			TempDouble = 0x8000 - (double)Tempu16;
			TempDouble = TempDouble/22.75;

			SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
			SteeringWheel = SteeringWheel | BIT15;
		}

			

	}


	//Handbrake
	if (SID == 0x1B3)
	{
		//Handbrake
		//CheckSet8(DATA[6], BIT1, &Controls, BIT0);
		
		Tempu8 = DATA[6] & 0x03;
		if (Tempu8 == 0b10) Controls = Controls | BIT0;
		else if (Tempu8 == 0) Controls = Controls & ~BIT0;
		
		//Engine Light
		Tempu8 = DATA[4] & 0x30;
		if ((Tempu8 == 0x10) || (Tempu8 == 0x20))
			MILLights |= BIT0;
		else MILLights &= ~BIT0;

		//Airbag Light
		Tempu8 = DATA[4] & 0x03;
		if ((Tempu8 == 0x01) || (Tempu8 == 0x02))
			MILLights |= BIT6;
		else MILLights &= ~BIT6;

		//Seatbelt Light
		Tempu8 = DATA[4] & 0x04;
		if (Tempu8 == 0x04)
			MILLights |= BIT5;
		else MILLights &= ~BIT5;

	}
	//Horn
	if (SID == 0x1D6)
	{
		CheckSet8(DATA[1], BIT2, &Controls, BIT2);
	}

	//Autobox
	if (SID == 0x3F9)
	{

		AutoBox = 0;
		
		Tempu8 = DATA[6] & 0xF;

		switch (Tempu8)
		{
			case 1:
				AutoBox = AutoBox | BIT2;
				break;
			case 2:
				AutoBox = AutoBox | BIT1;
				break;
			case 3:
				AutoBox = AutoBox | BIT0;
				break;
		}
	
	
		if ((Tempu8>4)&&(Tempu8<0xF))
		{
			Tempu8 -= 4;
			Tempu8 = Tempu8 <<3;
			AutoBox = Tempu8 & 0b01111000;
		}

	}

	//VIN
	if (SID == 0x380)
	{

				
					VIN[0] = 'x';
					VIN[1] = 'x';
					VIN[2] = 'x';
					VIN[3] = 'x';
					VIN[4] = 'x';
					VIN[5] = 'x';
					VIN[6] = 'x';
					VIN[7] = 'x';
					VIN[8] = 'x';					
					VIN[9] = 'x';
					VIN[10] = message->data[0];
					VIN[11] = message->data[1];
					VIN[12] = message->data[2];
					VIN[13] = message->data[3];
					VIN[14] = message->data[4];
					VIN[15] = message->data[5];
					VIN[16] = message->data[6];
		
	}



	CANActivity = 1;
	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}

void DecodeDataBMWE84Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
//	static uint8_t FuelFlow_Last;
//	static uint8_t LeftCtr;
//	static uint8_t RightCtr;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

//Template - CheckSet8(ByteNum, BITxx, Controls, BITxx);
//Template - CheckSet8Inv
	//RPM
	if (SID == 0x0AA)
	{
		Tempu16 = message->data[5] & 0x7F;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[4];
		//RPM Data is 4 x  actual value
		if (Tempu16 != 0x7FFF)
		{
			Tempu16 = Tempu16 / 4;

			//Holds RPM data - no scaling yet.
			Tempu16 = (Tempu16 + 25) / 50;	

			RPM = Tempu16 & 0xFF;
		}
		else RPM = 0x00;

		Tempu8 = (message->data[3]);

		TempDouble = (double)Tempu8 / 2.54;
		
		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Accelerator = Tempu8;
	}
	
	//Clutch Pedal
	if (SID == 0x0A8)
	{
		CheckSet8(DATA[5], BIT0, &Controls, BIT1);
	}

	//Brake Pedal
	if (SID == 0x19E)
	{
		Tempu8 = message->data[6];

		//Tempu8 = 0x7D - Tempu8;

		TempDouble = (double)Tempu8 / 1.6;

		Brake = (uint8_t)TempDouble;

	}

	//Speed & Handbrake
	if (SID == 0x1B4)
	{
		Tempu16 = message->data[1] & 0x0F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[0];
	
		if (Tempu16 != 0xFFFF)	RoadSpeed = Tempu16 / 10;

		//Handbrake
		CheckSet8(DATA[6], BIT1, &Controls, BIT0);
	}
	//Odometer
	//Fuel
	if (SID == 0x330)
	{
			Tempu32 = message->data[2];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[1];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[0];
		
			if (Tempu32 != 0xFFFFFF) Odometer = Tempu32;

			Tempu8 = message->data[3];

			if ((Ignition & BIT2) == BIT2)
			{
				FuelLevel = Tempu8 * 10;

				if (FuelLevel < 80)
					MILLights = MILLights | BIT4;
				else
					MILLights = MILLights & ~BIT4;	

				MILLights = MILLights & 0b00010000;
			}
	}

//Wipers
	if (SID == 0x2A6)
	{

		
		Wipers = 0;
		
		switch (message->data[0] & 0x03)
		{
			case 1:
				Wipers = Wipers | 0x05;
				break;
			case 2:
				Wipers = Wipers | 0x03;
				break;
			case 3:
				Wipers = Wipers | 0x04;
				break;
		}
		//Single Wiper
		CheckSet8(DATA[0], BIT3, &Wipers, BIT3);
		//Front Wash
		CheckSet8(DATA[0], BIT4, &Wipers, BIT5);
		//Rear Wiper
		CheckSet8(DATA[0], BIT6, &Wipers, BIT4);
		//Rear Wash
		CheckSet8(DATA[0], BIT7, &Wipers, BIT6);		

	}
	//Ambient Temp
	if (SID == 0x2CA)
	{
		Tempu8 = DATA[0];

		if (Tempu8 != 0xFF)	AmbientTemp = Tempu8 / 2;
	}

	//Ignition
	if (SID == 0x130)
	{
		
		//CheckSet8(DATA[0], BIT
		Tempu8 = DATA[0]&0xC;
		if (Tempu8 == 0x4) Ignition |= BIT2;
			else Ignition &= ~BIT2;

		Tempu8 = DATA[0]&0x30;
		if (Tempu8 == 0x10) Ignition |= BIT3;
			else Ignition &= ~BIT3;
	
	}
		
	//Indicator
	if (SID == 0x1F6)
	{
		//Left
		CheckSet16(DATA[0], BIT4, &Lights, BIT8);
		//Right
		CheckSet16(DATA[0], BIT5, &Lights, BIT9);

	}

	//Lights
	if (SID == 0x21A)
	{
		//Main Beam
		CheckSet16(DATA[0], BIT1, &Lights, BIT3);
		//Rear Fogs
		CheckSet16(DATA[0], BIT6, &Lights, BIT5);
		//Front Fogs
		CheckSet16(DATA[0], BIT5, &Lights, BIT4);
		//Reverse Lights
		CheckSet16(DATA[1], BIT0, &Lights, BIT7);
		//Sidelights
		CheckSet16(DATA[0], BIT2, &Lights, BIT0);
		//Dipped Beam
		CheckSet16(DATA[0], BIT0, &Lights, BIT2);
		//Brake Lights
		CheckSet16(DATA[0], BIT7, &Lights, BIT6);

	}
	//Doors
	if (SID == 0x2FC)
	{
		//OSF
		CheckSet8(DATA[1], BIT0, &Doors, BIT0);
		//NSF
		CheckSet8(DATA[1], BIT2, &Doors, BIT1);
		//NSR
		CheckSet8(DATA[1], BIT6, &Doors, BIT3);
		//OSR
		CheckSet8(DATA[1], BIT4, &Doors, BIT2);		
		//Tailgate
		CheckSet8(DATA[2], BIT0, &Doors, BIT4);
	}



	//Steering Wheel
	if (SID == 0x0C4)
	{
		Tempu16 = message->data[1] & 0xFF;
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[0];

		//0x47FE Centre
		//0x478F Right
		//0x4870 Left

		//If right
		if ((Tempu16 & 0x8000) == 0x8000)
		{
			TempDouble = 0xFFFF - (double)Tempu16;
			TempDouble = TempDouble / 22.61;

			SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
			SteeringWheel = SteeringWheel | BIT15;
		}
		else
		{
			TempDouble = (double)Tempu16 / 22.61;
			//TempDouble = TempDouble;

			SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
			SteeringWheel = SteeringWheel & ~BIT15;
		}
	

	}

	if (SID == 0x1D0)
	{
		Tempu8 = message->data[0];
//Data is temp + 48 (we send temp + 8)
		if (Tempu8 == 0xFF) CoolantTemp = 0xFF;
		else
			CoolantTemp = Tempu8 - 8;
	}

	//Seatbelt
	if (SID == 0x581)
	{		

		//Seatbelt Light
		CheckSet8(DATA[3], BIT0, &MILLights, BIT5);

	}

/*
	//Horn
	if (SID == 0x1D6)
	{
		CheckSet8(DATA[1], BIT2, &Controls, BIT2);
	}
*/
/*
	//Autobox
	if (SID == 0x3F9)
	{

		AutoBox = 0;
		
		Tempu8 = DATA[6] & 0xF;

		switch (Tempu8)
		{
			case 1:
				AutoBox = AutoBox | BIT2;
				break;
			case 2:
				AutoBox = AutoBox | BIT1;
				break;
			case 3:
				AutoBox = AutoBox | BIT0;
				break;
		}
	
	
		if ((Tempu8>4)&&(Tempu8<0xF))
		{
			Tempu8 -= 4;
			Tempu8 = Tempu8 <<3;
			AutoBox = Tempu8 & 0b01111000;
		}

	}
*/
	//VIN
	if (SID == 0x380)
	{

				
					VIN[0] = 'x';
					VIN[1] = 'x';
					VIN[2] = 'x';
					VIN[3] = 'x';
					VIN[4] = 'x';
					VIN[5] = 'x';
					VIN[6] = 'x';
					VIN[7] = 'x';
					VIN[8] = 'x';					
					VIN[9] = 'x';
					VIN[10] = message->data[0];
					VIN[11] = message->data[1];
					VIN[12] = message->data[2];
					VIN[13] = message->data[3];
					VIN[14] = message->data[4];
					VIN[15] = message->data[5];
					VIN[16] = message->data[6];
		
	}



	CANActivity = 1;
	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}
