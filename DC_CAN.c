#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_LED.h"
#include "DC_CAN.h"

/*********************************************
*	This file contains all the code required
*	to initialise and interact with the CAN 
*	hardware on the board
*
*********************************************/

//Set Baud Rates
//Set operation mode
//Set interrupts
//Set filters (non used here but required for future)

BYTE CAN1MessageFifoArea[2 * 8 * 16];
BYTE CAN2MessageFifoArea[2 * 8 * 16];

typedef struct CanDataStorage DataStorageType;
DataStorageType DataStorage[8];

int i;
CANRxMessageBuffer * message;
uint32_t SID;

uint32_t FILTER0_VOL = 0x001;
uint32_t FILTER1_VOL = 0x003;
uint32_t FILTER2_VOL = 0x006;
uint32_t FILTER3_VOL = 0x007;
uint32_t FILTER4_VOL = 0x029;
uint32_t FILTER5_VOL = 0x02F;
uint32_t FILTER6_VOL = 0x045;
uint32_t FILTER7_VOL = 0x069;
uint32_t FILTER8_VOL = 0x105;
uint32_t FILTER9_VOL = 0x10C;
uint32_t FILTER10_VOL = 0x19F;
uint32_t FILTER11_VOL = 0x204;
uint32_t FILTER12_VOL = 0x371;
uint32_t FILTER13_VOL = 0x375;
uint32_t FILTER14_VOL = 0x388;
uint32_t FILTER15_VOL = 0x355;

BOOL	FILTER0_EN = TRUE;
BOOL	FILTER1_EN = TRUE;
BOOL	FILTER2_EN = TRUE;
BOOL	FILTER3_EN = TRUE;
BOOL	FILTER4_EN = TRUE;
BOOL	FILTER5_EN = TRUE;
BOOL	FILTER6_EN = TRUE;
BOOL	FILTER7_EN = TRUE;
BOOL	FILTER8_EN = TRUE;
BOOL	FILTER9_EN = TRUE;
BOOL	FILTER10_EN = TRUE;
BOOL	FILTER11_EN = TRUE;
BOOL	FILTER12_EN = TRUE;
BOOL	FILTER13_EN = TRUE;
BOOL	FILTER14_EN = TRUE;
BOOL	FILTER15_EN = TRUE;

uint32_t FILTER2_0_VOL = 0x001;
uint32_t FILTER2_1_VOL = 0x003;
uint32_t FILTER2_2_VOL = 0x006;
uint32_t FILTER2_3_VOL = 0x007;
uint32_t FILTER2_4_VOL = 0x029;
uint32_t FILTER2_5_VOL = 0x02F;
uint32_t FILTER2_6_VOL = 0x045;
uint32_t FILTER2_7_VOL = 0x069;
uint32_t FILTER2_8_VOL = 0x105;
uint32_t FILTER2_9_VOL = 0x10C;
uint32_t FILTER2_10_VOL = 0x19F;
uint32_t FILTER2_11_VOL = 0x204;
uint32_t FILTER2_12_VOL = 0x371;
uint32_t FILTER2_13_VOL = 0x375;
uint32_t FILTER2_14_VOL = 0x388;
uint32_t FILTER2_15_VOL = 0x355;

BOOL	FILTER2_0_EN = TRUE;
BOOL	FILTER2_1_EN = FALSE;
BOOL	FILTER2_2_EN = FALSE;
BOOL	FILTER2_3_EN = FALSE;
BOOL	FILTER2_4_EN = FALSE;
BOOL	FILTER2_5_EN = FALSE;
BOOL	FILTER2_6_EN = FALSE;
BOOL	FILTER2_7_EN = FALSE;
BOOL	FILTER2_8_EN = FALSE;
BOOL	FILTER2_9_EN = FALSE;
BOOL	FILTER2_10_EN = FALSE;
BOOL	FILTER2_11_EN = FALSE;
BOOL	FILTER2_12_EN = FALSE;
BOOL	FILTER2_13_EN = FALSE;
BOOL	FILTER2_14_EN = FALSE;
BOOL	FILTER2_15_EN = FALSE;


//Can be used to initialise both CAN modules from main InitCAN call
void InitCAN(void)
{
	//Disble analogue on CAN2 IO
	AD1PCFGbits.PCFG14 = 1;
	AD1PCFGbits.PCFG8 = 1;
	
	InitCAN1(125000, 1, 1, 4, 4);
	//InitCAN2(500000, 1, 1, 4, 4);

	InitCAN3(); //Uses SPI bus to talk to MCP2515
}

void InitCAN3(void)
{
	//
}

//Sets CAN interface to specific baud rate and to receive all messages for baud locking purposes.
void InitCAN1(uint32_t Baud, uint8_t SJW, uint8_t Prop, uint8_t Phase1, uint8_t Phase2)
{
	//uint32_t Baud = 125000;
	//uint8_t SJW = 1;
	//uint8_t Prop = 1;
	//uint8_t Phase1 = 4;	
	//uint8_t Phase2 = 4;

	//Enable CAN module 1
	CANEnableModule(CAN1, TRUE);
	//Put module into configuration mode
	CANSetOperatingMode(CAN1, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN1) != CAN_CONFIGURATION);

	//Set Baud Rate	defaulting to 125kbps
	SetCAN1Baud(&Baud, &SJW, &Prop, &Phase1, &Phase2); 

	//Assign CAN1 Memory buffer
	// 2 (Channels) x 8 (Buffers per channel) x 16 (Bytes per message)
	CANAssignMemoryBuffer(CAN1,CAN1MessageFifoArea,(2 * 8 * 16));

	//Configure Channels for TX
	CANConfigureChannelForTx(CAN1, CAN_CHANNEL0, 2, CAN_TX_RTR_DISABLED, CAN_LOW_MEDIUM_PRIORITY);

	//Configure Channels for RX - 12 buffers
	CANConfigureChannelForRx(CAN1, CAN_CHANNEL1, 14, CAN_RX_FULL_RECEIVE);
	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN1, CAN_FILTER0, FILTER0_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER1, FILTER1_VOL, CAN_EID); //EID!
	CANConfigureFilter		(CAN1, CAN_FILTER2, FILTER2_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER3, FILTER3_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER4, FILTER4_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER5, FILTER5_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER6, FILTER6_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER7, FILTER7_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER8, FILTER8_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER9, FILTER9_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER10, FILTER10_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER11, FILTER11_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER12, FILTER12_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER13, FILTER13_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER14, FILTER14_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER15, FILTER15_VOL, CAN_SID); //
	
	//Set Filter Masks (0 - Receive any, 1 - receive exact)
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK1, 0x000, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	//EID Filter Masks
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK2, 0x1FFFFFFF, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK3, 0x00000000, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);


	CANLinkFilterToChannel	(CAN1, CAN_FILTER0, CAN_FILTER_MASK1, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER1, CAN_FILTER_MASK3, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER1, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER2, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER3, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER4, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER5, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER6, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER7, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER8, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER9, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER10, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER11, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER12, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER13, CAN_FILTER_MASK0, CAN_CHANNEL1);	
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER14, CAN_FILTER_MASK0, CAN_CHANNEL1);
//	CANLinkFilterToChannel	(CAN1, CAN_FILTER15, CAN_FILTER_MASK0, CAN_CHANNEL1);

	CANEnableFilter			(CAN1, CAN_FILTER0, FILTER0_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER1, FILTER1_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER2, FILTER2_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER3, FILTER3_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER4, FILTER4_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER5, FILTER5_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER6, FILTER6_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER7, FILTER7_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER8, FILTER8_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER9, FILTER9_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER10, FILTER10_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER11, FILTER11_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER12, FILTER12_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER13, FILTER13_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER14, FILTER14_EN);	
//	CANEnableFilter			(CAN1, CAN_FILTER15, FILTER15_EN);	

	//Set Events - enable IRQs etc.
	CANEnableChannelEvent(CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
	CANEnableModuleEvent(CAN1, (CAN_RX_EVENT), TRUE);
	CANEnableModuleEvent(CAN1, (CAN_BUS_ACTIVITY_WAKEUP_EVENT), TRUE);


    INTSetVectorPriority(INT_CAN_1_VECTOR, INT_PRIORITY_LEVEL_6);
    INTSetVectorSubPriority(INT_CAN_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
	INTEnable(INT_CAN1, INT_ENABLED); // Enable CAN Interrupts (taken from CAN example)

	
	//Set Normal Operation mode
	CANSetOperatingMode(CAN1, CAN_LISTEN_ONLY);
    	while(CANGetOperatingMode(CAN1) != CAN_LISTEN_ONLY);
}

void InitCAN1Filters(void)
{

	//Put module into configuration mode
	CANSetOperatingMode(CAN1, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN1) != CAN_CONFIGURATION);
//Firstly disable all filters
	CANEnableFilter			(CAN1, CAN_FILTER0, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER0) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER1, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER1) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER2, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER2) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER3, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER3) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER4, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER4) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER5, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER5) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER6, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER6) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER7, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER7) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER8, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER8) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER9, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER9) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER10, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER10) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER11, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER11) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER12, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER12) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER13, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER13) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER14, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER14) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER15, FALSE);
	while (CANIsFilterDisabled(CAN1, CAN_FILTER15) == FALSE);

	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN1, CAN_FILTER0, FILTER0_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER1, FILTER1_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER2, FILTER2_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER3, FILTER3_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER4, FILTER4_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER5, FILTER5_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER6, FILTER6_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER7, FILTER7_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER8, FILTER8_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER9, FILTER9_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER10, FILTER10_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER11, FILTER11_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER12, FILTER12_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER13, FILTER13_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER14, FILTER14_VOL, CAN_SID); //
	CANConfigureFilter		(CAN1, CAN_FILTER15, FILTER15_VOL, CAN_SID); //
	
	//Set Filter Masks (0 - Receive exact match, 1 - Receive Any)
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK1, 0x000, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);


	CANLinkFilterToChannel	(CAN1, CAN_FILTER0, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER1, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER2, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER3, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER4, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER5, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER6, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER7, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER8, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER9, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER10, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER11, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER12, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER13, CAN_FILTER_MASK0, CAN_CHANNEL1);	
	CANLinkFilterToChannel	(CAN1, CAN_FILTER14, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER15, CAN_FILTER_MASK0, CAN_CHANNEL1);

	CANEnableFilter			(CAN1, CAN_FILTER0, FILTER0_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER1, FILTER1_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER2, FILTER2_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER3, FILTER3_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER4, FILTER4_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER5, FILTER5_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER6, FILTER6_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER7, FILTER7_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER8, FILTER8_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER9, FILTER9_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER10, FILTER10_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER11, FILTER11_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER12, FILTER12_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER13, FILTER13_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER14, FILTER14_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER15, FILTER15_EN);	

	//Set Normal Operation mode
	CANSetOperatingMode(CAN1, CAN_LISTEN_ONLY);
    	while(CANGetOperatingMode(CAN1) != CAN_LISTEN_ONLY);

}

void InitCAN1FiltersEID(void)
{

	//Put module into configuration mode
	CANSetOperatingMode(CAN1, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN1) != CAN_CONFIGURATION);
//Firstly disable all filters
	CANEnableFilter			(CAN1, CAN_FILTER0, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER0) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER1, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER1) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER2, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER2) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER3, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER3) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER4, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER4) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER5, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER5) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER6, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER6) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER7, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER7) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER8, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER8) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER9, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER9) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER10, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER10) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER11, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER11) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER12, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER12) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER13, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER13) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER14, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER14) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER15, FALSE);
	while (CANIsFilterDisabled(CAN1, CAN_FILTER15) == FALSE);

	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN1, CAN_FILTER0, FILTER0_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER1, FILTER1_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER2, FILTER2_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER3, FILTER3_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER4, FILTER4_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER5, FILTER5_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER6, FILTER6_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER7, FILTER7_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER8, FILTER8_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER9, FILTER9_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER10, FILTER10_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER11, FILTER11_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER12, FILTER12_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER13, FILTER13_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER14, FILTER14_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER15, FILTER15_VOL, CAN_EID); //
	
	//Set Filter Masks (0 - Receive exact match, 1 - Receive Any)
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK1, 0x000, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK2, 0x1FFFFFFF, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);


	CANLinkFilterToChannel	(CAN1, CAN_FILTER0, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER1, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER2, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER3, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER4, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER5, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER6, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER7, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER8, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER9, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER10, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER11, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER12, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER13, CAN_FILTER_MASK2, CAN_CHANNEL1);	
	CANLinkFilterToChannel	(CAN1, CAN_FILTER14, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER15, CAN_FILTER_MASK2, CAN_CHANNEL1);

	CANEnableFilter			(CAN1, CAN_FILTER0, FILTER0_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER1, FILTER1_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER2, FILTER2_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER3, FILTER3_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER4, FILTER4_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER5, FILTER5_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER6, FILTER6_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER7, FILTER7_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER8, FILTER8_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER9, FILTER9_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER10, FILTER10_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER11, FILTER11_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER12, FILTER12_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER13, FILTER13_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER14, FILTER14_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER15, FILTER15_EN);	

	//Set Normal Operation mode
	CANSetOperatingMode(CAN1, CAN_LISTEN_ONLY);
    	while(CANGetOperatingMode(CAN1) != CAN_LISTEN_ONLY);

}

void InitCAN1FiltersEIDNML(void)
{

	//Put module into configuration mode
	CANSetOperatingMode(CAN1, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN1) != CAN_CONFIGURATION);
//Firstly disable all filters
	CANEnableFilter			(CAN1, CAN_FILTER0, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER0) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER1, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER1) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER2, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER2) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER3, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER3) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER4, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER4) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER5, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER5) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER6, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER6) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER7, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER7) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER8, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER8) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER9, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER9) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER10, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER10) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER11, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER11) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER12, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER12) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER13, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER13) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER14, FALSE);	
	while (CANIsFilterDisabled(CAN1, CAN_FILTER14) == FALSE);
	CANEnableFilter			(CAN1, CAN_FILTER15, FALSE);
	while (CANIsFilterDisabled(CAN1, CAN_FILTER15) == FALSE);

	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN1, CAN_FILTER0, FILTER0_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER1, FILTER1_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER2, FILTER2_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER3, FILTER3_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER4, FILTER4_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER5, FILTER5_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER6, FILTER6_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER7, FILTER7_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER8, FILTER8_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER9, FILTER9_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER10, FILTER10_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER11, FILTER11_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER12, FILTER12_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER13, FILTER13_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER14, FILTER14_VOL, CAN_EID); //
	CANConfigureFilter		(CAN1, CAN_FILTER15, FILTER15_VOL, CAN_EID); //
	
	//Set Filter Masks (0 - Receive exact match, 1 - Receive Any)
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK1, 0x000, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN1, CAN_FILTER_MASK2, 0x1FFFFFFF, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);


	CANLinkFilterToChannel	(CAN1, CAN_FILTER0, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER1, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER2, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER3, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER4, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER5, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER6, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER7, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER8, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER9, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER10, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER11, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER12, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER13, CAN_FILTER_MASK2, CAN_CHANNEL1);	
	CANLinkFilterToChannel	(CAN1, CAN_FILTER14, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN1, CAN_FILTER15, CAN_FILTER_MASK2, CAN_CHANNEL1);

	CANEnableFilter			(CAN1, CAN_FILTER0, FILTER0_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER1, FILTER1_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER2, FILTER2_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER3, FILTER3_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER4, FILTER4_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER5, FILTER5_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER6, FILTER6_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER7, FILTER7_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER8, FILTER8_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER9, FILTER9_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER10, FILTER10_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER11, FILTER11_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER12, FILTER12_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER13, FILTER13_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER14, FILTER14_EN);	
	CANEnableFilter			(CAN1, CAN_FILTER15, FILTER15_EN);	

	//Set Normal Operation mode
	CANSetOperatingMode(CAN1, CAN_NORMAL_OPERATION);
    	while(CANGetOperatingMode(CAN1) != CAN_NORMAL_OPERATION);

}


void InitCAN2Filters(void)
{

	//Put module into configuration mode
	CANSetOperatingMode(CAN2, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN2) != CAN_CONFIGURATION);

	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN2, CAN_FILTER0, FILTER2_0_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER1, FILTER2_1_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER2, FILTER2_2_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER3, FILTER2_3_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER4, FILTER2_4_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER5, FILTER2_5_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER6, FILTER2_6_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER7, FILTER2_7_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER8, FILTER2_8_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER9, FILTER2_9_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER10, FILTER2_10_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER11, FILTER2_11_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER12, FILTER2_12_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER13, FILTER2_13_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER14, FILTER2_14_VOL, CAN_SID); //
	CANConfigureFilter		(CAN2, CAN_FILTER15, FILTER2_15_VOL, CAN_SID); //
	
	//Set Filter Masks (0 - Receive exact match, 1 - Receive Any)
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK1, 0x000, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);


	CANLinkFilterToChannel	(CAN2, CAN_FILTER0, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER1, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER2, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER3, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER4, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER5, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER6, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER7, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER8, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER9, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER10, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER11, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER12, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER13, CAN_FILTER_MASK0, CAN_CHANNEL1);	
	CANLinkFilterToChannel	(CAN2, CAN_FILTER14, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER15, CAN_FILTER_MASK0, CAN_CHANNEL1);

	CANEnableFilter			(CAN2, CAN_FILTER0, FILTER2_0_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER1, FILTER2_1_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER2, FILTER2_2_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER3, FILTER2_3_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER4, FILTER2_4_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER5, FILTER2_5_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER6, FILTER2_6_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER7, FILTER2_7_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER8, FILTER2_8_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER9, FILTER2_9_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER10, FILTER2_10_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER11, FILTER2_11_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER12, FILTER2_12_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER13, FILTER2_13_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER14, FILTER2_14_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER15, FILTER2_15_EN);	

	//Set Normal Operation mode
	CANSetOperatingMode(CAN2, CAN_LISTEN_ONLY);
    	while(CANGetOperatingMode(CAN2) != CAN_LISTEN_ONLY);

}

void InitCAN2FiltersFMS(void)
{

	//Put module into configuration mode
	CANSetOperatingMode(CAN2, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN2) != CAN_CONFIGURATION);

	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN2, CAN_FILTER0, FILTER2_0_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER1, FILTER2_1_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER2, FILTER2_2_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER3, FILTER2_3_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER4, FILTER2_4_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER5, FILTER2_5_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER6, FILTER2_6_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER7, FILTER2_7_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER8, FILTER2_8_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER9, FILTER2_9_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER10, FILTER2_10_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER11, FILTER2_11_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER12, FILTER2_12_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER13, FILTER2_13_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER14, FILTER2_14_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER15, FILTER2_15_VOL, CAN_EID); //
	
	//Set Filter Masks (0 - Receive exact match, 1 - Receive Any)
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK1, 0x000, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK2, 0x1FFFFFFF, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);


	CANLinkFilterToChannel	(CAN2, CAN_FILTER0, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER1, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER2, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER3, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER4, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER5, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER6, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER7, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER8, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER9, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER10, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER11, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER12, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER13, CAN_FILTER_MASK2, CAN_CHANNEL1);	
	CANLinkFilterToChannel	(CAN2, CAN_FILTER14, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER15, CAN_FILTER_MASK2, CAN_CHANNEL1);

	CANEnableFilter			(CAN2, CAN_FILTER0, FILTER2_0_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER1, FILTER2_1_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER2, FILTER2_2_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER3, FILTER2_3_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER4, FILTER2_4_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER5, FILTER2_5_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER6, FILTER2_6_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER7, FILTER2_7_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER8, FILTER2_8_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER9, FILTER2_9_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER10, FILTER2_10_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER11, FILTER2_11_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER12, FILTER2_12_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER13, FILTER2_13_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER14, FILTER2_14_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER15, FILTER2_15_EN);	

	//Set Normal Operation mode
	//CANSetOperatingMode(CAN2, CAN_LISTEN_ONLY);
    //	while(CANGetOperatingMode(CAN2) != CAN_LISTEN_ONLY);
	CANSetOperatingMode(CAN2, CAN_NORMAL_OPERATION);
    	while(CANGetOperatingMode(CAN2) != CAN_NORMAL_OPERATION);

}

void InitCAN2FiltersEID(void)
{

	//Put module into configuration mode
	CANSetOperatingMode(CAN2, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN2) != CAN_CONFIGURATION);

	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN2, CAN_FILTER0, FILTER2_0_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER1, FILTER2_1_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER2, FILTER2_2_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER3, FILTER2_3_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER4, FILTER2_4_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER5, FILTER2_5_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER6, FILTER2_6_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER7, FILTER2_7_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER8, FILTER2_8_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER9, FILTER2_9_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER10, FILTER2_10_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER11, FILTER2_11_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER12, FILTER2_12_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER13, FILTER2_13_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER14, FILTER2_14_VOL, CAN_EID); //
	CANConfigureFilter		(CAN2, CAN_FILTER15, FILTER2_15_VOL, CAN_EID); //
	
	//Set Filter Masks (0 - Receive exact match, 1 - Receive Any)
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK1, 0x000, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK2, 0x1FFFFFFF, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);


	CANLinkFilterToChannel	(CAN2, CAN_FILTER0, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER1, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER2, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER3, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER4, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER5, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER6, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER7, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER8, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER9, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER10, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER11, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER12, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER13, CAN_FILTER_MASK2, CAN_CHANNEL1);	
	CANLinkFilterToChannel	(CAN2, CAN_FILTER14, CAN_FILTER_MASK2, CAN_CHANNEL1);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER15, CAN_FILTER_MASK2, CAN_CHANNEL1);

	CANEnableFilter			(CAN2, CAN_FILTER0, FILTER2_0_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER1, FILTER2_1_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER2, FILTER2_2_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER3, FILTER2_3_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER4, FILTER2_4_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER5, FILTER2_5_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER6, FILTER2_6_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER7, FILTER2_7_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER8, FILTER2_8_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER9, FILTER2_9_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER10, FILTER2_10_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER11, FILTER2_11_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER12, FILTER2_12_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER13, FILTER2_13_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER14, FILTER2_14_EN);	
	CANEnableFilter			(CAN2, CAN_FILTER15, FILTER2_15_EN);	

	//Set Normal Operation mode
	CANSetOperatingMode(CAN2, CAN_LISTEN_ONLY);
    	while(CANGetOperatingMode(CAN2) != CAN_LISTEN_ONLY);
	//CANSetOperatingMode(CAN2, CAN_NORMAL_OPERATION);
    //	while(CANGetOperatingMode(CAN2) != CAN_NORMAL_OPERATION);

}

void InitCAN2(uint32_t Baud, uint8_t SJW, uint8_t Prop, uint8_t Phase1, uint8_t Phase2)
{

	//uint32_t Baud = 500000;
	//uint8_t SJW = 1;
	//uint8_t Prop = 1;
	//uint8_t Phase1 = 4;	
	//uint8_t Phase2 = 4;

	//Disable Analogue ports on CAN2 IO
	//Disble analogue on CAN2 IO
	AD1PCFGbits.PCFG14 = 1;
	AD1PCFGbits.PCFG8 = 1;

	//Enable CAN module 2
	CANEnableModule(CAN2, TRUE);
	//Put module into configuration mode
	CANSetOperatingMode(CAN2, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN2) != CAN_CONFIGURATION);

	//Set Baud Rate	defaulting to 500kbps
	SetCAN2Baud(&Baud, &SJW, &Prop, &Phase1, &Phase2); 

	//Assign CAN1 Memory buffer
	CANAssignMemoryBuffer(CAN2,CAN2MessageFifoArea,(2 * 8 * 16));

	//Configure Channels for TX
	CANConfigureChannelForTx(CAN2, CAN_CHANNEL0, 8, CAN_TX_RTR_DISABLED, CAN_LOW_MEDIUM_PRIORITY);

	//Configure Channels for RX
	CANConfigureChannelForRx(CAN2, CAN_CHANNEL1, 8, CAN_RX_FULL_RECEIVE);
	
	//Set Filters / Masks - single filter, mask set to receive all, filter set to SID
	CANConfigureFilter		(CAN2, CAN_FILTER0, 0x2A2, CAN_SID); //
	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK0, 0x7FF, CAN_SID, CAN_FILTER_MASK_ANY_TYPE);
	CANLinkFilterToChannel	(CAN2, CAN_FILTER0, CAN_FILTER_MASK0, CAN_CHANNEL1);
	CANEnableFilter			(CAN2, CAN_FILTER0, FALSE);	

	//Set Filters / Masks - single filter, mask set to receive all, filter set to EID
//	CANConfigureFilter		(CAN2, CAN_FILTER1, 0x00000000, CAN_EID); //
//	CANConfigureFilterMask	(CAN2, CAN_FILTER_MASK1, 0x00000000, CAN_EID, CAN_FILTER_MASK_ANY_TYPE);
//	CANLinkFilterToChannel	(CAN2, CAN_FILTER1, CAN_FILTER_MASK1, CAN_CHANNEL2);
//	CANEnableFilter			(CAN2, CAN_FILTER1, TRUE);	

	//Set Events - enable IRQs etc.
	CANEnableChannelEvent(CAN2, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
	CANEnableModuleEvent(CAN2, (CAN_RX_EVENT), TRUE);

    INTSetVectorPriority(INT_CAN_2_VECTOR, INT_PRIORITY_LEVEL_6);
    INTSetVectorSubPriority(INT_CAN_2_VECTOR, INT_SUB_PRIORITY_LEVEL_1);
	INTEnable(INT_CAN2, INT_ENABLED); // Enable CAN Interrupts (taken from CAN example)

	//Set Normal Operation mode
	CANSetOperatingMode(CAN2, CAN_LISTEN_ONLY);
    	while(CANGetOperatingMode(CAN2) != CAN_LISTEN_ONLY);
}



void SetCANModuleBaud(uint32_t *Baud,uint8_t *SJW,uint8_t *Prop,uint8_t *Phase1,uint8_t *Phase2)
{
	//Put module 1 & 2 into configuration mode - this should also abort any transmissions
	CANSetOperatingMode(CAN1, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN1) != CAN_CONFIGURATION);

	CANSetOperatingMode(CAN2, CAN_CONFIGURATION);
    	while(CANGetOperatingMode(CAN2) != CAN_CONFIGURATION);

	SetCAN1Baud(Baud, SJW, Prop, Phase1, Phase2); 
	SetCAN2Baud(Baud, SJW, Prop, Phase1, Phase2); 

	CANSetOperatingMode(CAN1, CAN_NORMAL_OPERATION);
    	while(CANGetOperatingMode(CAN1) != CAN_NORMAL_OPERATION);

	CANSetOperatingMode(CAN2, CAN_NORMAL_OPERATION);
    	while(CANGetOperatingMode(CAN2) != CAN_NORMAL_OPERATION);
}

void SetCAN1Baud(uint32_t *Baud,uint8_t *SJW,uint8_t *Prop,uint8_t *Phase1,uint8_t *Phase2)
{
	CAN_BIT_CONFIG canBitConfig;
	uint8_t baudPrescalar;

	baudPrescalar = *Phase2;

	canBitConfig.phaseSeg2Tq 			= (*Phase2) - 1;
	canBitConfig.phaseSeg1Tq            = (*Phase1) - 1;
    canBitConfig.propagationSegTq       = (*Prop) - 1;
    canBitConfig.phaseSeg2TimeSelect    = TRUE;
    canBitConfig.sample3Time            = TRUE;
    canBitConfig.syncJumpWidth          = *SJW;

	CANSetSpeed(CAN1, &canBitConfig, CAN_CLK , *Baud);
}

void SetCAN2Baud(uint32_t *Baud,uint8_t *SJW,uint8_t *Prop,uint8_t *Phase1,uint8_t *Phase2)
{
	CAN_BIT_CONFIG canBitConfig;
	uint8_t baudPrescalar;

	canBitConfig.phaseSeg2Tq 			= (*Phase2) - 1;
	canBitConfig.phaseSeg1Tq            = (*Phase1) - 1;
    canBitConfig.propagationSegTq       = (*Prop) - 1;
    canBitConfig.phaseSeg2TimeSelect    = TRUE;
    canBitConfig.sample3Time            = TRUE;
    canBitConfig.syncJumpWidth          = *SJW;

	CANSetSpeed(CAN2, &canBitConfig, CAN_CLK , *Baud);
}


#ifdef ENABLETX
//Sends standard CAN Message from CAN1 Module
void CANTxMessage(uint8_t *Bytes, uint16_t *uID, uint8_t *uCANData)
{
	CANTxMessageBuffer * message;

	message = CANGetTxMessageBuffer(CAN1, CAN_CHANNEL0);

	if (message != NULL)
	{
		message->messageWord[0] = 0;
		message->messageWord[1] = 0;
		message->messageWord[2] = 0;
		message->messageWord[3] = 0;

		message->msgSID.SID = *uID;
		message->msgEID.IDE = 0;
		message->msgEID.DLC = *Bytes;
		message->data[0] = uCANData[0];
		message->data[1] = uCANData[1];
		message->data[2] = uCANData[2];
		message->data[3] = uCANData[3];
		message->data[4] = uCANData[4];
		message->data[5] = uCANData[5];
		message->data[6] = uCANData[6];
		message->data[7] = uCANData[7];

	}

	CANUpdateChannel(CAN1, CAN_CHANNEL0);
	CANFlushTxChannel(CAN1, CAN_CHANNEL0);


}

//Sends standard CAN Message from CAN1 Module

void CANTxExMessage(uint8_t *Bytes, uint32_t *uID, uint8_t *uCANData)
{
	CANTxMessageBuffer * message;
	uint32_t temp32;

	temp32 = *uID;

	message = CANGetTxMessageBuffer(CAN1, CAN_CHANNEL0);

	if (message != NULL)
	{
		message->messageWord[0] = 0;
		message->messageWord[1] = 0;
		message->messageWord[2] = 0;
		message->messageWord[3] = 0;

		message->msgSID.SID = (((*uID)>>18) & 0x7FF);
		message->msgEID.EID = (((*uID)) & 0x3FFFF);
		message->msgEID.IDE = 1;
		message->msgEID.DLC = *Bytes;
		message->data[0] = uCANData[0];
		message->data[1] = uCANData[1];
		message->data[2] = uCANData[2];
		message->data[3] = uCANData[3];
		message->data[4] = uCANData[4];
		message->data[5] = uCANData[5];
		message->data[6] = uCANData[6];
		message->data[7] = uCANData[7];

	}

	CANUpdateChannel(CAN1, CAN_CHANNEL0);
	CANFlushTxChannel(CAN1, CAN_CHANNEL0);


}


/*****************************************************************
*	DESCRIPTION : CheckData	
*
*	ARGUMENTS 	: 	unsigned long * id
*					char * data
*					BYTE dataLen
*
*	RETURN		:	void
*
*	NOTES		:	Checks data against storage structure
*					and updates datastore if required
*
*****************************************************************/
void CheckData(void)
{
	//int i;
	//CANRxMessageBuffer * message;

	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);

	ToggleLED_CAN();
	
	SID = message->msgSID.SID;

	//go through data storage structure and see if received ID appears
	for (i=0; i<8; i++)
	{
		if (DataStorage[i].uID == SID)
		{
			DataStorage[i].bNewData = 1;
			DataStorage[i].bData[0] = message->data[0];
			DataStorage[i].bData[1] = message->data[1];
			DataStorage[i].bData[2] = message->data[2];
			DataStorage[i].bData[3] = message->data[3];
			DataStorage[i].bData[4] = message->data[4];
			DataStorage[i].bData[5] = message->data[5];
			DataStorage[i].bData[6] = message->data[6];
			DataStorage[i].bData[7] = message->data[7];
		}
	}

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}
#endif