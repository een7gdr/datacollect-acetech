#ifndef _DC_CAN_H
#define _DC_CAN_H

#include "SYS.h"
//Phase Segment 2 Time Select Bit
// 1 - Freely programmable
// 0 - Max of SEG1PH or IPT
#define SEG2PHTS 1

//CAN Sample CAN Bus Line
// 1 - Sampled 3 times
// 0 - Sampled once
#define SAMPLE 0

enum {	CAN_MODE_NORMAL=0, 
		CAN_MODE_DISABLE,
		CAN_MODE_LOOPBACK,
		CAN_MODE_LISTEN,
		CAN_MODE_CONFIG
		};

/*Data checking structure*/
/*Array of structures, one corresponding to each possible filter hit*/
struct CanDataStorage{
	uint16_t uID;
	uint8_t bData[8];
	uint8_t bNewData;
};


//#define CAN_CLK 40000000 //80Mhz
#define CAN_CLK 20000000 //40Mhz
//#define CAN_CLK 16000000 //32Mhz


void InitCAN(void);
void InitCAN1(uint32_t Baud, uint8_t SJW, uint8_t Prop, uint8_t Phase1, uint8_t Phase2);
void InitCAN1Filters(void);
void InitCAN1FiltersEID(void);
void InitCAN1FiltersEIDNML(void);
void InitCAN2(uint32_t Baud, uint8_t SJW, uint8_t Prop, uint8_t Phase1, uint8_t Phase2);
void InitCAN2Filters(void);
void InitCAN3(void);
void SetCAN1Baud(uint32_t *BRP,uint8_t *SJW,uint8_t *Prop,uint8_t *Phase1,uint8_t *Phase2);
void SetCAN2Baud(uint32_t *BRP,uint8_t *SJW,uint8_t *Prop,uint8_t *Phase1,uint8_t *Phase2);
void InitCAN2FiltersEID(void);
void InitCAN2FiltersFMS(void);
void CANTxMessage(uint8_t *Bytes, uint16_t *uID, uint8_t *uCANData);
void CANTxExMessage(uint8_t *Bytes, uint32_t *uID, uint8_t *uCANData);
void CheckData();



#endif