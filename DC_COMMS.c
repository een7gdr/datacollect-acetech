#include <p32xxxx.h>
#include "DC_COMMS.h"
#include "DC_LED.h"

enum {LOC_IDLE=0, LOC_DATA};
enum {BLS_IDLE=0, BLS_CT, BLS_RX};
uint8_t LocalPortStatus = LOC_IDLE;
uint8_t BluesPortStatus = BLS_IDLE;
uint8_t Rs232_4PortStatus = BLS_IDLE;
uint8_t RsgRxStatus =0;
char LocBuffer[MAX_LOCBUFFER_LENGTH];
char BlsBuffer[MAX_BLSBUFFER_LENGTH];
char Bls2Buffer[MAX_BLS2BUFFER_LENGTH];
uint8_t PmcBankChange[4];
uint16_t LocBufferLen;
uint16_t BlsBufferLen;
uint16_t Rs232_4BufferLen;
uint8_t BlsCount = 0;
uint8_t LocalInUseFlag = FALSE;
uint8_t BlsLRC = 0;
uint16_t RsgCRC = 0;
uint16_t RsgCount = 0;
extern uint8_t ToggleLEDGREEN;


void onRx232_4Packet()
{
	char NextChar;
	static uint8_t Chk;
	uint8_t RxChk;

	while(U4STAbits.URXDA == 1)
	{
		NextChar = (char)(U4RXREG & 0xff);

		switch (Rs232_4PortStatus)
		{
		case BLS_IDLE:
			if (NextChar == '*') //0x2A
			{
				Rs232_4PortStatus = BLS_CT;
				Rs232_4BufferLen = 0;
//				PmcChange = 0;
				Chk = (uint8_t) NextChar;
			}
			break;
		case BLS_CT:
			if (NextChar == 'R') //0x52 Read of PMC data
			{
				Rs232_4PortStatus = BLS_RX;
				Rs232_4BufferLen = 0;
				Chk += (uint8_t) NextChar;
			}
			else //reset to idle because we are not looking for this command
			{
				Rs232_4PortStatus = BLS_IDLE;
				Rs232_4BufferLen = 0;
			}
			break;
		case BLS_RX:
			//receive data and store
			if (Rs232_4BufferLen < (PMC_DATA_LENGTH-2)) //64 data, 1 status, 2 Checksum
			{
			//	PmcChange |= NextChar ^ Bls2Buffer[Rs232_4BufferLen];
				Bls2Buffer[Rs232_4BufferLen++] = NextChar;
				Chk += (uint8_t) NextChar;
			}
			else if (Rs232_4BufferLen == (PMC_DATA_LENGTH-2)) //should get here for checksum only
			{
				RxChk = NextChar & 0xF;
				Bls2Buffer[Rs232_4BufferLen++] = NextChar;
				//Rs232_4PortStatus = BLS_IDLE;
				//break;
			}
			else if (Rs232_4BufferLen == (PMC_DATA_LENGTH-1))
			{
				RxChk |= (NextChar << 4)	& 0xF0;
				Bls2Buffer[Rs232_4BufferLen++] = NextChar;
			}
			// All data received
			if (Rs232_4BufferLen >= PMC_DATA_LENGTH)
			{
				//Call process message function here
				//RxChk = (Bls2Buffer[67] << 4) & 0xF0;
				//RxChk |= Bls2Buffer[66] & 0xF;
				if (RxChk == Chk)
					ProcessRs232_4Message(Bls2Buffer, Rs232_4BufferLen);
				Rs232_4PortStatus = BLS_IDLE;
			}
			
			break;
		default:
			Rs232_4PortStatus = BLS_IDLE;
			break;

		}
	}
}

void onRx232Packet()
{
	char NextChar;

	while(U1STAbits.URXDA == 1)
	{
		NextChar = (char)(U1RXREG & 0xff);

		switch (LocalPortStatus)
		{
		case LOC_IDLE:
			if (NextChar == 'S')
			{
				LocalPortStatus = LOC_DATA;
				LocBufferLen = 0;
				//SoftReset();

			}

		break;
		case LOC_DATA:
			if (NextChar != 0xd)
			{		
				if (LocBufferLen < MAX_LOCBUFFER_LENGTH)
					LocBuffer[LocBufferLen++] = NextChar;
		
			}
			else
			{
				ProcessLocalMessage(LocBuffer, LocBufferLen);
				LocalPortStatus = LOC_IDLE;
				LocalInUseFlag = FALSE;
				ToggleLED_232();
				//Change status LED here
			}

		break;

		default:
			LocalPortStatus = LOC_IDLE;
			break;
		}
	

	}
	

}

uint8_t Address_Read(void)
{
	uint16_t Temp;
	
	PORTBbits.RB15 = 0;
	TRISBbits.TRISB15 = 0;


	Temp = ~PORTB;
	Temp = Temp >> 10;

	PORTBbits.RB15 = 1;	

	return ((uint8_t)(Temp&0x0F)); 
	
}

void onRx485Packet()
{
	uint16_t NextChar;
	uint8_t NextChar8; //Used for compatibility 8 bit data mode, i.e. RSG
	uint8_t Address=0;
	uint8_t BufferInc;

//	PORTEbits.RE0 = 1; //RTEST

	Address = Address_Read();

	//Need to select RSG or Genisys structures here
	//Address 0 is reserved so cannot be used in genisys land, therefore this is used to select RSG mode
	//RSG requires standard 8 bit serial, genisys used 9 bit.

	if (Address !=0)
	{

		while(U2STAbits.URXDA == 1)
		{
			NextChar = (uint16_t)(U2RXREG & 0x1FF);

			switch (BluesPortStatus)
			{
			case BLS_IDLE:
				//If bus idle and next character matches address and has 9th bit set
				if (NextChar == (Address | 0x100))
				{
					BluesPortStatus = BLS_CT;
					BlsBuffer[0] = (char)NextChar & 0xff;
					BlsBufferLen = 1;
					BlsLRC = BlsBuffer[0];				
				}

			break;
			case BLS_CT:
				//9th bit should not be set once bus is no longer idle
				//if it is then an error has occurred
				if ((NextChar & 0x100) != 0)
				{		
					BluesPortStatus = BLS_IDLE;
					BlsBufferLen = 0;

					//if (LocBufferLen < MAX_LOCBUFFER_LENGTH)
					//	LocBuffer[LocBufferLen++] = NextChar;
				}
				else
				{
					if((NextChar & 0xff) > (MAX_BLSBUFFER_LENGTH))
					{
						//Message is too long - abort
						BluesPortStatus = BLS_IDLE;
						BlsBufferLen = 0;
					}
					else
					{
							BlsCount = (NextChar & 0xff) - 2;  // We have already received the address and count
							BluesPortStatus = BLS_RX;
							BlsBuffer[BlsBufferLen++] = (char) NextChar & 0xff;
							BlsLRC += ((char) NextChar & 0xff);	
					}

				}
			break;

			case BLS_RX:
				if ((NextChar & 0x100) != 0)
				{		
					BluesPortStatus = BLS_IDLE;
					BlsBufferLen = 0;

				}
				else
				{
					BlsBuffer[BlsBufferLen++] = (char) NextChar & 0xff;
					BlsCount--;
					BlsLRC += ((char) NextChar & 0xff);	
					if (BlsCount == 0)
					{
						if (BlsLRC == 0)
						{
							BluesPortStatus = BLS_IDLE;
							ProcessBlsMessage(BlsBuffer, BlsBufferLen);
							//LED_ECU_RED = 1; // Active low - turn off error LED
						}
						//else LED_ECU_RED = 0; // Active Low
					}	
				}
			break;

			default:
				BluesPortStatus = LOC_IDLE;
				break;
			}	
	

		}
	}//End of genisys section
	
	//RSG Mode i.e. Address = 0
	else
	{
	//Populate buffer with received data. This method is called if/while there is data on the UART
		
		//While these is data on the RX buffer
		while(U2STAbits.URXDA == 1)
		{	
			NextChar = (uint8_t)(U2RXREG);
			//Cases are: 
			//1:Looking for potential start bit
			//2:Confirm actual start bit by receiving following header bytes. check length and CRC
			//3:Once confirmed, actual data read in until final ETX is received at the expected offset determined by the length field
			//	Call processing routine if correct otherwise clear buffer and try again

			switch (RsgRxStatus)
			{
				case 0:
					if (NextChar == 0x02)
					{
						BlsBufferLen = 1;
						BlsLRC = 0; //Reset redundancy check var
						RsgRxStatus = 1; //potential start bit received, jump to next section
						BlsBuffer[0] = (char)NextChar;

					}	
					else
					{
						BlsBufferLen = 0;
					}
				break;
				
				//potential start bit received, fill up to header length and check LRC
				case 1:
					
					if (BlsBufferLen >= MAX_BLSBUFFER_LENGTH-1) //trap if buffer will overflow
					{
						//Reset and start over looking for next start bit
						BlsBufferLen = 0;
						RsgRxStatus = 0;
					}
				
					BlsBuffer[BlsBufferLen++] = (char)NextChar;
					
					//Need to check header CRC before continuing
					if (BlsBufferLen > 5) //i.e. at least 6 which is the size of the header
					{
						RsgCRC = 0;
						for (BufferInc = 1; BufferInc < 6 ; BufferInc++)
						{
							rsgCrcUpdate(BlsBuffer[BufferInc]);	
						}
						//Accoring to documentation, crc should be zero if correct
						if (RsgCRC == 0)
						{
							//This should be the length of the packet
							RsgCount = (uint16_t)((uint8_t)BlsBuffer[2] + ((uint8_t)BlsBuffer[3]) * 256);
							RsgRxStatus = 2;	
							//ensure RsgCount is not larger than array otherwise an error might have occured
							if (RsgCount > MAX_BLSBUFFER_LENGTH-1)
							{	
								BlsBufferLen = 0;
								RsgRxStatus = 0;
							} 	
						}
						else //if CRC is incorrect, either data error or not a true start bit. 
						{
							BlsBufferLen = 0;
							RsgRxStatus = 0;
						}
					}
				break;
				//Header received and passed CRC check. We have a known length of the packet	
				case 2:

					if (BlsBufferLen >= MAX_BLSBUFFER_LENGTH-1) //trap if buffer will overflow
					{
						//Reset and start over looking for next start bit
						BlsBufferLen = 0;
						RsgRxStatus = 0;
					}
				
					BlsBuffer[BlsBufferLen++] = (char)NextChar;

					//if true we should have the complete packet and can check for ETX, otherwise let the buffer fill
					if (BlsBufferLen >= RsgCount)
					{
						//Data received and ETX located at correct point
						if (BlsBuffer[RsgCount-1] == 0x03)
						{
							RsgCRC = 0;
							for (BufferInc = 1; BufferInc < (RsgCount - 1) ; BufferInc++)
							{
								rsgCrcUpdate(BlsBuffer[BufferInc]);	
							}
							//Accoring to documentation, crc should be zero if correct
							if (RsgCRC == 0)
							{
								RsgRxStatus = 0;
								//Data received correctly, needs to be parsed
								ProcessRsgMessage(BlsBuffer, BlsBufferLen);
							}
							else //CRC incorrect
							{
								BlsBufferLen = 0;
								RsgRxStatus = 0;
							}

						}
						else //ETX wasnt there
						{
							BlsBufferLen = 0;
							RsgRxStatus = 0;
						}
					}


				break;
				
				default:
					BlsBufferLen = 0;
					RsgRxStatus = 0;
				break;
			}
		
		}

	}
//		PORTEbits.RE0 = 0; //RTEST
}

void rsgCrcUpdate(uint8_t data)
{
	uint8_t tmpData = data;

	tmpData ^= (uint8_t) RsgCRC;
	tmpData ^= tmpData << 4;

	RsgCRC =((((uint16_t)tmpData << 8) | (uint8_t)(RsgCRC>>8)) ^ (uint8_t)(tmpData >>4) ^ ((uint16_t)tmpData <<3));
}