#ifndef _DC_COMMS_H
#define _DC_COMMS_H

#include "SYS.h"

#define MAX_LOCBUFFER_LENGTH 100
#define MAX_BLSBUFFER_LENGTH 100
#define MAX_BLS2BUFFER_LENGTH 100
#define PMC_DATA_LENGTH 67 //64 data, 1 status, 2 Checksums

uint8_t Address_Read(void);

void onRx232Packet(void);
void onRx232_4Packet(void);
void onRx485Packet(void);
void rsgCrcUpdate(uint8_t data);

#endif