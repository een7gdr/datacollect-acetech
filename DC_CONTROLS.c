#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_CONTROLS.h"
#include "DC_LIB_MERCEDES.h"
#include "DC_LIB_VOLVO.h"
#include "DC_LIB_SKODA.h"
#include "DC_LIB_LANDROVER.h"
#include "DC_LIB_FORD.h"
#include "DC_LED.h"
#include "DC_LIB_BMW.h"
#include "DC_LIB_RENAULT.h"
#include "DC_LIB_HONDA.h"
#include "DC_LIB_FIAT.h"
#include "DC_LIB_FERNO.h"
//#include "DC_CAN_LIBRARIES.h"

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80
#define BIT8 0x0100
#define BIT9 0x0200
#define BIT10 0x0400
#define BIT11 0x0800
#define BIT12 0x1000
#define BIT13 0x2000
#define BIT14 0x4000
#define BIT15 0x8000

extern uint8_t CANActivity;
extern uint8_t uBaudSelector;
extern uint8_t SecondCANActive;

extern uint8_t GPIOByte0_Prev;
extern uint8_t GPIOByte1_Prev;

extern enum Module_State
{
	STARTUP,
	NOT_LOCKED,
	BAUD_LOCKED,	
	LOCKED,
	SLEEP,
	ERROR
}State, Previous_State;

uint8_t StartPacket = 0x02; 	//Byte0
uint8_t NumBytes = 28;			//Byte1
uint8_t StatusID = 'C';			//Byte2
uint32_t Odometer = 0xFFFFFFFF;	//Bytes3-5
uint8_t RPM = 0xFF;				//Byte6
uint8_t RPM_Temp;
uint8_t RoadSpeed = 0xFF;		//Byte7
uint8_t RoadSpeed_Temp;
uint16_t FuelLevel = 0xFFFF; 	//Bytes8-9
uint16_t FuelLevel_Temp;
uint16_t FuelFlowCount = 0xFFFF;//Bytes10-11
uint8_t	Ignition = 0xFF;		//Byte12
uint8_t Ignition_Temp;
uint8_t Accelerator = 0xFF;		//Byte13
uint8_t Accelerator_Temp;
uint8_t Brake = 0xFF;			//Byte14
uint8_t Brake_Temp;
uint16_t Lights = 0xF0FF;		//Byte15-16
uint16_t Lights_Temp;
uint8_t Wipers = 0xFF;			//Byte17
uint8_t Wipers_Temp;
uint8_t CoolantTemp = 0xFF;		//Byte18
uint8_t CoolantTemp_Temp;
uint8_t AmbientTemp = 0xFF;		//Byte19
uint8_t AmbientTemp_Temp;
uint16_t SteeringWheel = 0xFFFF;//Bytes20-21 (Byte 15 Direction 0 Left, 1 Right)
uint16_t SteeringWheel_Temp;
uint8_t Controls = 0xFF;		//Byte22 (HB, Horn, Clutch, Reverse)
uint8_t Controls_Temp;
uint8_t AutoBox = 0xFF;			//Byte23
uint8_t AutoBox_Temp;
uint8_t ManualBox = 0xFF;		//Byte24
uint8_t ManualBox_Temp;
uint8_t Doors = 0xFF;			//Byte25
uint8_t Doors_Temp;
uint8_t SeatBelts = 0xFF;		//Byte26
uint8_t SeatBelts_Temp;
uint8_t Occupancy = 0xFF;		//Byte27
uint8_t Occupancy_Temp;
uint8_t MILLights = 0xFF;		//Byte28
uint8_t MILLights_Temp;
uint8_t Checksum = 0xFF;		//Byte29
uint8_t	EndOfPacket = 0x03;		//Byte30

uint8_t AceTechBluesBytes[7]; 	//acetech blue
uint8_t AceTechBluesBytesPrev[7];
uint8_t AceTechBattBytes[5];	//acetech battery
uint8_t AceTechBattBytesPrev[5];


//Start Packet Byte0
uint8_t VehicleNumBytes = 25;	//Byte1
uint8_t StaticID = 'V';			//Byte2
//uint8_t HardwareVersion = 0x01;	//Byte3
//extern const uint8_t	FirmwareVersion; //Byte4
uint8_t VehicleType = 0x00;		//Byte5 Mercedes

uint8_t GenisysID = 'G';			//Byte2
uint8_t GenisysNumBytes = 6;	//Byte1

uint8_t RsgID = 'R';
uint8_t RsgNumBytes = 11;

uint8_t RsgInfoID = 'S';
uint8_t RsgInfoNumBytes = 12;

uint8_t PmcID = 'P';
uint8_t PmcNumBytes = 13; // Check!

uint8_t AceTechID = 'A';
uint8_t AceTechNumBytes = 14; //Check!

uint8_t GPIONumBytes = 4;
uint8_t GPIOID = 'O';
uint8_t GPIOByte0 = 0xFF;
uint8_t GPIOByte1 = 0xFF;


uint8_t VIN[17] = {'A', 'B', 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 'C'};				//Bytes6-22
//Checksum byte 23
//End Packet byte 24

extern uint8_t AceTechActivity;
extern uint8_t AceTechActioned;

extern uint32_t FILTER0_VOL;
extern uint32_t FILTER1_VOL;
extern uint32_t FILTER2_VOL;
extern uint32_t FILTER3_VOL;
extern uint32_t FILTER4_VOL;
extern uint32_t FILTER5_VOL;
extern uint32_t FILTER6_VOL;
extern uint32_t FILTER7_VOL;
extern uint32_t FILTER8_VOL;
extern uint32_t FILTER9_VOL;
extern uint32_t FILTER10_VOL;
extern uint32_t FILTER11_VOL;
extern uint32_t FILTER12_VOL;
extern uint32_t FILTER13_VOL;
extern uint32_t FILTER14_VOL;
extern uint32_t FILTER15_VOL;

extern BOOL	FILTER0_EN;
extern BOOL	FILTER1_EN;
extern BOOL	FILTER2_EN;
extern BOOL	FILTER3_EN;
extern BOOL	FILTER4_EN;
extern BOOL	FILTER5_EN;
extern BOOL	FILTER6_EN;
extern BOOL	FILTER7_EN;
extern BOOL	FILTER8_EN;
extern BOOL	FILTER9_EN;
extern BOOL	FILTER10_EN;
extern BOOL	FILTER11_EN;
extern BOOL	FILTER12_EN;
extern BOOL	FILTER13_EN;
extern BOOL	FILTER14_EN;
extern BOOL	FILTER15_EN;

extern uint32_t FILTER2_0_VOL;
extern uint32_t FILTER2_1_VOL;
extern uint32_t FILTER2_2_VOL;
extern uint32_t FILTER2_3_VOL;
extern uint32_t FILTER2_4_VOL;
extern uint32_t FILTER2_5_VOL;
extern uint32_t FILTER2_6_VOL;
extern uint32_t FILTER2_7_VOL;
extern uint32_t FILTER2_8_VOL;
extern uint32_t FILTER2_9_VOL;
extern uint32_t FILTER2_10_VOL;
extern uint32_t FILTER2_11_VOL;
extern uint32_t FILTER2_12_VOL;
extern uint32_t FILTER2_13_VOL;
extern uint32_t FILTER2_14_VOL;
extern uint32_t FILTER2_15_VOL;

extern BOOL	FILTER2_0_EN;
extern BOOL	FILTER2_1_EN;
extern BOOL	FILTER2_2_EN;
extern BOOL	FILTER2_3_EN;
extern BOOL	FILTER2_4_EN;
extern BOOL	FILTER2_5_EN;
extern BOOL	FILTER2_6_EN;
extern BOOL	FILTER2_7_EN;
extern BOOL	FILTER2_8_EN;
extern BOOL	FILTER2_9_EN;
extern BOOL	FILTER2_10_EN;
extern BOOL	FILTER2_11_EN;
extern BOOL	FILTER2_12_EN;
extern BOOL	FILTER2_13_EN;
extern BOOL	FILTER2_14_EN;
extern BOOL	FILTER2_15_EN;

uint8_t Hold_Counter = 0;

CANRxMessageBuffer * message;
uint32_t SID;

static inline void set_bitByte(uint8_t  *x, uint8_t bitNum)
	{ *x |= (((uint8_t)0x01) << bitNum);}

static inline void set_bit2Byte(uint16_t *x, uint8_t bitNum)
	{ *x |= (((uint16_t)0x01) << bitNum);}

static inline void clear_bitByte(uint8_t  *x, uint8_t bitNum)
	{ *x &= ~(((uint8_t)0x01) << bitNum);}

static inline void clear_bit2Byte(uint16_t *x, uint8_t bitNum)
	{ *x &= ~(((uint16_t)0x01) << bitNum);}




BOOL CheckBitFn(uint8_t ToCheck, uint16_t BitToCheck)
{
	if((ToCheck & BitToCheck) == BitToCheck) return(TRUE);
	else return (FALSE);
}

void SetBitFn8(uint8_t* VarPoint, uint16_t BitToSet)
{
	*VarPoint = *VarPoint | BitToSet;
}
void SetBitFn16(uint16_t* VarPoint, uint16_t BitToSet)
{
	*VarPoint = *VarPoint | BitToSet;
}
void ClrBitFn8(uint8_t* VarPoint, uint16_t BitToSet)
{
	*VarPoint = *VarPoint &~ BitToSet;
}
void ClrBitFn16(uint16_t* VarPoint, uint16_t BitToSet)
{
	*VarPoint = *VarPoint &~ BitToSet;
}
void CheckSet8(uint8_t Byte, uint16_t BitToCheck, uint8_t* VarPoint, uint16_t BitToSet)
{
	if((Byte & BitToCheck) == BitToCheck) *VarPoint = *VarPoint | BitToSet;
	else *VarPoint = *VarPoint &~ BitToSet;
}
void CheckSet8Inv(uint8_t ByteNum, uint16_t BitToCheck, uint8_t* VarPoint, uint16_t BitToSet)
{
	if((ByteNum & BitToCheck) == BitToCheck) *VarPoint = *VarPoint &~ BitToSet;
	else *VarPoint = *VarPoint | BitToSet;
}
void CheckSet16(uint8_t ByteNum, uint16_t BitToCheck, uint16_t* VarPoint, uint16_t BitToSet)
{
	if((ByteNum & BitToCheck) == BitToCheck) *VarPoint = *VarPoint | BitToSet;
	else *VarPoint = *VarPoint &~ BitToSet;
}
void CheckSet16Inv(uint8_t ByteNum, uint16_t BitToCheck, uint16_t* VarPoint, uint16_t BitToSet)
{
	if((ByteNum & BitToCheck) == BitToCheck) *VarPoint = *VarPoint &~ BitToSet;
	else *VarPoint = *VarPoint | BitToSet;
}

BOOL CheckGPIO(void)
{
	uint16_t TempPort0;
	uint16_t TempPort1;
	uint8_t Test;

	TempPort0 = PORTD;
	TempPort1 = PORTC;

	GPIOByte1 = (~TempPort1 >> 13 ) & 0x03;
	GPIOByte0 = TempPort0 & 0x01;
	GPIOByte0 = GPIOByte0 | ((TempPort0 >> 3)& 0x3E);
	GPIOByte0 = GPIOByte0 | ((TempPort0 >> 4)& 0xC0);

	Test = GPIOByte0^GPIOByte0_Prev;
	Test |= GPIOByte1^GPIOByte1_Prev;

	if (Test!=0) return TRUE;
	else return FALSE;
	
}

//Generic function - should call relavent function to interpret vehicles CAN data
void DecodeData(void)
{
	switch(VehicleType)
	{
		case MERCEDES_CAR_1_TYPE:
			DecodeDataMercW176Type();
			break;
		
		case MERCEDES_VAN_1_TYPE:
			DecodeDataMercSprinterType();
			break;

		case LR_CAR_1_TYPE:
			DecodeDataLandRoverDisco4Type();
			break;
	
		case VOLVO_CAR_1_TYPE:
			DecodeDataVolvoV702012Type();
			break;

		case SKODA_CAR_1_TYPE:
			DecodeDataSkodaScout2014Type();
			break;

		case FORD_CAR_1_TYPE:
			DecodeDataFordFocus2010Type();
			break;

		case FORD_CAR_2_TYPE:
			DecodeDataFordMondeo2015Type();
			break;

		case BMW_CAR_1_TYPE:
		case BMW_CAR_3_TYPE:
		case BMW_CAR_4_TYPE:
			DecodeDataBMWF15Type();
			break;
	
		case RENAULT_VAN_1_TYPE:
			DecodeDataRenaultMaster2010Type();
			break;

		case HONDA_CAR_1_TYPE:
			DecodeDataHondaCRV2012Type();
			break;

		case BMW_CAR_2_TYPE:
			DecodeDataBMWE84Type();
			break;

		case HONDA_CAR_2_TYPE:
			DecodeDataHondaCRV2007Type();
			break;

		case LR_CAR_2_TYPE:
			DecodeDataLandRoverDiscoSportType();
			break;

		case MERCEDES_CAR_2_TYPE:
			//DecodeDataMercVito2015Type();
			DecodeDataMercW176Type();
			break;

		case VAG_CAR_2_TYPE:
			DecodeDataTransporterT5Type();
			break;

		case VAG_CAR_3_TYPE:
			DecodeDataTransporterT6Type();
			break;
		
		case FIAT_CAR_1_TYPE:
			DecodeDataFiatDucatoE6Type();
			break;
	
		case VAG_CAR_4_TYPE:
			DecodeDataPorscheMacanType();
			break;

		case FERNO_TYPE1_TYPE:
			Decode_AceTechType();
			break;

	}
	
}

//Generic function - should call relavent function to interpret vehicles CAN data
void DecodeData2(void)
{
	switch(VehicleType)
	{
		case MERCEDES_CAR_1_TYPE:
			break;
		
		case MERCEDES_VAN_1_TYPE:
			DecodeData2MercSprinterType();
			break;

		case HONDA_CAR_1_TYPE:
			DecodeData2HondaCRV2012Type();
			break;

		case HONDA_CAR_2_TYPE:
			DecodeData2HondaCRV2007Type();
			break;
		case FIAT_CAR_1_TYPE:
			DecodeData2FiatDucatoE6Type();
			break;
		default:
			break;
	}
	
}


/*********************************************************
*	Function:	DetectVehicle
*	Notes:		The CAN Baud rate has been found. Need to 
*				incoming messages to see which vehicle library
*				matches the data IDs.
*				Search can be narrowed as the CAN Baud rate is
*				known.	 	
*
**********************************************************/
void DetectVehicle(void)
{
	
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	SID = message->msgSID.SID;


	//CAN Buad rate 125kbps
	if (uBaudSelector == 1)
	{
		Check_MercW176Type();
		Check_VolvoV70_2012Type();
		Check_FordFocus2010Type();
		Check_FordMondeo2015Type();
		Check_HondaCRV2012Type();
	}
	//CAN Baud rate 100kbps
	else if (uBaudSelector == 2)
	{
		Check_BMWE84Type();
		Check_TransporterT5Type();
		Check_AceTechType();
	}
	//CAN Baud rate 83.33 kbps
	else if (uBaudSelector == 3)
	{
		Check_MercSprinterType();
	}
	//CAN Baud rate 500kbps
	else if (uBaudSelector == 4)
	{
		Check_LRDisco4HSType(); //
		Check_SkodaScout2014Type();
		Check_BMWF15Type();		
		Check_BMWF45Type();
		Check_BMW5erGType();
		//Check_BMWF15Type();
		Check_RenaultMaster2010Type();
		Check_LRDiscoSportType();
		Check_TransporterT6Type();
		Check_PorscheMacanType();
	}
	//33.33kbps
	else if (uBaudSelector == 5)
	{
		Check_HondaCRV2007Type();
	}
	//250kbps
	else if (uBaudSelector == 6)
	{
		Check_MercVito2015Type();
	}
	//50kbps
	else if (uBaudSelector == 7)
	{
		Check_FiatDucatoE6Type();
	}

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}


void Check_AceTechType(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;

	ToggleLED_RED();

	SID = message->msgSID.SID;
	//If the message is of extended 29 bit type
	if (message->msgEID.IDE == 1)
	{
		SID = SID << 18;
		SID = SID | message->msgEID.EID;
	}
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == FERNO_TYPE1_CAN1_FILTER0)
		Check[0] = 1;
	//if (SID == FERNO_TYPE1_CAN1_FILTER1)
		Check[1] = 1;
	//if (SID == FERNO_TYPE1_CAN1_FILTER2)
		Check[2] = 1;
	//if (SID == FERNO_TYPE1_CAN1_FILTER3)
		Check[3] = 1;
	//if (SID == FERNO_TYPE1_CAN1_FILTER5)
		Check[4] = 1;
	//if (SID == FERNO_TYPE1_CAN1_FILTER6)
		Check[5] = 1;
	//if (SID == FERNO_TYPE1_CAN1_FILTER7)
		Check[6] = 1;
	//if (SID == FERNO_TYPE1_CAN1_FILTER8)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = FERNO_TYPE1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = FERNO_TYPE1_CAN1_FILTER0;
		FILTER1_VOL = FERNO_TYPE1_CAN1_FILTER1;
		FILTER2_VOL = FERNO_TYPE1_CAN1_FILTER2;
		FILTER3_VOL = FERNO_TYPE1_CAN1_FILTER3;
		FILTER4_VOL = FERNO_TYPE1_CAN1_FILTER4;
		FILTER5_VOL = FERNO_TYPE1_CAN1_FILTER5;
		FILTER6_VOL = FERNO_TYPE1_CAN1_FILTER6;
		FILTER7_VOL = FERNO_TYPE1_CAN1_FILTER7;
		FILTER8_VOL = FERNO_TYPE1_CAN1_FILTER8;
		FILTER9_VOL = FERNO_TYPE1_CAN1_FILTER9;
		FILTER10_VOL = FERNO_TYPE1_CAN1_FILTER10;
		FILTER11_VOL = FERNO_TYPE1_CAN1_FILTER11;
		FILTER12_VOL = FERNO_TYPE1_CAN1_FILTER12;
		FILTER13_VOL = FERNO_TYPE1_CAN1_FILTER13;
		FILTER14_VOL = FERNO_TYPE1_CAN1_FILTER14;
		FILTER15_VOL = FERNO_TYPE1_CAN1_FILTER15;

		FILTER0_EN = FERNO_TYPE1_CAN1_FILTER0_EN;
		FILTER1_EN = FERNO_TYPE1_CAN1_FILTER1_EN;
		FILTER2_EN = FERNO_TYPE1_CAN1_FILTER2_EN;
		FILTER3_EN = FERNO_TYPE1_CAN1_FILTER3_EN;
		FILTER4_EN = FERNO_TYPE1_CAN1_FILTER4_EN;
		FILTER5_EN = FERNO_TYPE1_CAN1_FILTER5_EN;
		FILTER6_EN = FERNO_TYPE1_CAN1_FILTER6_EN;
		FILTER7_EN = FERNO_TYPE1_CAN1_FILTER7_EN;
		FILTER8_EN = FERNO_TYPE1_CAN1_FILTER8_EN;
		FILTER9_EN = FERNO_TYPE1_CAN1_FILTER9_EN;
		FILTER10_EN = FERNO_TYPE1_CAN1_FILTER10_EN;
		FILTER11_EN = FERNO_TYPE1_CAN1_FILTER11_EN;
		FILTER12_EN = FERNO_TYPE1_CAN1_FILTER12_EN;
		FILTER13_EN = FERNO_TYPE1_CAN1_FILTER13_EN;
		FILTER14_EN = FERNO_TYPE1_CAN1_FILTER14_EN;
		FILTER15_EN = FERNO_TYPE1_CAN1_FILTER15_EN;

		//InitCAN1FiltersEIDNML();
		InitCAN1FiltersEID();
		SecondCANActive = 0;
	}

}

void Decode_AceTechType(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	static double FuelFilter = 0;
	uint8_t DATA[8];
	static uint8_t FuelFlow_Last;
	static uint8_t LeftCtr;
	static uint8_t RightCtr;
	static uint8_t Previous_Ignition;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//If the message is of extended 29 bit type
	if (message->msgEID.IDE == 1)
	{
		SID = SID << 18;
		SID = SID | message->msgEID.EID;
	}

//Template - CheckSet8(ByteNum, BITxx, Controls, BITxx);
//Template - CheckSet8Inv

	if (SID == 0x18FF7030)
	{
		if (DATA[0] == 0x01)
		{
			AceTechBluesBytes[0] = DATA[1];
			AceTechBluesBytes[1] = DATA[2];
			AceTechBluesBytes[2] = DATA[3];
			AceTechBluesBytes[3] = DATA[4];
			AceTechBluesBytes[4] = DATA[5];
			AceTechBluesBytes[5] = DATA[6];
			AceTechBluesBytes[6] = DATA[7];
			ToggleLED_RED();
		}

		if (DATA[0] == 0x02)
		{
			AceTechBattBytes[0] = DATA[1];
			AceTechBattBytes[1] = DATA[2];
			AceTechBattBytes[2] = DATA[3];
			AceTechBattBytes[3] = DATA[4];
			AceTechBattBytes[4] = DATA[5];
			ToggleLED_GREEN();
		}

		
	}

	AceTechActivity = 1;
	AceTechActioned = 0;

	CANActivity = 1;
	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}

void Check_RenaultMaster2010Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == RENAULT_VAN_1_CAN1_FILTER1) // 0x35D
		Check[0] = 1;
	if (SID == RENAULT_VAN_1_CAN1_FILTER2) //0x354
		Check[1] = 1;
	if (SID == RENAULT_VAN_1_CAN1_FILTER3)  //0x5C5
		Check[2] = 1;
	if (SID == RENAULT_VAN_1_CAN1_FILTER4) //0x60D
		Check[3] = 1;
	//if (SID == RENAULT_VAN_1_CAN1_FILTER5) //0x5C5
		Check[4] = 1;
	//if (SID == RENAULT_VAN_1_CAN1_FILTER6) //0x0C2
		Check[5] = 1;
	//if (SID == RENAULT_VAN_1_CAN1_FILTER7)
	Check[6] = 1;

	Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = RENAULT_VAN_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = RENAULT_VAN_1_CAN1_FILTER0;
		FILTER1_VOL = RENAULT_VAN_1_CAN1_FILTER1;
		FILTER2_VOL = RENAULT_VAN_1_CAN1_FILTER2;
		FILTER3_VOL = RENAULT_VAN_1_CAN1_FILTER3;
		FILTER4_VOL = RENAULT_VAN_1_CAN1_FILTER4;
		FILTER5_VOL = RENAULT_VAN_1_CAN1_FILTER5;
		FILTER6_VOL = RENAULT_VAN_1_CAN1_FILTER6;
		FILTER7_VOL = RENAULT_VAN_1_CAN1_FILTER7;
		FILTER8_VOL = RENAULT_VAN_1_CAN1_FILTER8;
		FILTER9_VOL = RENAULT_VAN_1_CAN1_FILTER9;
		FILTER10_VOL = RENAULT_VAN_1_CAN1_FILTER10;
		FILTER11_VOL = RENAULT_VAN_1_CAN1_FILTER11;
		FILTER12_VOL = RENAULT_VAN_1_CAN1_FILTER12;
		FILTER13_VOL = RENAULT_VAN_1_CAN1_FILTER13;
		FILTER14_VOL = RENAULT_VAN_1_CAN1_FILTER14;
		FILTER15_VOL = RENAULT_VAN_1_CAN1_FILTER15;

		FILTER0_EN = RENAULT_VAN_1_CAN1_FILTER0_EN;
		FILTER1_EN = RENAULT_VAN_1_CAN1_FILTER1_EN;
		FILTER2_EN = RENAULT_VAN_1_CAN1_FILTER2_EN;
		FILTER3_EN = RENAULT_VAN_1_CAN1_FILTER3_EN;
		FILTER4_EN = RENAULT_VAN_1_CAN1_FILTER4_EN;
		FILTER5_EN = RENAULT_VAN_1_CAN1_FILTER5_EN;
		FILTER6_EN = RENAULT_VAN_1_CAN1_FILTER6_EN;
		FILTER7_EN = RENAULT_VAN_1_CAN1_FILTER7_EN;
		FILTER8_EN = RENAULT_VAN_1_CAN1_FILTER8_EN;
		FILTER9_EN = RENAULT_VAN_1_CAN1_FILTER9_EN;
		FILTER10_EN = RENAULT_VAN_1_CAN1_FILTER10_EN;
		FILTER11_EN = RENAULT_VAN_1_CAN1_FILTER11_EN;
		FILTER12_EN = RENAULT_VAN_1_CAN1_FILTER12_EN;
		FILTER13_EN = RENAULT_VAN_1_CAN1_FILTER13_EN;
		FILTER14_EN = RENAULT_VAN_1_CAN1_FILTER14_EN;
		FILTER15_EN = RENAULT_VAN_1_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		CoolantTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_VolvoV70_2012Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == VOLVO_CAR_1_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == VOLVO_CAR_1_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == VOLVO_CAR_1_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == VOLVO_CAR_1_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == VOLVO_CAR_1_CAN1_FILTER5)
		Check[4] = 1;
	if (SID == VOLVO_CAR_1_CAN1_FILTER6)
		Check[5] = 1;
	if (SID == VOLVO_CAR_1_CAN1_FILTER7)
		Check[6] = 1;
	if (SID == VOLVO_CAR_1_CAN1_FILTER8)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = VOLVO_CAR_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = VOLVO_CAR_1_CAN1_FILTER0;
		FILTER1_VOL = VOLVO_CAR_1_CAN1_FILTER1;
		FILTER2_VOL = VOLVO_CAR_1_CAN1_FILTER2;
		FILTER3_VOL = VOLVO_CAR_1_CAN1_FILTER3;
		FILTER4_VOL = VOLVO_CAR_1_CAN1_FILTER4;
		FILTER5_VOL = VOLVO_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = VOLVO_CAR_1_CAN1_FILTER6;
		FILTER7_VOL = VOLVO_CAR_1_CAN1_FILTER7;
		FILTER8_VOL = VOLVO_CAR_1_CAN1_FILTER8;
		FILTER9_VOL = VOLVO_CAR_1_CAN1_FILTER9;
		FILTER10_VOL = VOLVO_CAR_1_CAN1_FILTER10;
		FILTER11_VOL = VOLVO_CAR_1_CAN1_FILTER11;
		FILTER12_VOL = VOLVO_CAR_1_CAN1_FILTER12;
		FILTER13_VOL = VOLVO_CAR_1_CAN1_FILTER13;
		FILTER14_VOL = VOLVO_CAR_1_CAN1_FILTER14;
		FILTER15_VOL = VOLVO_CAR_1_CAN1_FILTER15;

		FILTER0_EN = VOLVO_CAR_1_CAN1_FILTER0_EN;
		FILTER1_EN = VOLVO_CAR_1_CAN1_FILTER1_EN;
		FILTER2_EN = VOLVO_CAR_1_CAN1_FILTER2_EN;
		FILTER3_EN = VOLVO_CAR_1_CAN1_FILTER3_EN;
		FILTER4_EN = VOLVO_CAR_1_CAN1_FILTER4_EN;
		FILTER5_EN = VOLVO_CAR_1_CAN1_FILTER5_EN;
		FILTER6_EN = VOLVO_CAR_1_CAN1_FILTER6_EN;
		FILTER7_EN = VOLVO_CAR_1_CAN1_FILTER7_EN;
		FILTER8_EN = VOLVO_CAR_1_CAN1_FILTER8_EN;
		FILTER9_EN = VOLVO_CAR_1_CAN1_FILTER9_EN;
		FILTER10_EN = VOLVO_CAR_1_CAN1_FILTER10_EN;
		FILTER11_EN = VOLVO_CAR_1_CAN1_FILTER11_EN;
		FILTER12_EN = VOLVO_CAR_1_CAN1_FILTER12_EN;
		FILTER13_EN = VOLVO_CAR_1_CAN1_FILTER13_EN;
		FILTER14_EN = VOLVO_CAR_1_CAN1_FILTER14_EN;
		FILTER15_EN = VOLVO_CAR_1_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0x00;
		Doors = 0x00;
		SeatBelts = 0x00;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_MercW176Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == MERCEDES_CAR_1_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == MERCEDES_CAR_1_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == MERCEDES_CAR_1_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == MERCEDES_CAR_1_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == MERCEDES_CAR_1_CAN1_FILTER5)
		Check[4] = 1;
	if (SID == MERCEDES_CAR_1_CAN1_FILTER6)
		Check[5] = 1;
	if (SID == MERCEDES_CAR_1_CAN1_FILTER7)
		Check[6] = 1;
	if (SID == MERCEDES_CAR_1_CAN1_FILTER8)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = MERCEDES_CAR_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = MERCEDES_CAR_1_CAN1_FILTER0;
		FILTER1_VOL = MERCEDES_CAR_1_CAN1_FILTER1;
		FILTER2_VOL = MERCEDES_CAR_1_CAN1_FILTER2;
		FILTER3_VOL = MERCEDES_CAR_1_CAN1_FILTER3;
		FILTER4_VOL = MERCEDES_CAR_1_CAN1_FILTER4;
		FILTER5_VOL = MERCEDES_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = MERCEDES_CAR_1_CAN1_FILTER6;
		FILTER7_VOL = MERCEDES_CAR_1_CAN1_FILTER7;
		FILTER8_VOL = MERCEDES_CAR_1_CAN1_FILTER8;
		FILTER9_VOL = MERCEDES_CAR_1_CAN1_FILTER9;
		FILTER10_VOL = MERCEDES_CAR_1_CAN1_FILTER10;
		FILTER11_VOL = MERCEDES_CAR_1_CAN1_FILTER11;
		FILTER12_VOL = MERCEDES_CAR_1_CAN1_FILTER12;
		FILTER13_VOL = MERCEDES_CAR_1_CAN1_FILTER13;
		FILTER14_VOL = MERCEDES_CAR_1_CAN1_FILTER14;
		FILTER15_VOL = MERCEDES_CAR_1_CAN1_FILTER15;

		FILTER0_EN = MERCEDES_CAR_1_CAN1_FILTER0_EN;
		FILTER1_EN = MERCEDES_CAR_1_CAN1_FILTER1_EN;
		FILTER2_EN = MERCEDES_CAR_1_CAN1_FILTER2_EN;
		FILTER3_EN = MERCEDES_CAR_1_CAN1_FILTER3_EN;
		FILTER4_EN = MERCEDES_CAR_1_CAN1_FILTER4_EN;
		FILTER5_EN = MERCEDES_CAR_1_CAN1_FILTER5_EN;
		FILTER6_EN = MERCEDES_CAR_1_CAN1_FILTER6_EN;
		FILTER7_EN = MERCEDES_CAR_1_CAN1_FILTER7_EN;
		FILTER8_EN = MERCEDES_CAR_1_CAN1_FILTER8_EN;
		FILTER9_EN = MERCEDES_CAR_1_CAN1_FILTER9_EN;
		FILTER10_EN = MERCEDES_CAR_1_CAN1_FILTER10_EN;
		FILTER11_EN = MERCEDES_CAR_1_CAN1_FILTER11_EN;
		FILTER12_EN = MERCEDES_CAR_1_CAN1_FILTER12_EN;
		FILTER13_EN = MERCEDES_CAR_1_CAN1_FILTER13_EN;
		FILTER14_EN = MERCEDES_CAR_1_CAN1_FILTER14_EN;
		FILTER15_EN = MERCEDES_CAR_1_CAN1_FILTER15_EN;

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}
//Mercedes Vito has same raw data as Mercees W176 but at 250kbps
void Check_MercVito2015Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == MERCEDES_CAR_2_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == MERCEDES_CAR_2_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == MERCEDES_CAR_2_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == MERCEDES_CAR_2_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == MERCEDES_CAR_2_CAN1_FILTER5)
		Check[4] = 1;
	if (SID == MERCEDES_CAR_2_CAN1_FILTER6)
		Check[5] = 1;
	if (SID == MERCEDES_CAR_2_CAN1_FILTER7)
		Check[6] = 1;
	if (SID == MERCEDES_CAR_2_CAN1_FILTER8)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = MERCEDES_CAR_2_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = MERCEDES_CAR_2_CAN1_FILTER0;
		FILTER1_VOL = MERCEDES_CAR_2_CAN1_FILTER1;
		FILTER2_VOL = MERCEDES_CAR_2_CAN1_FILTER2;
		FILTER3_VOL = MERCEDES_CAR_2_CAN1_FILTER3;
		FILTER4_VOL = MERCEDES_CAR_2_CAN1_FILTER4;
		FILTER5_VOL = MERCEDES_CAR_2_CAN1_FILTER5;
		FILTER6_VOL = MERCEDES_CAR_2_CAN1_FILTER6;
		FILTER7_VOL = MERCEDES_CAR_2_CAN1_FILTER7;
		FILTER8_VOL = MERCEDES_CAR_2_CAN1_FILTER8;
		FILTER9_VOL = MERCEDES_CAR_2_CAN1_FILTER9;
		FILTER10_VOL = MERCEDES_CAR_2_CAN1_FILTER10;
		FILTER11_VOL = MERCEDES_CAR_2_CAN1_FILTER11;
		FILTER12_VOL = MERCEDES_CAR_2_CAN1_FILTER12;
		FILTER13_VOL = MERCEDES_CAR_2_CAN1_FILTER13;
		FILTER14_VOL = MERCEDES_CAR_2_CAN1_FILTER14;
		FILTER15_VOL = MERCEDES_CAR_2_CAN1_FILTER15;

		FILTER0_EN = MERCEDES_CAR_2_CAN1_FILTER0_EN;
		FILTER1_EN = MERCEDES_CAR_2_CAN1_FILTER1_EN;
		FILTER2_EN = MERCEDES_CAR_2_CAN1_FILTER2_EN;
		FILTER3_EN = MERCEDES_CAR_2_CAN1_FILTER3_EN;
		FILTER4_EN = MERCEDES_CAR_2_CAN1_FILTER4_EN;
		FILTER5_EN = MERCEDES_CAR_2_CAN1_FILTER5_EN;
		FILTER6_EN = MERCEDES_CAR_2_CAN1_FILTER6_EN;
		FILTER7_EN = MERCEDES_CAR_2_CAN1_FILTER7_EN;
		FILTER8_EN = MERCEDES_CAR_2_CAN1_FILTER8_EN;
		FILTER9_EN = MERCEDES_CAR_2_CAN1_FILTER9_EN;
		FILTER10_EN = MERCEDES_CAR_2_CAN1_FILTER10_EN;
		FILTER11_EN = MERCEDES_CAR_2_CAN1_FILTER11_EN;
		FILTER12_EN = MERCEDES_CAR_2_CAN1_FILTER12_EN;
		FILTER13_EN = MERCEDES_CAR_2_CAN1_FILTER13_EN;
		FILTER14_EN = MERCEDES_CAR_2_CAN1_FILTER14_EN;
		FILTER15_EN = MERCEDES_CAR_2_CAN1_FILTER15_EN;

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_MercSprinterType(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == MERCEDES_VAN_1_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == MERCEDES_VAN_1_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == MERCEDES_VAN_1_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == MERCEDES_VAN_1_CAN1_FILTER3)
		Check[3] = 1;
	//if (SID == MERCEDES_VAN_1_CAN1_FILTER4) //Data only comes in if indicators used
		Check[4] = 1;
	if (SID == MERCEDES_VAN_1_CAN1_FILTER5)
		Check[5] = 1;
	if (SID == MERCEDES_VAN_1_CAN1_FILTER6)
		Check[6] = 1;
	if (SID == MERCEDES_VAN_1_CAN1_FILTER7)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = MERCEDES_VAN_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = MERCEDES_VAN_1_CAN1_FILTER0;
		FILTER1_VOL = MERCEDES_VAN_1_CAN1_FILTER1;
		FILTER2_VOL = MERCEDES_VAN_1_CAN1_FILTER2;
		FILTER3_VOL = MERCEDES_VAN_1_CAN1_FILTER3;
		FILTER4_VOL = MERCEDES_VAN_1_CAN1_FILTER4;
		FILTER5_VOL = MERCEDES_VAN_1_CAN1_FILTER5;
		FILTER6_VOL = MERCEDES_VAN_1_CAN1_FILTER6;
		FILTER7_VOL = MERCEDES_VAN_1_CAN1_FILTER7;
		FILTER8_VOL = MERCEDES_VAN_1_CAN1_FILTER8;
		FILTER9_VOL = MERCEDES_VAN_1_CAN1_FILTER9;
		FILTER10_VOL = MERCEDES_VAN_1_CAN1_FILTER10;
		FILTER11_VOL = MERCEDES_VAN_1_CAN1_FILTER11;
		FILTER12_VOL = MERCEDES_VAN_1_CAN1_FILTER12;
		FILTER13_VOL = MERCEDES_VAN_1_CAN1_FILTER13;
		FILTER14_VOL = MERCEDES_VAN_1_CAN1_FILTER14;
		FILTER15_VOL = MERCEDES_VAN_1_CAN1_FILTER15;

		FILTER0_EN = MERCEDES_VAN_1_CAN1_FILTER0_EN;
		FILTER1_EN = MERCEDES_VAN_1_CAN1_FILTER1_EN;
		FILTER2_EN = MERCEDES_VAN_1_CAN1_FILTER2_EN;
		FILTER3_EN = MERCEDES_VAN_1_CAN1_FILTER3_EN;
		FILTER4_EN = MERCEDES_VAN_1_CAN1_FILTER4_EN;
		FILTER5_EN = MERCEDES_VAN_1_CAN1_FILTER5_EN;
		FILTER6_EN = MERCEDES_VAN_1_CAN1_FILTER6_EN;
		FILTER7_EN = MERCEDES_VAN_1_CAN1_FILTER7_EN;
		FILTER8_EN = MERCEDES_VAN_1_CAN1_FILTER8_EN;
		FILTER9_EN = MERCEDES_VAN_1_CAN1_FILTER9_EN;
		FILTER10_EN = MERCEDES_VAN_1_CAN1_FILTER10_EN;
		FILTER11_EN = MERCEDES_VAN_1_CAN1_FILTER11_EN;
		FILTER12_EN = MERCEDES_VAN_1_CAN1_FILTER12_EN;
		FILTER13_EN = MERCEDES_VAN_1_CAN1_FILTER13_EN;
		FILTER14_EN = MERCEDES_VAN_1_CAN1_FILTER14_EN;
		FILTER15_EN = MERCEDES_VAN_1_CAN1_FILTER15_EN;

		FILTER2_0_VOL = MERCEDES_VAN_1_CAN2_FILTER0;
		FILTER2_1_VOL = MERCEDES_VAN_1_CAN2_FILTER1;
		FILTER2_2_VOL = MERCEDES_VAN_1_CAN2_FILTER2;
		FILTER2_3_VOL = MERCEDES_VAN_1_CAN2_FILTER3;
		FILTER2_4_VOL = MERCEDES_VAN_1_CAN2_FILTER4;
		FILTER2_5_VOL = MERCEDES_VAN_1_CAN2_FILTER5;
		FILTER2_6_VOL = MERCEDES_VAN_1_CAN2_FILTER6;
		FILTER2_7_VOL = MERCEDES_VAN_1_CAN2_FILTER7;
		FILTER2_8_VOL = MERCEDES_VAN_1_CAN2_FILTER8;
		FILTER2_9_VOL = MERCEDES_VAN_1_CAN2_FILTER9;
		FILTER2_10_VOL = MERCEDES_VAN_1_CAN2_FILTER10;
		FILTER2_11_VOL = MERCEDES_VAN_1_CAN2_FILTER11;
		FILTER2_12_VOL = MERCEDES_VAN_1_CAN2_FILTER12;
		FILTER2_13_VOL = MERCEDES_VAN_1_CAN2_FILTER13;
		FILTER2_14_VOL = MERCEDES_VAN_1_CAN2_FILTER14;
		FILTER2_15_VOL = MERCEDES_VAN_1_CAN2_FILTER15;

		FILTER2_0_EN = MERCEDES_VAN_1_CAN2_FILTER0_EN;
		FILTER2_1_EN = MERCEDES_VAN_1_CAN2_FILTER1_EN;
		FILTER2_2_EN = MERCEDES_VAN_1_CAN2_FILTER2_EN;
		FILTER2_3_EN = MERCEDES_VAN_1_CAN2_FILTER3_EN;
		FILTER2_4_EN = MERCEDES_VAN_1_CAN2_FILTER4_EN;
		FILTER2_5_EN = MERCEDES_VAN_1_CAN2_FILTER5_EN;
		FILTER2_6_EN = MERCEDES_VAN_1_CAN2_FILTER6_EN;
		FILTER2_7_EN = MERCEDES_VAN_1_CAN2_FILTER7_EN;
		FILTER2_8_EN = MERCEDES_VAN_1_CAN2_FILTER8_EN;
		FILTER2_9_EN = MERCEDES_VAN_1_CAN2_FILTER9_EN;
		FILTER2_10_EN = MERCEDES_VAN_1_CAN2_FILTER10_EN;
		FILTER2_11_EN = MERCEDES_VAN_1_CAN2_FILTER11_EN;
		FILTER2_12_EN = MERCEDES_VAN_1_CAN2_FILTER12_EN;
		FILTER2_13_EN = MERCEDES_VAN_1_CAN2_FILTER13_EN;
		FILTER2_14_EN = MERCEDES_VAN_1_CAN2_FILTER14_EN;
		FILTER2_15_EN = MERCEDES_VAN_1_CAN2_FILTER15_EN;


		InitCAN1Filters();
		InitCAN2(500000, 1, 1, 4, 4);

		SecondCANActive = 1;
		//Initialise second CAN channel to pick up AdBlue information
	//	InitCAN2(5000000, 1, 1, 4, 4);
		InitCAN2Filters();
	}

}

void Check_FordMondeo2015Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type
	SendDebugMsg(9);
	if (SID == FORD_CAR_2_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == FORD_CAR_2_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == FORD_CAR_2_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == FORD_CAR_2_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == FORD_CAR_2_CAN1_FILTER4)
		Check[4] = 1;
	if (SID == FORD_CAR_2_CAN1_FILTER5)
		Check[5] = 1;
	if (SID == FORD_CAR_2_CAN1_FILTER6)
		Check[6] = 1;
	if (SID == FORD_CAR_2_CAN1_FILTER7)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = FORD_CAR_2_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = FORD_CAR_2_CAN1_FILTER0;
		FILTER1_VOL = FORD_CAR_2_CAN1_FILTER1;
		FILTER2_VOL = FORD_CAR_2_CAN1_FILTER2;
		FILTER3_VOL = FORD_CAR_2_CAN1_FILTER3;
		FILTER4_VOL = FORD_CAR_2_CAN1_FILTER4;
		FILTER5_VOL = FORD_CAR_2_CAN1_FILTER5;
		FILTER6_VOL = FORD_CAR_2_CAN1_FILTER6;
		FILTER7_VOL = FORD_CAR_2_CAN1_FILTER7;
		FILTER8_VOL = FORD_CAR_2_CAN1_FILTER8;
		FILTER9_VOL = FORD_CAR_2_CAN1_FILTER9;
		FILTER10_VOL = FORD_CAR_2_CAN1_FILTER10;
		FILTER11_VOL = FORD_CAR_2_CAN1_FILTER11;
		FILTER12_VOL = FORD_CAR_2_CAN1_FILTER12;
		FILTER13_VOL = FORD_CAR_2_CAN1_FILTER13;
		FILTER14_VOL = FORD_CAR_2_CAN1_FILTER14;
		FILTER15_VOL = FORD_CAR_2_CAN1_FILTER15;

		FILTER0_EN = FORD_CAR_2_CAN1_FILTER0_EN;
		FILTER1_EN = FORD_CAR_2_CAN1_FILTER1_EN;
		FILTER2_EN = FORD_CAR_2_CAN1_FILTER2_EN;
		FILTER3_EN = FORD_CAR_2_CAN1_FILTER3_EN;
		FILTER4_EN = FORD_CAR_2_CAN1_FILTER4_EN;
		FILTER5_EN = FORD_CAR_2_CAN1_FILTER5_EN;
		FILTER6_EN = FORD_CAR_2_CAN1_FILTER6_EN;
		FILTER7_EN = FORD_CAR_2_CAN1_FILTER7_EN;
		FILTER8_EN = FORD_CAR_2_CAN1_FILTER8_EN;
		FILTER9_EN = FORD_CAR_2_CAN1_FILTER9_EN;
		FILTER10_EN = FORD_CAR_2_CAN1_FILTER10_EN;
		FILTER11_EN = FORD_CAR_2_CAN1_FILTER11_EN;
		FILTER12_EN = FORD_CAR_2_CAN1_FILTER12_EN;
		FILTER13_EN = FORD_CAR_2_CAN1_FILTER13_EN;
		FILTER14_EN = FORD_CAR_2_CAN1_FILTER14_EN;
		FILTER15_EN = FORD_CAR_2_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0xFF;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}
}

void Check_FordFocus2010Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == FORD_CAR_1_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == FORD_CAR_1_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == FORD_CAR_1_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == FORD_CAR_1_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == FORD_CAR_1_CAN1_FILTER4)
		Check[4] = 1;
	if (SID == FORD_CAR_1_CAN1_FILTER5)
		Check[5] = 1;
	if (SID == FORD_CAR_1_CAN1_FILTER6)
		Check[6] = 1;
	if (SID == FORD_CAR_1_CAN1_FILTER7)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = FORD_CAR_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = FORD_CAR_1_CAN1_FILTER0;
		FILTER1_VOL = FORD_CAR_1_CAN1_FILTER1;
		FILTER2_VOL = FORD_CAR_1_CAN1_FILTER2;
		FILTER3_VOL = FORD_CAR_1_CAN1_FILTER3;
		FILTER4_VOL = FORD_CAR_1_CAN1_FILTER4;
		FILTER5_VOL = FORD_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = FORD_CAR_1_CAN1_FILTER6;
		FILTER7_VOL = FORD_CAR_1_CAN1_FILTER7;
		FILTER8_VOL = FORD_CAR_1_CAN1_FILTER8;
		FILTER9_VOL = FORD_CAR_1_CAN1_FILTER9;
		FILTER10_VOL = FORD_CAR_1_CAN1_FILTER10;
		FILTER11_VOL = FORD_CAR_1_CAN1_FILTER11;
		FILTER12_VOL = FORD_CAR_1_CAN1_FILTER12;
		FILTER13_VOL = FORD_CAR_1_CAN1_FILTER13;
		FILTER14_VOL = FORD_CAR_1_CAN1_FILTER14;
		FILTER15_VOL = FORD_CAR_1_CAN1_FILTER15;

		FILTER0_EN = FORD_CAR_1_CAN1_FILTER0_EN;
		FILTER1_EN = FORD_CAR_1_CAN1_FILTER1_EN;
		FILTER2_EN = FORD_CAR_1_CAN1_FILTER2_EN;
		FILTER3_EN = FORD_CAR_1_CAN1_FILTER3_EN;
		FILTER4_EN = FORD_CAR_1_CAN1_FILTER4_EN;
		FILTER5_EN = FORD_CAR_1_CAN1_FILTER5_EN;
		FILTER6_EN = FORD_CAR_1_CAN1_FILTER6_EN;
		FILTER7_EN = FORD_CAR_1_CAN1_FILTER7_EN;
		FILTER8_EN = FORD_CAR_1_CAN1_FILTER8_EN;
		FILTER9_EN = FORD_CAR_1_CAN1_FILTER9_EN;
		FILTER10_EN = FORD_CAR_1_CAN1_FILTER10_EN;
		FILTER11_EN = FORD_CAR_1_CAN1_FILTER11_EN;
		FILTER12_EN = FORD_CAR_1_CAN1_FILTER12_EN;
		FILTER13_EN = FORD_CAR_1_CAN1_FILTER13_EN;
		FILTER14_EN = FORD_CAR_1_CAN1_FILTER14_EN;
		FILTER15_EN = FORD_CAR_1_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0xFF;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}
}



void DecodeDataRenaultMaster2010Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	static double FuelFilter = 0;
	uint8_t DATA[8];
	static uint8_t FuelFlow_Last;
	static uint8_t LeftCtr;
	static uint8_t RightCtr;
	static uint8_t Previous_Ignition;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

//Template - CheckSet8(ByteNum, BITxx, Controls, BITxx);
//Template - CheckSet8Inv
	//RPM
	//Accelerator
	//Footbrake
	//Clutch
	//Brake lights
	if (SID == 0x181)
	{
		//RPM
		Tempu16 = message->data[0];

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[1];
		//RPM Data is 4 x  actual value
		if (Tempu16 != 0xFFFF)
		{
			Tempu16 = Tempu16 / 8;

			//Holds RPM data - no scaling yet.
			Tempu16 = (Tempu16 + 25) / 50;	

			RPM = Tempu16 & 0xFF;
		}
		else RPM = 0x00;

		//Accelerator
		Tempu8 = message->data[3];
		Tempu8 -= 16;
	
		TempDouble = (double)Tempu8 / 2.33;
		
		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Accelerator = Tempu8;

		//Clutch
		CheckSet8(DATA[5], BIT3, &Controls, BIT1);
		//Brake Lights
		CheckSet16(DATA[5], BIT0, &Lights, BIT6);

	}

	//fuel flow
	if (SID == 0x551)
	{
		Tempu8 = message->data[1];

		if ((RPM < 0x6)||(RPM==0xFF)) FuelFlow_Last = Tempu8;
		if ((Ignition & BIT2) != BIT2) FuelFlow_Last = Tempu8;
		//if engine not renning, no fuel used but data might reset

		if (Tempu8 < FuelFlow_Last)
		{
			Tempu8 = Tempu8 + (0xFF - FuelFlow_Last);
		}
		else
		{
			Tempu8 = Tempu8 - FuelFlow_Last;
		}

		if (FuelFlowCount > (0xFFFE - Tempu8))
		{
			//Rollover
			FuelFlowCount +=  Tempu8 + 1;
			//makes sure it rolls over at 0xFFFE not 0xFFFF
		}
		else
		{
		//Normal
			FuelFlowCount += Tempu8;
		}

		FuelFlow_Last = message->data[1];
	}

	//Ignition
	//Wipers
	if (SID == 0x35D)
	{
		//Full Ignition
		CheckSet8(DATA[0], BIT7, &Ignition, BIT2);
		//Cranking
		CheckSet8(DATA[0], BIT6, &Ignition, BIT3);
		//Wipers
		if ((DATA[2] & 0b11100000) == 0xC0)
		{
			Tempu8 = Wipers;
			Tempu8 &= 0b11111000;
			Tempu8 |= 0x03;
			Wipers = Tempu8;
		}
		else if ((DATA[2] & 0b11100000) == 0xE0)
		{
			Tempu8 = Wipers;
			Tempu8 &= 0b11111000;
			Tempu8 |= 0x04;
			Wipers = Tempu8;
		}
		else
		{
			Tempu8 = Wipers;
			Tempu8 &= 0b11111000;
			Wipers = Tempu8;
		}

	}

	//Speed
	if (SID == 0x354)
	{
		Tempu16 = message->data[0] & 0x7F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[1];
	
		if (Tempu16 != 0xFFFF)	RoadSpeed = Tempu16 / 100;
	}

	//Handbrake
	if (SID == 0x5C5)
	{
		CheckSet8(DATA[0], BIT2, &Controls, BIT0);
	}

	//Lights
	//Doors
	//Coolant
	//Ambient
	if (SID == 0x60D)
	{
		//Dipped Beam
		CheckSet16(DATA[0], BIT1, &Lights, BIT2);
		//Sidelights
		CheckSet16(DATA[0], BIT2, &Lights, BIT1);
		//NSF Door (Left Front Door)
		CheckSet8(DATA[0], BIT3, &Doors, BIT1);
		//OSF Door (Right Front Door)
		CheckSet8(DATA[0], BIT4, &Doors, BIT0);
		//N/S Side Load Door
		CheckSet8(DATA[0], BIT5, &Doors, BIT3);
		//O/S Side Load Door (Europe?)
		CheckSet8(DATA[0], BIT6, &Doors, BIT2);
		//Rear Door
		CheckSet8(DATA[0], BIT7, &Doors, BIT4);

		CheckSet8(DATA[1], BIT1, &Ignition, BIT1);

		//Front Fog Lights
		CheckSet16(DATA[1], BIT0, &Lights, BIT4);
		//Main Beam
		CheckSet16(DATA[1], BIT3, &Lights, BIT3);
		//Rear Fog Lights
		CheckSet16(DATA[2], BIT2, &Lights, BIT5);
		//Reverse Lights
		CheckSet16(DATA[6], BIT4, &Lights, BIT7);	

		Tempu8 = DATA[5];
		CoolantTemp = Tempu8;

		Tempu8 = DATA[4];
		AmbientTemp = Tempu8;


		//Left Indicator
		if ((DATA[1] & BIT5) == BIT5)
		{
			Lights = Lights | BIT8;
			LeftCtr = 0;
		}
		else 
		{
			if (LeftCtr>20)
				Lights = Lights &~ BIT8;
			else
				LeftCtr++;
		}
		//Right Indicator
		if ((DATA[1] & BIT6) == BIT6)
		{
			Lights = Lights | BIT9;
			RightCtr = 0;
		}
		else 
		{
			if (RightCtr>20)
				Lights = Lights &~ BIT9;
			else
				RightCtr++;
		}
		//Message comes in every 100ms therefore 2s before indicatos sensed as off

	}

	//Odometer
	//Fuel?
	if (SID == 0x5C5)
	{
			Tempu32 = message->data[1];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[2];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[3];
		
			if (Tempu32 != 0xFFFFFF) Odometer = Tempu32;

			Tempu8 = (message->data[7]&0x7F);

			if ((Ignition & BIT2) == BIT2)
			{
				
			//	if (Previous_Ignition == FALSE)
			//		FuelFilter = Tempu8;
			//	else
			//	{

					if (Tempu8 > (FuelFilter - 7)) 
						FuelFilter =  ((0.005 * (double)Tempu8) + (0.995 * FuelFilter));
					else
						FuelFilter =  ((0.001 * (double)Tempu8) + (0.999 * FuelFilter));
			//	}

				//FuelLevel = Tempu8 * 8;
				FuelLevel = (uint16_t)FuelFilter * 8;

				if (FuelLevel < 80)
					MILLights = MILLights | BIT4;
				else
					MILLights = MILLights & ~BIT4;	

				MILLights = MILLights & 0b00010000;

				Previous_Ignition = TRUE;

			}
			else
				Previous_Ignition = FALSE;

	}


	//Steering Wheel
	if (SID == 0x0C2)
	{
		Tempu16 = message->data[0];
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[1];

		//0x8000 Centre
		//0x6599 Right
		//0x9A16 Left

	
		if (Tempu16 > 0x8000)
		{
			TempDouble = (double)Tempu16 - 0x8000;
			TempDouble = TempDouble / 10;

			SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
			SteeringWheel = SteeringWheel & ~BIT15;
		}
		else
		{
			TempDouble = 0x8000 - (double)Tempu16;
			TempDouble = TempDouble / 10;
			SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
			SteeringWheel = SteeringWheel | BIT15;
		}

		

	}

	CANActivity = 1;
	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}


void DecodeDataFordFocus2010Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t FuelFlow_Last;
	static uint8_t LeftCtr;
	static uint8_t RightCtr;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

//Template - CheckSet8(ByteNum, BITxx, Controls, BITxx);
	//RPM
	//Road Speed
	if (SID == 0x110)
	{
		Tempu16 = message->data[4] & 0x3F;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[5];
		//RPM Data is half actual value
		Tempu16 = Tempu16 * 2;

		//Holds RPM data - no scaling yet.
		Tempu16 = (Tempu16 + 25) / 50;

		RPM = Tempu16 & 0xFF;

		Tempu16 = message->data[6] & 0x7F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[7];
	
		RoadSpeed = Tempu16 / 100;
	}
	//Odometer
	if (SID == 0x290)
	{
			Tempu32 = message->data[5];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[6];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[7];

			Odometer = Tempu32;
	}

	//Fuel Level
	if (SID == 0x2E0)
	{
			Tempu16 = message->data[2] & 0x01;
			Tempu16 = Tempu16 << 8;
			Tempu16 = Tempu16 | message->data[3];
		


			if ((Ignition & BIT2) == BIT2)
			{
				FuelLevel = Tempu16 * 2;

				if (FuelLevel < 80)
					MILLights = MILLights | BIT4;
				else
					MILLights = MILLights & ~BIT4;	

				MILLights = MILLights & 0b00010000;
			}
	}
//Fuel flow
	if (SID == 0x1A8)
	{
		Tempu8 = message->data[4];

		if ((RPM < 0x6)||(RPM==0xFF)) FuelFlow_Last = Tempu8;
		if ((Ignition & BIT2) != BIT2) FuelFlow_Last = Tempu8;
		//if engine not renning, no fuel used but data might reset

		if (Tempu8 < FuelFlow_Last)
		{
			Tempu8 = Tempu8 + (0xFF - FuelFlow_Last);
		}
		else
		{
			Tempu8 = Tempu8 - FuelFlow_Last;
		}

		if (FuelFlowCount > (0xFFFE - Tempu8))
		{
		//Rollover
			FuelFlowCount +=  Tempu8 + 1;
			//makes sure it rolls over at 0xFFFE not 0xFFFF
		}
		else
		{
		//Normal
			FuelFlowCount += Tempu8;
		}

		FuelFlow_Last = message->data[4];
		
		Wipers = 0;
		
		switch (message->data[1] & 0x06)
		{
			case 2:
				Wipers = Wipers | 0x03;
				break;
			case 6:
				Wipers = Wipers | 0x04;
				break;
		}
		//Main Beam
		CheckSet16(DATA[1], BIT0, &Lights, BIT3);
		//Rear Fogs
		CheckSet16(DATA[2], BIT4, &Lights, BIT5);

	}
	//Ignition
	//Sidelights
	//Doors
	if (SID == 0x080)
	{
		//Accessory
		CheckSet8(DATA[5],BIT2, &Ignition, BIT1);
		//Ignition
		CheckSet8(DATA[5],BIT1, &Ignition, BIT2);
		//Cranking
		CheckSet8(DATA[5],BIT3, &Ignition, BIT3);

		//Sidelights
		CheckSet16(DATA[1], BIT7, &Lights, BIT0);
		//OSF
		CheckSet8Inv(DATA[3], BIT0, &Doors, BIT0);
		//NSF
		CheckSet8Inv(DATA[3], BIT1, &Doors, BIT1);
		//NSR
		CheckSet8Inv(DATA[3], BIT2, &Doors, BIT3);
		//OSR
		CheckSet8Inv(DATA[3], BIT3, &Doors, BIT2);		
		//Tailgate
		CheckSet8Inv(DATA[3], BIT4, &Doors, BIT4);		
		//Bonnet
		CheckSet8Inv(DATA[3], BIT5, &Doors, BIT5);		
		
	
	}
	//Indicators
	//Reverse
	if (SID == 0x03A)
	{
		if ((message->data[1] & BIT2) == BIT2)
		{
			Lights = Lights | BIT8;
			LeftCtr = 0;
		}
		else 
		{
			if (LeftCtr>20)
				Lights = Lights &~ BIT8;
			else
				LeftCtr++;
		}

		if ((message->data[1] & BIT3) == BIT3)
		{
			Lights = Lights | BIT9;
			RightCtr = 0;
		}
		else 
		{
			if (RightCtr>20)
				Lights = Lights &~ BIT9;
			else
				RightCtr++;
		}
		//Reverse Lights
		CheckSet16(DATA[4],BIT5, &Lights, BIT7);
		
	}
	//Steering Wheel
	if (SID == 0x2A0)
	{
		Tempu16 = message->data[6] & 0x7F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[7];
	
		TempDouble = ((double) Tempu16 / 22.75f) + 0.5f;

		SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;;
 			
		if ((message->data[4] & BIT5) == BIT5) SteeringWheel = SteeringWheel | BIT15;
			else SteeringWheel = SteeringWheel &~ BIT15;

	}
	//Dipped Beam
	if (SID == 0x1E0)
	{
		//Dipped Beam
		CheckSet16(DATA[0], BIT7, &Lights, BIT2);
	}
	//Brake Lights
	if (SID == 0x361)
	{
		//Brake Lights
		CheckSet16(DATA[0], BIT3, &Lights, BIT6);
	}
	//Handbrake
	if (SID == 0x240)
	{
		//Handbrake
		CheckSet8(DATA[3], BIT7, &Controls, BIT0);
	}
	//VIN
	if (SID == 0x40A)
	{
		if (message->data[0] == 0xC1)
		{
			switch (message->data[1])
			{
				case 0:
					VIN[0] = message->data[2];
					VIN[1] = message->data[3];
					VIN[2] = message->data[4];
					VIN[3] = message->data[5];
					VIN[4] = message->data[6];
					VIN[5] = message->data[7];
					break;
				case 1:
					VIN[6] = message->data[2];
					VIN[7] = message->data[3];
					VIN[8] = message->data[4];
					VIN[9] = message->data[5];
					VIN[10] = message->data[6];
					VIN[11] = message->data[7];
					break;
				case 2:
					VIN[12] = message->data[2];
					VIN[13] = message->data[3];
					VIN[14] = message->data[4];
					VIN[15] = message->data[5];
					VIN[16] = message->data[6];
					break;
			}
		}
	}




	CANActivity = 1;
	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}

void DecodeDataFordMondeo2015Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t FuelFlow_Last;
	static uint8_t LeftCtr;
	static uint8_t RightCtr;
	//static uint8_t FuelFlow_Last;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

//Template - CheckSet8(ByteNum, BITxx, Controls, BITxx);
//Template - CheckSet8(ByteNum, BITxx, Controls, BITxx);
	//RPM
	if (SID == 0x078)
	{
		Tempu16 = message->data[5] & 0x3F;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[6];

		//Holds RPM data - no scaling yet.
		Tempu16 = (Tempu16 + 25) / 50;

		RPM = Tempu16 & 0xFF;

	}
	//Road Speed
	if (SID == 0x084)
	{
		Tempu16 = message->data[1] & 0x7F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[2];
	
		RoadSpeed = Tempu16 / 100;
	}

	//Odometer
	if (SID == 0x230)
	{
			Tempu32 = message->data[0];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[1];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[2];

			Odometer = Tempu32;


	}

	//Fuel Level
	if (SID == 0x060)
	{

		Tempu16 = message->data[3] & 0x01;
		Tempu16 = Tempu16 << 8;
		Tempu16 = Tempu16 | message->data[4];
		

		if ((Ignition & BIT2) == BIT2)
		{
			FuelLevel = Tempu16 * 2;

			if (FuelLevel < 80)
				MILLights = MILLights | BIT4;
			else
				MILLights = MILLights & ~BIT4;	

			MILLights = MILLights & 0b00010000;
		}

	}

//Fuel flow
//Sidelights
//Reverse
	if (SID == 0x050)
	{
		Tempu8 = message->data[5];

		if ((RPM < 0x6)||(RPM==0xFF)) FuelFlow_Last = Tempu8;
		if ((Ignition & BIT2) != BIT2) FuelFlow_Last = Tempu8;
		//if engine not renning, no fuel used but data might reset

		if (Tempu8 < FuelFlow_Last)
		{
			Tempu8 = Tempu8 + (0xFF - FuelFlow_Last);
		}
		else
		{
			Tempu8 = Tempu8 - FuelFlow_Last;
		}

		if (FuelFlowCount > (0xFFFE - Tempu8))
		{
		//Rollover
			FuelFlowCount +=  Tempu8 + 1;
			//makes sure it rolls over at 0xFFFE not 0xFFFF
		}
		else
		{
		//Normal
			FuelFlowCount += Tempu8;
		}

		FuelFlow_Last = message->data[5];

		//Sidelights
		CheckSet16(DATA[7], BIT7, &Lights, BIT0);
		//Reverse
		CheckSet16(DATA[7], BIT4, &Lights, BIT7);

	}
	//Ignition
	//Brakelights
	if (SID == 0x048)
	{
		//Accessory
		CheckSet8(DATA[4],BIT2, &Ignition, BIT1);
		//Ignition
		CheckSet8(DATA[4],BIT1, &Ignition, BIT2);
		//Cranking
		CheckSet8(DATA[4],BIT3, &Ignition, BIT3);

		//Brakelights
		CheckSet16(DATA[2], BIT0, &Lights, BIT6);
	
	}
//Doors
//Indicators
//Handbrake
//Main Beam
	if (SID == 0x06B)
	{
		//OSF
		CheckSet8Inv(DATA[6], BIT0, &Doors, BIT0);
		//NSF
		CheckSet8Inv(DATA[6], BIT1, &Doors, BIT1);
		//NSR
		CheckSet8Inv(DATA[6], BIT2, &Doors, BIT3);
		//OSR
		CheckSet8Inv(DATA[6], BIT3, &Doors, BIT2);		
		//Tailgate
		CheckSet8Inv(DATA[6], BIT4, &Doors, BIT4);		
		//Bonnet
		CheckSet8Inv(DATA[6], BIT5, &Doors, BIT5);		

		//Handbrake
		CheckSet8(DATA[5], BIT4, &Controls, BIT0);
		//Main Beam
		CheckSet16(DATA[5], BIT2, &Lights, BIT3);
		//Rear fogs added after FW5
		CheckSet16(DATA[2], BIT4, &Lights, BIT5);

		if ((message->data[7] & BIT0) == BIT0)
		{
			Lights = Lights | BIT8;
			LeftCtr = 0;
		}
		else 
		{
			if (LeftCtr>20)
				Lights = Lights &~ BIT8;
			else
				LeftCtr++;
		}

		if ((message->data[7] & BIT1) == BIT1)
		{
			Lights = Lights | BIT9;
			RightCtr = 0;
		}
		else 
		{
			if (RightCtr>20)
				Lights = Lights &~ BIT9;
			else
				RightCtr++;
		}

	}

	//Steering Wheel
	if (SID == 0x480)
	{
		Tempu16 = message->data[6] & 0x7F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[7];
	
		TempDouble = ((double) Tempu16 / 22.75f) + 0.5f;

		SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;;
 			
		if ((message->data[6] & BIT7) == BIT7) SteeringWheel = SteeringWheel | BIT15;
			else SteeringWheel = SteeringWheel &~ BIT15;

	}
	//Dipped Beam
	if (SID == 0x433)
	{
		//Dipped Beam
		CheckSet16(DATA[3], BIT7, &Lights, BIT2);
	}
	//Wipers
	if (SID == 0x010)
	{
		Wipers = 0;
		
		switch (message->data[0] & 0x03)
		{
			case 1:
				Wipers = Wipers | 0x03;
				break;
			case 3:
				Wipers = Wipers | 0x04;
				break;
		}

	}

	//VIN
	if (SID == 0x405)
	{
			switch (message->data[0])
			{
				case 0x10:
					VIN[0] = message->data[5];
					VIN[1] = message->data[6];
					VIN[2] = message->data[7];
	
					break;
				case 0x11:
					VIN[3] = message->data[1];
					VIN[4] = message->data[2];
					VIN[5] = message->data[3];
					VIN[6] = message->data[4];
					VIN[7] = message->data[5];
					VIN[8] = message->data[6];
					VIN[9] = message->data[7];

					break;
				case 0x12:
					VIN[10] = message->data[1];
					VIN[11] = message->data[2];
					VIN[12] = message->data[3];
					VIN[13] = message->data[4];
					VIN[14] = message->data[5];
					VIN[15] = message->data[6];
					VIN[16] = message->data[7];
					break;
			}
		
	}




	CANActivity = 1;
	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}

void DecodeDataVolvoV702012Type(void)
{

	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t FuelFlow_Last;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

//RPM
//Brake lights
	if (SID == 0x155)
	{
		Tempu16 = message->data[4] & 0x3F;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[5];

		//Holds RPM data - no scaling yet.
		Tempu16 = (Tempu16 + 25) / 50;

		RPM = Tempu16 & 0xFF;

		if ((message->data[1] & BIT2) == BIT2) Lights = Lights | BIT6;
			else Lights = Lights & ~BIT6;
	}
//Road Speed
	if (SID == 0x145)
	{
			Tempu16 = message->data[4] & 0x3F;
			Tempu16 = Tempu16<<8;
			Tempu16 |= message->data[5];
	
			RoadSpeed = Tempu16 / 100;
	}
//Odometer
	if (SID == 0x385)
	{
			Tempu32 = message->data[5];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[6];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[7];

			Odometer = Tempu32;
	}
//Manual Gearbox
//Main beam, Rear fogs
	if (SID == 0x205)
	{

		//Main Beam
		if ((message->data[2] & BIT5) == BIT5) Lights = Lights | BIT3;
			else Lights = Lights & ~BIT3;
		//Rear Fogs
		if ((message->data[2] & BIT3) == BIT3) Lights = Lights | BIT5;
			else Lights = Lights &~ BIT5;
		if ((message->data[5] & BIT6) == BIT6) Lights = Lights | BIT7;
			else Lights = Lights &~BIT7;


		Tempu8 = message->data[6];
		Tempu8 = ((Tempu8 >> 4) & 0x07);
		ManualBox = Tempu8;

	}
//Fuel Level
	if (SID == 0x3D5)
	{
			Tempu16 = message->data[2] & 0x01;
			Tempu16 = Tempu16 << 8;
			Tempu16 = Tempu16 | message->data[3];
		


			if ((Ignition & BIT2) == BIT2)
			{

				FuelLevel = Tempu16 * 2;

				if (FuelLevel < 80)
					MILLights = MILLights | BIT4;
				else
					MILLights = MILLights & ~BIT4;	

				MILLights = MILLights & 0b00010000;
			}
	}
//Fuel flow
	if (SID == 0x375)
	{
		Tempu8 = message->data[4];

		if ((RPM < 1)||(RPM==0xFF)) FuelFlow_Last = Tempu8;
		//if engine not renning, no fuel used but data might reset

		if (Tempu8 < FuelFlow_Last)
		{
			Tempu8 = Tempu8 + (0xFF - FuelFlow_Last);
		}
		else
		{
			Tempu8 = Tempu8 - FuelFlow_Last;
		}

		if (FuelFlowCount > (0xFFFE - Tempu8))
		{
		//Rollover
			FuelFlowCount +=  Tempu8 + 1;
			//makes sure it rolls over at 0xFFFE not 0xFFFF
		}
		else
		{
		//Normal
			FuelFlowCount += Tempu8;
		}

		FuelFlow_Last = message->data[4];
		
	}
//Coolant temp
	if (SID == 0x4C5)
	{
		Tempu8 = message->data[5];
//Data is temp + 50h
		if (Tempu8 == 0xFF) CoolantTemp = 0xFF;
		else
			CoolantTemp = Tempu8 - 10;
	}
//Ignition
//Front washer
//Handbrake
	if (SID == 0x185)
	{
		//Ignition
		if ((message->data[1] & BIT4) == BIT4) Ignition = Ignition | BIT2;
			else Ignition = Ignition &~ BIT2;
	//	if ((message->data[1] & BIT6) == BIT6) Ignition = Ignition | BIT4;
	//		else Ignition = Ignition &~ BIT4;
//new bit of data to indicate engine run state (even if auto start/stop has stalled engine)
		//Handbrake
		if ((message->data[6] & BIT5) == BIT5) Controls = Controls | BIT0;
			else Controls = Controls &~ BIT0;
		//Front wash
		if ((message->data[0] & BIT0) == BIT0) Wipers = Wipers | BIT5;
			else Wipers = Wipers &~ BIT5;
		if ((message->data[1] & BIT6) == BIT6) Lights = Lights | BIT0;
			else Lights = Lights &~BIT0;


	}
//Indicators
	if (SID == 0x035)
	{
		//Left
		if ((message->data[0] & BIT5) == BIT5) Lights = Lights | BIT8;
			else Lights = Lights &~ BIT8;
		//Right
		if ((message->data[0] & BIT6) == BIT6) Lights = Lights | BIT9;
			else Lights = Lights &~ BIT9;
	}
//Front wipers
	if (SID == 0x395)
	{
		Wipers = 0;
		
		switch (message->data[7] & 0x06)
		{
			case 2:
				Wipers = Wipers | 0x01;
				break;
			case 4:
				Wipers = Wipers | 0x03;
				break;
			case 6:
				Wipers = Wipers | 0x04;
				break;

		}
		//Rear wash
		if ((message->data[7] & BIT0) == BIT0) Wipers = Wipers | BIT6;
			else Wipers = Wipers &~ BIT6;
		//Manual
		if ((message->data[7] & BIT3) == BIT3) Wipers = Wipers | BIT3;
			else Wipers = Wipers &~ BIT3;
		
	}
//Steering wheel
	if (SID == 0x2D5)
	{
		Tempu16 = message->data[3] & 0x7F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[4];
		//If Right
	
		TempDouble = ((double) Tempu16 / 22.75f) + 0.5f;

		SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;;
 			
		if ((message->data[1] & BIT6) == BIT6) SteeringWheel = SteeringWheel | BIT15;
			else SteeringWheel = SteeringWheel &~ BIT15;
		
	}
//Dipped beam
	if (SID == 0x2B5)
	{
		if ((message->data[0] & BIT4) == BIT4) Lights = Lights | BIT2;
			else Lights = Lights &~ BIT2;
	}
//Auto Gearbox
	if (SID == 0x0F5)
	{
		switch (message->data[1] & 0x0C)
		{
			case 0:
				AutoBox = BIT0;
				//Lights = Lights & ~BIT7;
				break;
			case 0x8:
				AutoBox = BIT2;
				//Lights = Lights & ~BIT7;
				break;
			case 0xC:
				AutoBox = BIT3;
				//Lights = Lights | BIT7;
				break;
			case 0x4:
				AutoBox = BIT1;
				//Lights = Lights & ~BIT7;
				break;
			default:
				AutoBox = 0xFF;
				//Lights = Lights & ~BIT7;
		
		}
	}
//Door status
	if (SID == 0x0E5)
	{
		if ((BIT0 & DATA[0]) == BIT0) Doors = Doors &~ BIT0;
			else Doors = Doors | BIT0;

		if ((BIT1 & DATA[0]) == BIT1) Doors = Doors &~ BIT1;
			else Doors = Doors | BIT1;

		if ((BIT2 & DATA[0]) == BIT2) Doors = Doors &~ BIT3;
			else Doors = Doors | BIT3;

		if ((BIT3 & DATA[0]) == BIT3) Doors = Doors &~ BIT2;
			else Doors = Doors | BIT2;

		if ((BIT4 & DATA[0]) == BIT4) Doors = Doors &~ BIT4;
			else Doors = Doors | BIT4;

		if ((BIT5 & DATA[0]) == BIT5) Doors = Doors &~ BIT5;
			else Doors = Doors | BIT5;
	}
//seat belt status
	if (SID == 0x115)
	{
		if ((BIT6 & DATA[4]) == BIT6) SeatBelts = SeatBelts | BIT0;
			else SeatBelts = SeatBelts &~ BIT0;

		if ((BIT0 & DATA[4]) == BIT0) SeatBelts = SeatBelts | BIT1;
			else SeatBelts = SeatBelts &~ BIT1;

		if ((BIT4 & DATA[5]) == BIT4) SeatBelts = SeatBelts | BIT2;
			else SeatBelts = SeatBelts &~ BIT2;

		if ((BIT4 & DATA[6]) == BIT4) SeatBelts = SeatBelts | BIT3;
			else SeatBelts = SeatBelts &~ BIT3;

		if ((BIT0 & DATA[5]) == BIT0) SeatBelts = SeatBelts | BIT4;
			else SeatBelts = SeatBelts &~ BIT4;


	}



	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}

void DecodeDataMercW176Type(void)
{

	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//message->data[x]

	//Ignition / Key State
	if (SID == 0x001)
	{
		//Key In
		if ((message->data[0] & BIT0) == BIT0) Ignition = Ignition | BIT0;
			else Ignition = Ignition & ~BIT0;
		
		//Aux Ignition
		if ((message->data[0] & BIT1) == BIT1) Ignition = Ignition | BIT1;
			else Ignition = Ignition & ~BIT1;

		//Full Ignition
		if ((message->data[0] & BIT2) == BIT2) Ignition = Ignition | BIT2;
			else Ignition = Ignition & ~BIT2;

		//Cranking
		if ((message->data[0] & 0x5) == 0x5) Ignition = Ignition | BIT3;
			else Ignition = Ignition & ~BIT3;

		Ignition = Ignition & 0b00001111;
		
	}
	//Steering Wheel
	else if (SID == 0x003)
	{
		Tempu16 = message->data[0] & 0x1F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[1];
		//If Right
		if (Tempu16 > 0x1000)
		{
			Tempu16 = (Tempu16 - 0x1000) / 2;
			SteeringWheel = Tempu16 & 0x3FF;
 			SteeringWheel = SteeringWheel | BIT15;
		}
		//If Left
		else
		{
			Tempu16 = (0x1000 - Tempu16) / 2;
			SteeringWheel = Tempu16 & 0x3FF;
			SteeringWheel = SteeringWheel & ~BIT15;
		}

	}
	//Main Beam
	else if (SID == 0x006)
	{
		if ((message->data[0] & BIT6) == BIT6) Lights = Lights | BIT3;
			else Lights = Lights & ~BIT3;
	}
	//Tailgate
	else if (SID == 0x007)
	{
		if ((message->data[1] & BIT1) == BIT1) Doors = Doors | BIT4;
			else Doors = Doors & ~BIT4;

		//This should clear the bonnet data on A class vehicles where both BITS2 & 3 are set
		//regardless of bonnet state. Ensures data only works on the E class vehicles
		if (((message->data[1] & BIT3) == BIT3) && ((message->data[1] & BIT2) == 0))
				Doors = Doors | BIT5;
			else Doors = Doors & ~BIT5;
			
	}
	//Indicators
	else if (SID == 0x029)
	{
		if ((message->data[2] & BIT0) == BIT0) Lights = Lights | BIT8;
			else Lights = Lights & ~BIT8;

		if ((message->data[2] & BIT1) == BIT1) Lights = Lights | BIT9;
			else Lights = Lights & ~BIT9;
	}
	//Doors
	else if ((SID == 0x02F)&&(VehicleType == MERCEDES_CAR_1_TYPE))
	{
		if ((BIT0 & DATA[6]) == BIT0) Doors = Doors | BIT1;
			else Doors = Doors & ~BIT1;

		if ((BIT1 & DATA[6]) == BIT1) Doors = Doors | BIT0;
			else Doors = Doors & ~BIT0;

		if ((BIT2 & DATA[6]) == BIT2) Doors = Doors | BIT3;
			else Doors = Doors & ~BIT3;

		if ((BIT3 & DATA[6]) == BIT3) Doors = Doors | BIT2;
			else Doors = Doors & ~BIT2;

		Doors = Doors & 0x3F; //Clear Bit 7
	}	
	else if ((SID == 0x2EF)&&(VehicleType == MERCEDES_CAR_2_TYPE))
	{
		CheckSet8Inv(DATA[7], BIT2, &Doors, BIT0);//OSF
		CheckSet8Inv(DATA[7], BIT0, &Doors, BIT1);//NSF
		CheckSet8Inv(DATA[5], BIT6, &Doors, BIT2);//OSR
		CheckSet8Inv(DATA[5], BIT4, &Doors, BIT3);//OSF
		Doors = Doors & 0x3F; //Clear Bit 7



	}
	//Wiper Stalk
	else if (SID == 0x045)
	{
		Wipers = 0;
		
		switch (message->data[6] & 0x07)
		{
			case 1:
				Wipers = Wipers | 0x01;
				break;
			case 2:
				Wipers = Wipers | 0x02;
				break;
			case 5:
				Wipers = Wipers | 0x03;
				break;
			case 6:
				Wipers = Wipers | 0x04;

		}

		if ((message->data[2] & BIT6) == BIT6) Wipers = Wipers | BIT4;
			else Wipers = Wipers & ~BIT4;

		if ((message->data[2] & BIT5) == BIT5) Wipers = Wipers | BIT5;
			else Wipers = Wipers & ~BIT5;

		if ((message->data[2] & BIT7) == BIT7) Wipers = Wipers | BIT6;
			else Wipers = Wipers & ~BIT6;

		Wipers = Wipers & ~BIT7;

	}
	//Lights Status
	else if (SID == 0x069)
	{
		if ((message->data[0] & BIT2) == BIT2) Lights = Lights | BIT0;
			else Lights = Lights & ~BIT0;

		if ((message->data[0] & BIT3) == BIT3) Lights = Lights | BIT2;
			else Lights = Lights & ~BIT2;
		
		if ((message->data[0] & BIT5) == BIT5) Lights = Lights | BIT5;
			else Lights = Lights & ~BIT5;

		Lights = Lights & 0b0000001111101101;
		//Clear running lights, front fogs and NA bits
		
	}
	//Engine Accelerator & RPM
	else if (SID == 0x105)
	{
		//0x3FFF ia invalid data
		if ((message->data[0] == 0x3F)&&(message->data[1] == 0xFF))
		{
			RPM = 0xFF;
			Accelerator = 0xFF;
		}
		else
		{			
			Tempu16 = message->data[0] & 0x3F;

			Tempu16 = Tempu16 <<8;
			Tempu16 = Tempu16 | message->data[1];

			//Holds RPM data - no scaling yet.
			Tempu16 = (Tempu16 + 25) / 50;

			RPM = Tempu16 & 0xFF;

			Tempu8 = message->data[3];
			TempDouble = (double)Tempu8;

			TempDouble = TempDouble / 2.5;

			Tempu8 = (uint8_t)(TempDouble + 0.5);		

			Accelerator = Tempu8;
		}

	}
	//Autobox & Fuel (Autobox data used from 0x388 instead)
	else if (SID == 0x10C)
	{		
		if (message->data[1] != 0xFF)
		{
			Tempu8 = message->data[1] & 0x7F;

			if (Tempu8 != 0x7F)
				FuelLevel = Tempu8 * 10;

			if ((message->data[1] & BIT7) == BIT7) MILLights = MILLights | BIT4;
				else MILLights = MILLights & ~BIT4;



			MILLights = MILLights & 0b00010000; //Clear bit 7 to make signal valid
		}
		else
		{
			//Do nothing as data invalid
			FuelLevel = 0xFFFF;
		}
		
	}
	//Odometer & Speed
	else if (SID == 0x19F)
	{

		if((message->data[5] != 0xFF)&&(message->data[6] != 0xFF)&&(message->data[7] != 0xFE))
		{
			Tempu32 = message->data[5];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[6];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[7];

			Odometer = Tempu32 / 10;

			Tempu16 = message->data[0] & 0x1F;
			Tempu16 = Tempu16<<8;
			Tempu16 |= message->data[1];
	
			RoadSpeed = Tempu16 / 10;
		}
	}
	//VIN
	else if (SID == 0x204)
	{
		switch (message->data[0])
		{
			case 0:
				VIN[0] = message->data[1];
				VIN[1] = message->data[2];
				VIN[2] = message->data[3];
				VIN[3] = message->data[4];
				VIN[4] = message->data[5];
				VIN[5] = message->data[6];
				VIN[6] = message->data[7];
				break;
			case 1:
				VIN[7] = message->data[1];
				VIN[8] = message->data[2];
				VIN[9] = message->data[3];
				VIN[10] = message->data[4];
				VIN[11] = message->data[5];
				VIN[12] = message->data[6];
				VIN[13] = message->data[7];
				break;
			case 2:
				VIN[14] = message->data[1];
				VIN[15] = message->data[2];
				VIN[16] = message->data[3];
				break;
		}
	}

	else if (SID == 0x355)
	{
		
		Tempu8 = message->data[0];

		if (Tempu8 != 0x55)
		{
			//This gives coolant deg C + 41.
			CoolantTemp = Tempu8 - 1;
		}
		else CoolantTemp = 0xFF;

	}
	
	//E Class only
	else if (SID == 0x371)
	{
		Tempu8 = message->data[5] & 0x06;

		Tempu8 = Tempu8 >>1;

		switch (Tempu8)
		{
			case 0:
				SeatBelts = SeatBelts | 0x1C;
				break;
			case 1:
				SeatBelts = SeatBelts & ~BIT4;
				SeatBelts = SeatBelts | 0x0C;
				break;
			case 2:
				SeatBelts = SeatBelts & 0xE7;
				SeatBelts = SeatBelts | BIT2;
				break;
			case 3:
				SeatBelts = SeatBelts & 0xE3;
				break;
		}

	}

	//Seat Belts
	else if (SID == 0x375)
	{
		if ((message->data[2] & BIT0) == BIT0) SeatBelts = SeatBelts & ~BIT0;
			else SeatBelts = SeatBelts | BIT0;

		if ((message->data[2] & BIT2) == BIT2) SeatBelts = SeatBelts & ~BIT1;
			else SeatBelts = SeatBelts | BIT1;

		//Check if NOT E Class ID 0x375 Byte 3 != 3F
		//If A class then rear seat belts are on this ID
		if ((message->data[3] & 0x3F) != 0x3F)
		{
			if ((message->data[3] & BIT0) == BIT0) SeatBelts = SeatBelts & ~BIT3;
				else SeatBelts = SeatBelts | BIT3;

			if ((message->data[3] & BIT2) == BIT2) SeatBelts = SeatBelts & ~BIT2;
				else SeatBelts = SeatBelts | BIT2;

			if ((message->data[3] & BIT4) == BIT4) SeatBelts = SeatBelts & ~BIT4;
				else SeatBelts = SeatBelts | BIT4;
			
		}

		
		if (VehicleType == MERCEDES_CAR_2_TYPE) SeatBelts = SeatBelts & 0b0000011;
		else SeatBelts = SeatBelts & 0b0001111;

	}
	//Autbox & Brake pressure
	else if (SID == 0x388)
	{
	
	
		Tempu16 = message->data[3] & 0x0F;
		Tempu16 = Tempu16 << 8;
		Tempu16 = Tempu16 | message->data[4];

		TempDouble = (double)Tempu16;

		TempDouble = TempDouble / 40.94;

		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Brake = Tempu8;

		if ((message->data[0] & BIT7) == BIT7) Lights = Lights | BIT6;
			else Lights = Lights & ~BIT6;

		//ECO Mode
		if ((message->data[3] & BIT6) == BIT6) Controls = Controls | BIT3;
			else Controls = Controls & ~BIT3;



		


		switch (message->data[0] & 0x0F)
		{
			case 5:
				AutoBox = BIT3;
				Lights = Lights & ~BIT7;
				break;
			case 6:
				AutoBox = BIT2;
				Lights = Lights & ~BIT7;
				break;
			case 7:
				AutoBox = BIT1;
				Lights = Lights | BIT7;
				break;
			case 8:
				AutoBox = BIT0;
				Lights = Lights & ~BIT7;
				break;
			default:
				AutoBox = 0;
				Lights = Lights & ~BIT7;
		
		}

		if ((message->data[2] & BIT2) == BIT2) AutoBox = AutoBox & ~BIT4;
			else AutoBox = AutoBox | BIT4;

		Controls = Controls & 0x08; //Clear unused bits
	}

	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}

void DecodeDataMercSprinterType(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//message->data[x]

	//Ignition / Key State
	if (SID == 0x000)
	{
		//Key In
		if ((message->data[0] & BIT0) == BIT0) Ignition = Ignition | BIT0;
			else Ignition = Ignition & ~BIT0;
		
		//Aux Ignition
		if ((message->data[0] & BIT1) == BIT1) Ignition = Ignition | BIT1;
			else Ignition = Ignition & ~BIT1;

		//Full Ignition
		if ((message->data[0] & BIT2) == BIT2) Ignition = Ignition | BIT2;
			else Ignition = Ignition & ~BIT2;

		//Cranking
		if ((message->data[0] & BIT4) == BIT4) Ignition = Ignition | BIT3;
			else Ignition = Ignition & ~BIT3;

		Ignition = Ignition & 0b00001111;
		//Runlock
		CheckSet8(DATA[6], BIT0, &Controls, BIT5);

		
		
	}
	//RPM
	else if (SID == 0x002)
	{
		if (message->data[2] == 0xFF)
		{
			RPM = 0xFF;
		}
		else
		{
			Tempu16 = message->data[2] & 0x1F;

			Tempu16 = Tempu16 <<8;
			Tempu16 = Tempu16 | message->data[3];

			//Holds RPM data - no scaling yet.
			Tempu16 = (Tempu16 + 25) / 50;

			RPM = Tempu16 & 0xFF;
		}

		Tempu8 = DATA[4];
		if (Tempu8 != 0xFF);
			CoolantTemp = Tempu8;

	}
	//Speed and Brake pressure
	//brake lights & Reverse
	//Autobox
	else if (SID == 0x003)
	{

		

		Tempu16 = message->data[2] & 0x0F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[3];
	
		if (Tempu16 != 0xFFF)
		{
			RoadSpeed = Tempu16 / 15;
		}

		Tempu16 = message->data[5] & 0x0F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[6];
		
		if (Tempu16 != 0xFFF)
		{
			//TempDouble = (double)message->data[6];
			TempDouble = (double)Tempu16;
			TempDouble = TempDouble * 0.0244;

			Tempu8 = (uint8_t)(TempDouble + 0.5);		

			Brake = Tempu8;
		}

		//Reverse
		if ((message->data[1] & BIT7) == BIT7) Lights = Lights | BIT7;
			else Lights = Lights & ~BIT7;

		switch (message->data[4] & 0x70)
		{
			case 0x00: //Park
				AutoBox = BIT0;
				break;
			case 0x10: //Reverse
				AutoBox = BIT1;
				break;
			case 0x20: //Neutral
				AutoBox = BIT2;
				break;
			case 0x40: //Drive
				AutoBox = BIT3;
				break;
			default:
				AutoBox = 0xFF;	
		}

	}

	else if (SID == 0x004)
	{
	
		if ((BIT0 & DATA[0]) == BIT0) Doors = Doors | BIT1;
			else Doors = Doors & ~BIT1;

		if ((BIT1 & DATA[0]) == BIT1) Doors = Doors | BIT0;
			else Doors = Doors & ~BIT0;

		if ((BIT2 & DATA[0]) == BIT2) Doors = Doors | BIT3;
			else Doors = Doors & ~BIT3;

		if ((BIT3 & DATA[0]) == BIT3) Doors = Doors | BIT2;
			else Doors = Doors & ~BIT2;

		if ((BIT4 & DATA[0]) == BIT4) Doors = Doors | BIT4;
			else Doors = Doors & ~BIT4;
	
		Doors = Doors & 0x1F; //Clear Bit 7

		//Footbrake
		if ((message->data[0] & BIT6) == BIT6) Lights = Lights | BIT6;
			else Lights = Lights & ~BIT6;

	}

	else if (SID == 0x006)
	{
		Tempu16 = message->data[3] & 0x0F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[4];

		if(Tempu16!=0xFFF)
		{
			//If Left
			if (Tempu16 > 0x0800)
			{
				Tempu16 = (Tempu16 - 0x0800) / 2;
				SteeringWheel = Tempu16 & 0x3FF;
 				SteeringWheel = SteeringWheel & ~BIT15;
			}
			//If Right
			else
			{
				Tempu16 = (Tempu16) / 2;
				SteeringWheel = Tempu16 & 0x3FF;
				SteeringWheel = SteeringWheel | BIT15;
			}
		}
		else SteeringWheel = 0xFFFF;

		//Wipers
		Wipers = 0;
		
		//Intermittent
		if ((message->data[1] & BIT5) == BIT5) Wipers = 0x01;
		//Slow
		if ((message->data[1] & BIT6) == BIT6) Wipers = 0x03;
		//Fast
		if ((message->data[1] & BIT7) == BIT7) Wipers = 0x04;

		//Manual
		if ((message->data[1] & BIT3) == BIT3) Wipers = Wipers | BIT3;
			else Wipers = Wipers & ~BIT3;
		//Front Wash
		if ((message->data[2] & BIT5) == BIT5) Wipers = Wipers | BIT5;
			else Wipers = Wipers & ~BIT5;


		Wipers = Wipers & ~BIT7;


		//clear last bit to make data valid
		
		//Controls = Controls & 0x00000101;
		
	}
	
	//Lights & Handbrake
	else if (SID == 0x00A)
	{
		if ((message->data[1] & BIT4) == BIT4) Lights = Lights | BIT0;
			else Lights = Lights & ~BIT0;

		if ((message->data[1] & BIT5) == BIT5) Lights = Lights | BIT2;
			else Lights = Lights & ~BIT2;
		
		if ((message->data[0] & BIT2) == BIT2) Lights = Lights | BIT3;
			else Lights = Lights & ~BIT3;

		if ((message->data[0] & BIT1) == BIT1) Lights = Lights | BIT4;
			else Lights = Lights & ~BIT4;

		if ((message->data[1] & BIT7) == BIT7) Lights = Lights | BIT5;
			else Lights = Lights & ~BIT5;

;

		Lights = Lights & 0b0000001111111101;
		//Clear running lights and NA bits

		Controls &= 0b01111111;;

		if ((message->data[2] & BIT5) == BIT5) Controls = Controls | BIT0;
			else Controls = Controls & ~BIT0;

		//Horn
		if ((message->data[0] & BIT3) == BIT3) Controls = Controls | BIT2;
			else Controls = Controls & ~BIT2;

	}

	//Fuel level
	else if (SID == 0x00C)
	{

		Tempu8 = message->data[6] & 0x7F;
		
		FuelLevel = Tempu8 * 10;

		if ((Ignition & BIT2) == BIT2)
		{
			if (FuelLevel < 80)
				MILLights = MILLights | BIT4;
			else
				MILLights = MILLights & ~BIT4;	

			MILLights = MILLights & 0b00010000;
		}

		//else
		//	MILLights = 0xFF;
	}
	//Indicators
	else if (SID == 0x00E)
	{
		if ((message->data[0] & BIT6) == BIT6) Lights = Lights | BIT8;
			else Lights = Lights & ~BIT8;

		if ((message->data[0] & BIT7) == BIT7) Lights = Lights | BIT9;
			else Lights = Lights & ~BIT9;
	}
	//Seat Belt
	else if (SID == 0x012)
	{

		SeatBelts = 0;

		if ((message->data[3] & BIT6) == BIT6) SeatBelts = SeatBelts &~ BIT0;
			else SeatBelts = SeatBelts | BIT0;

	}
	//Odometer
	else if (SID == 0x09E)
	{
			Tempu32 = message->data[4];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[5];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[6];

			if (Tempu32 > 0)
				Odometer = Tempu32 / 10;
	}



	//VIN
	else if (SID == 0x0B2)
	{
		switch (message->data[0])
		{
			case 1:
				VIN[0] = message->data[1];
				VIN[1] = message->data[2];
				VIN[2] = message->data[3];
				VIN[3] = message->data[4];
				VIN[4] = message->data[5];
				VIN[5] = message->data[6];
				VIN[6] = message->data[7];
				break;
			case 2:
				VIN[7] = message->data[1];
				VIN[8] = message->data[2];
				VIN[9] = message->data[3];
				VIN[10] = message->data[4];
				VIN[11] = message->data[5];
				VIN[12] = message->data[6];
				VIN[13] = message->data[7];
				break;
			case 3:
				VIN[14] = message->data[1];
				VIN[15] = message->data[2];
				VIN[16] = message->data[3];
				break;
		}
	}
	//Accelerator position
	else if (SID == 0x222)
	{
		Tempu8 = message->data[2];
			
		if (Tempu8 != 0xFF)
		{

			TempDouble = (double)Tempu8;
	
			TempDouble = TempDouble / 2.5;

			Tempu8 = (uint8_t)(TempDouble + 0.5);		

			Accelerator = Tempu8;

		}
		else Accelerator = 0;
	}




	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}

void DecodeData2MercSprinterType(void)
{

	message = CANGetRxMessage(CAN2, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//AmbientTemp assignment used for Adblue
	if (SID == 0x2A2)
	{
		AmbientTemp = message->data[6];
	}


	CANUpdateChannel(CAN2, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN2, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}

//used to clear down data prior to sleep
void ClearData(void)
{
	AmbientTemp = 0xFF;
	FuelLevel = 0xFFFF;
	Accelerator = Brake = 0x00;
}