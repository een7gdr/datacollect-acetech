#ifndef _DC_CONTROLS_H
#define _DC_CONTROLS_H

#include "SYS.h"

/* Function Prototypes */

void DecodeData(void);
void DecodeData2(void);
void CompileHvacStatusBits(void);
void DecodeDataMercW176Type(void);
void DecodeDataMercSprinterType(void);
void DecodeData2MercSprinterType(void);
void Check_VolvoV70_2012Type(void);
void Check_MercW176Type(void);
void Check_MercSprinterType(void);
void DecodeDataVolvoV702012Type(void);
void DecodeDataFordMondeo2015Type(void);
void DecodeDataFordFocus2010Type(void);
void DecodeDataRenaultMaster2010Type(void);
void Check_RenaultMaster2010Type(void);
void Check_FordMondeo2015Type(void);
void Check_FordFocus2010Type(void);

BOOL CheckBitFn(uint8_t ToCheck, uint16_t BitToCheck);
void SetBitFn8(uint8_t* VarPoint, uint16_t BitToSet);
void SetBitFn16(uint16_t* VarPoint, uint16_t BitToSet);
void ClrBitFn8(uint8_t* VarPoint, uint16_t BitToSet);
void ClrBitFn16(uint16_t* VarPoint, uint16_t BitToSet);
void CheckSet8(uint8_t ByteNum, uint16_t BitToCheck, uint8_t* VarPoint, uint16_t BitToSet);
void CheckSet8Inv(uint8_t ByteNum, uint16_t BitToCheck, uint8_t* VarPoint, uint16_t BitToSet);
void CheckSet16(uint8_t ByteNum, uint16_t BitToCheck, uint16_t* VarPoint, uint16_t BitToSet);
void CheckSet16Inv(uint8_t ByteNum, uint16_t BitToCheck, uint16_t* VarPoint, uint16_t BitToSet);
BOOL CheckGPIO(void);
void ClearData(void);
void Check_MercVito2015Type(void);
void DecodeDataMercVito2015Type(void);
void Decode_AceTechType(void);
void Check_AceTechType(void);


#endif