#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_CONTROLS.h"
#include "DC_LED.h"
#include "DC_LIB_FIAT.h"
//#include "DC_CAN_LIBRARIES.h"

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80
#define BIT8 0x0100
#define BIT9 0x0200
#define BIT10 0x0400
#define BIT11 0x0800
#define BIT12 0x1000
#define BIT13 0x2000
#define BIT14 0x4000
#define BIT15 0x8000

extern uint8_t CANActivity;
extern uint8_t uBaudSelector;
extern uint8_t SecondCANActive;


extern enum Module_State
{
	STARTUP,
	NOT_LOCKED,
	BAUD_LOCKED,	
	LOCKED,
	SLEEP,
	ERROR
}State, Previous_State;

extern uint32_t Odometer;	//Bytes3-5
/*
KMs
*/
extern uint8_t RPM;				//Byte6
/*
Data = RPM / 50
*/
extern uint8_t RoadSpeed;		//Byte7
/*
KpH
*/
extern uint16_t FuelLevel; 	//Bytes8-9
/*
Data = Litres x 10 
*/
extern uint16_t FuelFlowCount;//Bytes10-11
extern uint8_t	Ignition;		//Byte12
/*
BIT0 - Key In
BIT1 - Accessory Position
BIT2 - Full Ignition
BIT3 - Cranking
BIT4-7 Unused
*/
extern uint8_t Accelerator;		//Byte13
/*
Percentage of full travel
*/
extern uint8_t Brake;			//Byte14
/*
Percetnrage of full travel
*/
extern uint16_t Lights;		//Byte15-16
/*
BIT0 - Sidelights
BIT1 - Running Lights
BIT2 - Dipped Beam
BIT3 - MAin Beam
BIT4 - Front Fogs
BIT5 - Rear Fogs
BIT6 - Brake Lights
BIT7 - Reverse Lights
BIT8 - Left Indicator
BIT9 - Right Indicator
BIT10 - Hazard Lighs
*/
extern uint8_t Wipers;			//Byte17
/*
BIT0-2 - 0(Off), 1(Int1), 2(Int2), 3(Speed1), 4(Speed2), 5(Auto)
BIT3 - Manual
BIT4 - Rear Wipers
BIT5 - Front Wash
BIT6 - Rear Wash
*/
extern uint8_t CoolantTemp;		//Byte18
/*
Data = Temp(C) + 40
*/
extern uint8_t AmbientTemp;
/*
Data = Temp(C) + 40
*/
extern uint16_t SteeringWheel;
/*
BIT0-14 - Degrees from Centre
BIT15 - Direction 1-Right
*/
extern uint8_t Controls;	
/*
BIT0 - Hanbrake
BIT1 - Clutch
BIT2 - Horn
BIT3 - ECO Mode
BIT4 - Auto Start/Stop Mode
*/
extern uint8_t AutoBox;			//Byte23
/*
BIT0 - Park
BIT1 - Reverse
BIT2 - Neutral
BIT3 - Drive
BIT4 - Manual
BIT5 - Gear Up
BIT6 - Gear Down
BIT7 - 
*/
extern uint8_t ManualBox;		//Byte24
/*
BIT0-2 - Gear (0 Neutral - 6th gear 7 = reverse 
BIT3-5 - Gear Advised 
BIT6 - Shift Up Advice
BIT7 - Shift Down Advice
*/
extern uint8_t Doors;			//Byte25
/*
BIT0 - OSF Door Open
BIT1 - NSF Door Open
BIT2 - OSR Door Open
BIT3 - NSR Door Open
BIT4 - Tailgate/Boot/Rear Door
BIT5 - Bonnet Open
BIT6 - 
BIT7 - 
*/
extern uint8_t SeatBelts;		//Byte26
/*
BIT0 - OSF Belt Used
BIT1 - NSF Belt Used
BIT2 - OSR Belt USed
BIT3 - NSR Belt Used
BIT4 - Raar Middle Used
BIT5 - 
BIT6 - 
BIT7 - 
*/
extern uint8_t Occupancy;		//Byte27
/*
BIT0 - NSF Seat Occupied
BIT1 - OSR Seat Occupied
BIT2 - NSR Seat Occupied
BIT3 - Rear Middle Occupied
*/
extern uint8_t MILLights;		//Byte28
/*
BIT0 - Engine MIL
BIT1 - Oil Level
BIT2 - Brake System
BIT3 - Oil Temp
BIT4 - Low Fuel
BIT5 - Coolant Temp
BIT6 - Oil Warning
BIT7 - Emissions MIL
*/

extern uint8_t VehicleType;		

extern uint8_t VIN[17];			//Bytes6-22
//Checksum byte 23
//End Packet byte 24

extern uint32_t FILTER0_VOL;
extern uint32_t FILTER1_VOL;
extern uint32_t FILTER2_VOL;
extern uint32_t FILTER3_VOL;
extern uint32_t FILTER4_VOL;
extern uint32_t FILTER5_VOL;
extern uint32_t FILTER6_VOL;
extern uint32_t FILTER7_VOL;
extern uint32_t FILTER8_VOL;
extern uint32_t FILTER9_VOL;
extern uint32_t FILTER10_VOL;
extern uint32_t FILTER11_VOL;
extern uint32_t FILTER12_VOL;
extern uint32_t FILTER13_VOL;
extern uint32_t FILTER14_VOL;
extern uint32_t FILTER15_VOL;

extern BOOL	FILTER0_EN;
extern BOOL	FILTER1_EN;
extern BOOL	FILTER2_EN;
extern BOOL	FILTER3_EN;
extern BOOL	FILTER4_EN;
extern BOOL	FILTER5_EN;
extern BOOL	FILTER6_EN;
extern BOOL	FILTER7_EN;
extern BOOL	FILTER8_EN;
extern BOOL	FILTER9_EN;
extern BOOL	FILTER10_EN;
extern BOOL	FILTER11_EN;
extern BOOL	FILTER12_EN;
extern BOOL	FILTER13_EN;
extern BOOL	FILTER14_EN;
extern BOOL	FILTER15_EN;

extern uint32_t FILTER2_0_VOL;
extern uint32_t FILTER2_1_VOL;
extern uint32_t FILTER2_2_VOL;
extern uint32_t FILTER2_3_VOL;
extern uint32_t FILTER2_4_VOL;
extern uint32_t FILTER2_5_VOL;
extern uint32_t FILTER2_6_VOL;
extern uint32_t FILTER2_7_VOL;
extern uint32_t FILTER2_8_VOL;
extern uint32_t FILTER2_9_VOL;
extern uint32_t FILTER2_10_VOL;
extern uint32_t FILTER2_11_VOL;
extern uint32_t FILTER2_12_VOL;
extern uint32_t FILTER2_13_VOL;
extern uint32_t FILTER2_14_VOL;
extern uint32_t FILTER2_15_VOL;

extern BOOL	FILTER2_0_EN;
extern BOOL	FILTER2_1_EN;
extern BOOL	FILTER2_2_EN;
extern BOOL	FILTER2_3_EN;
extern BOOL	FILTER2_4_EN;
extern BOOL	FILTER2_5_EN;
extern BOOL	FILTER2_6_EN;
extern BOOL	FILTER2_7_EN;
extern BOOL	FILTER2_8_EN;
extern BOOL	FILTER2_9_EN;
extern BOOL	FILTER2_10_EN;
extern BOOL	FILTER2_11_EN;
extern BOOL	FILTER2_12_EN;
extern BOOL	FILTER2_13_EN;
extern BOOL	FILTER2_14_EN;
extern BOOL	FILTER2_15_EN;

extern uint8_t Hold_Counter;

extern CANRxMessageBuffer * message;
uint32_t SID;


void Check_FiatDucatoE6Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	SID = message->msgSID.SID;
	//If the message is of extended 29 bit type
	if (message->msgEID.IDE == 1)
	{
		SID = SID << 18;
		SID = SID | message->msgEID.EID;
	}
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == FIAT_CAR_1_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == FIAT_CAR_1_CAN1_FILTER2)
		Check[1] = 1;
	if (SID == FIAT_CAR_1_CAN1_FILTER3)
		Check[2] = 1;
	if (SID == FIAT_CAR_1_CAN1_FILTER4)
		Check[3] = 1;
	if (SID == FIAT_CAR_1_CAN1_FILTER5)
		Check[4] = 1;
	if (SID == FIAT_CAR_1_CAN1_FILTER7)
		Check[5] = 1;
	//if (SID == HONDA_CAR_1_CAN1_FILTER7)
		Check[6] = 1;
	//if (SID == HONDA_CAR_1_CAN1_FILTER8)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = FIAT_CAR_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = FIAT_CAR_1_CAN1_FILTER0;
		FILTER1_VOL = FIAT_CAR_1_CAN1_FILTER1;
		FILTER2_VOL = FIAT_CAR_1_CAN1_FILTER2;
		FILTER3_VOL = FIAT_CAR_1_CAN1_FILTER3;
		FILTER4_VOL = FIAT_CAR_1_CAN1_FILTER4;
		FILTER5_VOL = FIAT_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = FIAT_CAR_1_CAN1_FILTER6;
		FILTER7_VOL = FIAT_CAR_1_CAN1_FILTER7;
		FILTER8_VOL = FIAT_CAR_1_CAN1_FILTER8;
		FILTER9_VOL = FIAT_CAR_1_CAN1_FILTER9;
		FILTER10_VOL = FIAT_CAR_1_CAN1_FILTER10;
		FILTER11_VOL = FIAT_CAR_1_CAN1_FILTER11;
		FILTER12_VOL = FIAT_CAR_1_CAN1_FILTER12;
		FILTER13_VOL = FIAT_CAR_1_CAN1_FILTER13;
		FILTER14_VOL = FIAT_CAR_1_CAN1_FILTER14;
		FILTER15_VOL = FIAT_CAR_1_CAN1_FILTER15;

		FILTER0_EN = FIAT_CAR_1_CAN1_FILTER0_EN;
		FILTER1_EN = FIAT_CAR_1_CAN1_FILTER1_EN;
		FILTER2_EN = FIAT_CAR_1_CAN1_FILTER2_EN;
		FILTER3_EN = FIAT_CAR_1_CAN1_FILTER3_EN;
		FILTER4_EN = FIAT_CAR_1_CAN1_FILTER4_EN;
		FILTER5_EN = FIAT_CAR_1_CAN1_FILTER5_EN;
		FILTER6_EN = FIAT_CAR_1_CAN1_FILTER6_EN;
		FILTER7_EN = FIAT_CAR_1_CAN1_FILTER7_EN;
		FILTER8_EN = FIAT_CAR_1_CAN1_FILTER8_EN;
		FILTER9_EN = FIAT_CAR_1_CAN1_FILTER9_EN;
		FILTER10_EN = FIAT_CAR_1_CAN1_FILTER10_EN;
		FILTER11_EN = FIAT_CAR_1_CAN1_FILTER11_EN;
		FILTER12_EN = FIAT_CAR_1_CAN1_FILTER12_EN;
		FILTER13_EN = FIAT_CAR_1_CAN1_FILTER13_EN;
		FILTER14_EN = FIAT_CAR_1_CAN1_FILTER14_EN;
		FILTER15_EN = FIAT_CAR_1_CAN1_FILTER15_EN;

		FILTER2_0_VOL = FIAT_CAR_1_CAN2_FILTER0;
		FILTER2_1_VOL = FIAT_CAR_1_CAN2_FILTER1;
		FILTER2_2_VOL = FIAT_CAR_1_CAN2_FILTER2;
		FILTER2_3_VOL = FIAT_CAR_1_CAN2_FILTER3;
		FILTER2_4_VOL = FIAT_CAR_1_CAN2_FILTER4;
		FILTER2_5_VOL = FIAT_CAR_1_CAN2_FILTER5;
		FILTER2_6_VOL = FIAT_CAR_1_CAN2_FILTER6;
		FILTER2_7_VOL = FIAT_CAR_1_CAN2_FILTER7;
		FILTER2_8_VOL = FIAT_CAR_1_CAN2_FILTER8;
		FILTER2_9_VOL = FIAT_CAR_1_CAN2_FILTER9;
		FILTER2_10_VOL = FIAT_CAR_1_CAN2_FILTER10;
		FILTER2_11_VOL = FIAT_CAR_1_CAN2_FILTER11;
		FILTER2_12_VOL = FIAT_CAR_1_CAN2_FILTER12;
		FILTER2_13_VOL = FIAT_CAR_1_CAN2_FILTER13;
		FILTER2_14_VOL = FIAT_CAR_1_CAN2_FILTER14;
		FILTER2_15_VOL = FIAT_CAR_1_CAN2_FILTER15;

		FILTER2_0_EN = FIAT_CAR_1_CAN2_FILTER0_EN;
		FILTER2_1_EN = FIAT_CAR_1_CAN2_FILTER1_EN;
		FILTER2_2_EN = FIAT_CAR_1_CAN2_FILTER2_EN;
		FILTER2_3_EN = FIAT_CAR_1_CAN2_FILTER3_EN;
		FILTER2_4_EN = FIAT_CAR_1_CAN2_FILTER4_EN;
		FILTER2_5_EN = FIAT_CAR_1_CAN2_FILTER5_EN;
		FILTER2_6_EN = FIAT_CAR_1_CAN2_FILTER6_EN;
		FILTER2_7_EN = FIAT_CAR_1_CAN2_FILTER7_EN;
		FILTER2_8_EN = FIAT_CAR_1_CAN2_FILTER8_EN;
		FILTER2_9_EN = FIAT_CAR_1_CAN2_FILTER9_EN;
		FILTER2_10_EN = FIAT_CAR_1_CAN2_FILTER10_EN;
		FILTER2_11_EN = FIAT_CAR_1_CAN2_FILTER11_EN;
		FILTER2_12_EN = FIAT_CAR_1_CAN2_FILTER12_EN;
		FILTER2_13_EN = FIAT_CAR_1_CAN2_FILTER13_EN;
		FILTER2_14_EN = FIAT_CAR_1_CAN2_FILTER14_EN;
		FILTER2_15_EN = FIAT_CAR_1_CAN2_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0xFF;
		AmbientTemp = 0x00;
		Controls = 0x00;
		AutoBox = 0xFF;
		ManualBox = 0x00;
		Doors = 0x00;
		SeatBelts = 0x00;
		Occupancy = 0xFF;
		MILLights = 0x00;
		Accelerator = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}


		InitCAN1FiltersEID();

		InitCAN2(250000, 1, 1, 4, 4);
		SecondCANActive = 1;
		//Initialise second CAN channel to pick up MIL & Acc infor
		InitCAN2FiltersFMS();
	}

}

void DecodeData2FiatDucatoE6Type(void)
{
	message = CANGetRxMessage(CAN2, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint8_t Tempu82 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t LeftCtr;
	static uint8_t RightCtr;

	//LED_ECU_RED = !LED_ECU_RED;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//If the message is of extended 29 bit type
	if (message->msgEID.IDE == 1)
	{
		SID = SID << 18;
		SID = SID | message->msgEID.EID;
	}

	if (SID == 0x00FD7D00)//tell tale
	{
		Tempu8 = DATA[0] & 0xF; // get page number only 0,1,2 supported

		switch (Tempu8)
		{
			case 0:
				//brake failure, coolant, oil
				//brake warning indicator yellow, red or info			
				Tempu82 = DATA[4]&0x7;
				if ((Tempu82>0)&&(Tempu82<0x04))
					MILLights = MILLights | BIT2;
				else MILLights = MILLights & ~BIT2;
				//Coolant MIL
				Tempu82 = DATA[5]&0x70;
				Tempu82 = Tempu82 >>4;

				if ((Tempu82>0)&&(Tempu82<0x04))
					MILLights = MILLights | BIT5;
				else MILLights = MILLights & ~BIT5;

				//Oil MIL
				Tempu82 = DATA[6]&0x70;
				Tempu82 = Tempu82 >>4;

				if ((Tempu82>0)&&(Tempu82<0x04))
					MILLights = MILLights | BIT6;
				else MILLights = MILLights & ~BIT6;
			
				break;
			case 1:
				//Engine MIL
				Tempu82 = DATA[1]&0x7;
				if ((Tempu82>0)&&(Tempu82<0x04))
					MILLights = MILLights | BIT0;
				else MILLights = MILLights & ~BIT0;
				//Oil Temp MIL
				Tempu82 = DATA[6]&0x7;
				if ((Tempu82>0)&&(Tempu82<0x04))
					MILLights = MILLights | BIT3;
				else MILLights = MILLights & ~BIT3;
				//Oil LEvel MIL
				Tempu82 = DATA[6]&0x70;
				Tempu82 = Tempu82 >>4;
				if ((Tempu82>0)&&(Tempu82<0x04))
					MILLights = MILLights | BIT1;
				else MILLights = MILLights & ~BIT1;				
				break;
			case 2:
				//Emissions MIL
				Tempu82 = DATA[2]&0x7;
				if ((Tempu82>0)&&(Tempu82<0x04))
					MILLights = MILLights | BIT7;
				else MILLights = MILLights & ~BIT7;
				break;

		}
	}


	//CANActivity = 1;
	CANUpdateChannel(CAN2, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN2, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}


void DecodeDataFiatDucatoE6Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
//	static double FuelFilter = 0;
	uint8_t DATA[8];
//	static uint8_t FuelFlow_Last;
	static uint8_t LeftCtr;
	static uint8_t RightCtr;
//	static uint8_t Previous_Ignition;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//If the message is of extended 29 bit type
	if (message->msgEID.IDE == 1)
	{
		SID = SID << 18;
		SID = SID | message->msgEID.EID;
	}

	//Ignition, Doors
	if (SID == 0x06214000)
	{
		//Igniton
		CheckSet8(DATA[2], BIT6, &Ignition, BIT2);
		//Crank
		CheckSet8(DATA[2], BIT7, &Ignition, BIT3);

		//NSF Door
		CheckSet8(DATA[1], BIT3, &Doors, BIT1);
		//OSF Door
		CheckSet8(DATA[1], BIT2, &Doors, BIT0);
		//Side Door
		CheckSet8(DATA[1], BIT5, &Doors, BIT3);
		//Rear Door
		CheckSet8(DATA[1], BIT6, &Doors, BIT4);

		//Handbrake
		CheckSet8(DATA[0], BIT5, &Controls, BIT0);

		//Low Fuel Warning
		CheckSet8(DATA[4], BIT1, &MILLights, BIT4);

		//Fuel, data is percentage and tank is 90 litres
		TempDouble = (double)DATA[5];
		TempDouble = 9 * TempDouble;
		if (DATA[5] !=0)
			FuelLevel = (uint16_t)(TempDouble + 0.5);
		
	}
	else if (SID == 0x063D4000)
	{
		TempDouble = (double)DATA[0];
	
		TempDouble = TempDouble / 2.0;

		AmbientTemp = (uint8_t)(TempDouble + 0.5);
	}
	else if (SID == 0x04394100)
	{
		TempDouble = (double)DATA[3];

		TempDouble = 0.5 + TempDouble / 2.55;

		Accelerator = (uint8_t)TempDouble;

	}
	//Speed
	else if (SID == 0x04394000)
	{
		Tempu16 = DATA[0] & 0x0F;
		Tempu16 = Tempu16 << 8;
		Tempu16 |= DATA[1];

		TempDouble = (double)Tempu16;
		TempDouble = TempDouble / 15.0;

		RoadSpeed = (uint8_t) (TempDouble + 0.5);
	}
	//RPM
	else if (SID == 0x04214001)
	{
		TempDouble = (double)DATA[6];
		TempDouble = 0.5 + TempDouble * 0.64; // 32/50
		RPM = (uint8_t) TempDouble;
		//Reverse gear
		CheckSet16(DATA[7], BIT2, &Lights, BIT7);
		
		CoolantTemp = DATA[3];
	}
	//Lights
	else if (SID == 0x02214000)
	{
		//FOotbrake
		CheckSet16(DATA[0], BIT7, &Lights, BIT6);
		//Rear Fogs
		CheckSet16(DATA[1], BIT1, &Lights, BIT5 );
		//Front Fogs
		CheckSet16(DATA[1], BIT2, &Lights, BIT4 );
		//Sidelights
		CheckSet16(DATA[1], BIT5, &Lights, BIT0 );
		//Main Beam
		CheckSet16(DATA[1], BIT4, &Lights, BIT3 );
		//Dipped Beam
		CheckSet16(DATA[1], BIT3, &Lights, BIT2 );

		//Left Indicator
		if ((DATA[2] & BIT6) == BIT6)
		{
			Lights = Lights | BIT8;
			LeftCtr = 0;
		}
		else 
		{
			if (LeftCtr>4)
				Lights = Lights &~ BIT8;
			else
				LeftCtr++;
		}
		//Right Indicator
		if ((DATA[2] & BIT5) == BIT5)
		{
			Lights = Lights | BIT9;
			RightCtr = 0;
		}
		else 
		{
			if (RightCtr>4)
				Lights = Lights &~ BIT9;
			else
				RightCtr++;
		}
	}
	
	else if (SID == 0x0C054003)
	{
		Tempu32 = DATA[1] & 0x0F;
		Tempu32 = Tempu32<<8;
		Tempu32 |= DATA[2];
		Tempu32 = Tempu32<<8;
		Tempu32 |= DATA[3];

		if (Tempu32 != 0xFFFFF) Odometer = Tempu32;

	}
	else if (SID == 0x04294001)
	{
		//Change up
		CheckSet8(DATA[3], BIT1, &ManualBox, BIT6);
		//Change down
		CheckSet8(DATA[3], BIT0, &ManualBox, BIT7);
	}
	else if (SID == 0x0621401A)
	{
		CheckSet8Inv(DATA[2], BIT0, &SeatBelts, BIT0); //OSF Seatbelts
	}
	else if (SID == 0x03254000)
	{
		switch (message->data[0])
		{
			case 0:
				VIN[0] = message->data[1];
				VIN[1] = message->data[2];
				VIN[2] = message->data[3];
				VIN[3] = message->data[4];
				VIN[4] = message->data[5];
				VIN[5] = message->data[6];
				VIN[6] = message->data[7];
				break;
			case 1:

				VIN[7] = message->data[1];
				VIN[8] = message->data[2];
				VIN[9] = message->data[3];
				VIN[10] = message->data[4];
				VIN[11] = message->data[5];
				VIN[12] = message->data[6];
				VIN[13] = message->data[7];
				break;
			case 2:
				
				VIN[14] = message->data[1];
				VIN[15] = message->data[2];
				VIN[16] = message->data[3];
				break;
		}
	}





	CANActivity = 1;
	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
}
