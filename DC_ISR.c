#include <p32xxxx.h>
#include <peripheral/int.h>
#include <plib.h>
#include "DC_CAN.h"
#include "DC_LED.h"
#include "DC_TIMER.h"

extern uint8_t isCAN1MsgReceived;
extern uint8_t isCAN2MsgReceived;
extern uint8_t isCAN1WakeUp;
extern uint8_t isTimerExpired;
extern uint8_t onRx232PacketReq;
extern uint8_t CANSleep;
extern uint8_t GenisysSerialInv;

static enum {  
        EXCEP_IRQ = 0,                  // interrupt  
        EXCEP_AdEL = 4,                 // address error exception (load or ifetch)  
        EXCEP_AdES,                             // address error exception (store)  
        EXCEP_IBE,                              // bus error (ifetch)  
        EXCEP_DBE,                              // bus error (load/store)  
        EXCEP_Sys,                              // syscall  
        EXCEP_Bp,                               // breakpoint  
        EXCEP_RI,                               // reserved instruction  
        EXCEP_CpU,                              // coprocessor unusable  
        EXCEP_Overflow,                 // arithmetic overflow  
        EXCEP_Trap,                             // trap (possible divide by zero)  
        EXCEP_IS1 = 16,                 // implementation specfic 1  
        EXCEP_CEU,                              // CorExtend Unuseable  
        EXCEP_C2E                               // coprocessor 2  
  } _excep_code;  
   
  static unsigned int _epc_code;  
  static unsigned int _excep_addr; 

void InitISR(void)
{
	//INTCON
	

}

void __ISR(_CORE_TIMER_VECTOR, ipl7) CoreTimerHandler(void)
{
    // .. things to do
	isTimerExpired = 1;	

    // update the period
    UpdateCoreTimer(CORE_TICK_RATE);

    // clear the interrupt flag
    mCTClearIntFlag();
}


void __attribute((interrupt(ipl6), vector(_CAN_1_VECTOR), nomips16)) _CANRXInterrupt(void)
{
	// Check if the source of the interrupt is RX_EVENT. 
	if((CANGetModuleEvent(CAN1) & CAN_RX_EVENT) != 0)
	{
		
		/* Within this, you can check which channel caused the 
		 * event by using the CANGetModuleEvent() function
         * which returns a code representing the highest priority
         * pending event. */ 
		
		if(CANGetPendingEventCode(CAN1) == CAN_CHANNEL1_EVENT)
		{
			// This means that channel 1 caused the event.
			// The CAN_RX_CHANNEL_NOT_EMPTY event is persistent. 
			
			//Disable channel event for now and set flag to indicate CAN Msg Received
            CANEnableChannelEvent(CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, FALSE);
				
			isCAN1MsgReceived = TRUE;
				
		}
	}

	if(CANGetPendingEventCode(CAN1) == CAN_WAKEUP_EVENT)
	{  
 		CANClearModuleEvent(CAN1, CAN_BUS_ACTIVITY_WAKEUP_EVENT); 

		isCAN1WakeUp = TRUE;
	}

   /* The CAN1 Interrupt flag is  cleared at the end of the 
	* interrupt routine. This is because the event source
    * that could have caused this interrupt  to occur 
    * (CAN_RX_CHANNEL_NOT_EMPTY) is disabled. Attempting to 
	* clear the CAN1 interrupt flag when the the CAN_RX_CHANNEL_NOT_EMPTY
    * interrupt is enabled will not have any effect because the 
	* base event is still present. */ 
	
	INTClearFlag(INT_CAN1);
}

void __attribute((interrupt(ipl6), vector(_CAN_2_VECTOR), nomips16)) _CANRX2Interrupt(void)
{
	// Check if the source of the interrupt is RX_EVENT. 
	//LED_ECU_RED = 0;
	if((CANGetModuleEvent(CAN2) & CAN_RX_EVENT) != 0)
	{
		
		/* Within this, you can check which channel caused the 
		 * event by using the CANGetModuleEvent() function
         * which returns a code representing the highest priority
         * pending event. */ 
		
		if(CANGetPendingEventCode(CAN2) == CAN_CHANNEL1_EVENT)
		{
			// This means that channel 1 caused the event.
			// The CAN_RX_CHANNEL_NOT_EMPTY event is persistent. 
			
			//Disable channel event for now and set flag to indicate CAN Msg Received
            CANEnableChannelEvent(CAN2, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, FALSE);
				
			isCAN2MsgReceived = TRUE;
				
		}
	}

   /* The CAN1 Interrupt flag is  cleared at the end of the 
	* interrupt routine. This is because the event source
    * that could have caused this interrupt  to occur 
    * (CAN_RX_CHANNEL_NOT_EMPTY) is disabled. Attempting to 
	* clear the CAN1 interrupt flag when the the CAN_RX_CHANNEL_NOT_EMPTY
    * interrupt is enabled will not have any effect because the 
	* base event is still present. */ 
	
	INTClearFlag(INT_CAN2);
}

//Interrupt by RS232 recieved message from the PC
void __attribute((interrupt(ipl4), vector(_UART_1_VECTOR), nomips16)) _UARTRXInterrupt(void)
{

	if(IFS0bits.U1RXIF == 1) 
	{
		IFS0bits.U1RXIF = 0;
		onRx232Packet();
		CANSleep = 0;
	}

	//LED_ECU_RED = 0;

	IFS0bits.U1TXIF = 0;
}

//Interrupt by RS232 recieved message from the RS232 Blue light controller
void __attribute((interrupt(ipl5), vector(_UART_4_VECTOR), nomips16)) _UART4RXInterrupt(void)
{

	if(IFS2bits.U4RXIF == 1) 
	{
		IFS2bits.U4RXIF = 0;
		onRx232_4Packet();
		CANSleep = 0;
	}

//	LED_ECU_RED = 0;

	IFS2bits.U4TXIF = 0;
}
//Interrupt by RS232 recieved message from the the Blue light interface UART
void __attribute((interrupt(ipl5), vector(_UART_2_VECTOR), nomips16)) _UART2RXInterrupt(void)
{

	if(IFS1bits.U2RXIF == 1) 
	{
		IFS1bits.U2RXIF = 0;
		onRx485Packet();
		CANSleep = 0;
		GenisysSerialInv = 0;
		//ToggleLED_GREEN();
	}

	IFS1bits.U2TXIF = 0;
}



 
// this function overrides the normal _weak_ generic handler  
void _general_exception_handler(void)  
{  
      asm volatile("mfc0 %0,$13" : "=r" (_excep_code));  
      asm volatile("mfc0 %0,$14" : "=r" (_excep_addr));  
   
      _excep_code = (_excep_code & 0x0000007C) >> 2;  
            while (1)
			{  
              // Examine _excep_code to identify the type of exception  
              // Examine _excep_addr to find the address that caused the exception  
              Nop();  
              Nop();  
              Nop();   
  			}  
  }//   End of exception handler   
