#ifndef _DC_ISR_H
#define _DC_ISR_H


void InitISR(void);

void __ISR(_CORE_TIMER_VECTOR, ipl2) CoreTimerHandler(void);
void _general_exception_handler (void);
#endif

