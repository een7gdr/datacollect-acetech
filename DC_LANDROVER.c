#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_CONTROLS.h"
#include "DC_LED.h"
#include "DC_LIB_LANDROVER.h"
//#include "DC_CAN_LIBRARIES.h"

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80
#define BIT8 0x0100
#define BIT9 0x0200
#define BIT10 0x0400
#define BIT11 0x0800
#define BIT12 0x1000
#define BIT13 0x2000
#define BIT14 0x4000
#define BIT15 0x8000

extern uint8_t CANActivity;
extern uint8_t uBaudSelector;
extern uint8_t SecondCANActive;


extern enum Module_State
{
	STARTUP,
	NOT_LOCKED,
	BAUD_LOCKED,	
	LOCKED,
	SLEEP,
	ERROR
}State, Previous_State;

extern uint32_t Odometer;	//Bytes3-5
/*
KMs
*/
extern uint8_t RPM;				//Byte6
/*
Data = RPM / 50
*/
extern uint8_t RoadSpeed;		//Byte7
/*
KpH
*/
extern uint16_t FuelLevel; 	//Bytes8-9
/*
Data = Litres x 10 
*/
extern uint16_t FuelFlowCount;//Bytes10-11
extern uint8_t	Ignition;		//Byte12
/*
BIT0 - Key In
BIT1 - Accessory Position
BIT2 - Full Ignition
BIT3 - Cranking
BIT4-7 Unused
*/
extern uint8_t Accelerator;		//Byte13
/*
Percentage of full travel
*/
extern uint8_t Brake;			//Byte14
/*
Percetnrage of full travel
*/
extern uint16_t Lights;		//Byte15-16
/*
BIT0 - Sidelights
BIT1 - Running Lights
BIT2 - Dipped Beam
BIT3 - MAin Beam
BIT4 - Front Fogs
BIT5 - Rear Fogs
BIT6 - Brake Lights
BIT7 - Reverse Lights
BIT8 - Left Indicator
BIT9 - Right Indicator
BIT10 - Hazard Lighs
*/
extern uint8_t Wipers;			//Byte17
/*
BIT0-2 - 0(Off), 1(Int1), 2(Int2), 3(Speed1), 4(Speed2), 5(Auto)
BIT3 - Manual
BIT4 - Rear Wipers
BIT5 - Front Wash
BIT6 - Rear Wash
*/
extern uint8_t CoolantTemp;		//Byte18
/*
Data = Temp(C) + 40
*/
extern uint8_t AmbientTemp;
/*
Data = Temp(C) + 40
*/
extern uint16_t SteeringWheel;
/*
BIT0-14 - Degrees from Centre
BIT15 - Direction 1-Right
*/
extern uint8_t Controls;	
/*
BIT0 - Hanbrake
BIT1 - Clutch
BIT2 - Horn
BIT3 - ECO Mode
BIT4 - Auto Start/Stop Mode
*/
extern uint8_t AutoBox;			//Byte23
/*
BIT0 - Park
BIT1 - Reverse
BIT2 - Neutral
BIT3 - Drive
BIT4 - Manual
BIT5 - Gear Up
BIT6 - Gear Down
BIT7 - 
*/
extern uint8_t ManualBox;		//Byte24
/*
BIT0-2 - Gear (0 Neutral - 6th gear 7 = reverse 
BIT3-5 - Gear Advised 
BIT6 - Shift Up Advice
BIT7 - Shift Down Advice
*/
extern uint8_t Doors;			//Byte25
/*
BIT0 - OSF Door Open
BIT1 - NSF Door Open
BIT2 - OSR Door Open
BIT3 - NSR Door Open
BIT4 - Tailgate/Boot/Rear Door
BIT5 - Bonnet Open
BIT6 - 
BIT7 - 
*/
extern uint8_t SeatBelts;		//Byte26
/*
BIT0 - OSF Belt Used
BIT1 - NSF Belt Used
BIT2 - OSR Belt USed
BIT3 - NSR Belt Used
BIT4 - Raar Middle Used
BIT5 - 
BIT6 - 
BIT7 - 
*/
extern uint8_t Occupancy;		//Byte27
/*
BIT0 - NSF Seat Occupied
BIT1 - OSR Seat Occupied
BIT2 - NSR Seat Occupied
BIT3 - Rear Middle Occupied
*/
extern uint8_t MILLights;		//Byte28
/*
BIT0 - Engine MIL
BIT1 - Oil Level
BIT2 - Brake System
BIT3 - Oil Temp
BIT4 - Low Fuel
BIT5 - Coolant Temp
BIT6 - Oil Warning
BIT7 - Emissions MIL
*/

extern uint8_t VehicleType;		

extern uint8_t VIN[17];			//Bytes6-22
//Checksum byte 23
//End Packet byte 24

extern uint32_t FILTER0_VOL;
extern uint32_t FILTER1_VOL;
extern uint32_t FILTER2_VOL;
extern uint32_t FILTER3_VOL;
extern uint32_t FILTER4_VOL;
extern uint32_t FILTER5_VOL;
extern uint32_t FILTER6_VOL;
extern uint32_t FILTER7_VOL;
extern uint32_t FILTER8_VOL;
extern uint32_t FILTER9_VOL;
extern uint32_t FILTER10_VOL;
extern uint32_t FILTER11_VOL;
extern uint32_t FILTER12_VOL;
extern uint32_t FILTER13_VOL;
extern uint32_t FILTER14_VOL;
extern uint32_t FILTER15_VOL;

extern BOOL	FILTER0_EN;
extern BOOL	FILTER1_EN;
extern BOOL	FILTER2_EN;
extern BOOL	FILTER3_EN;
extern BOOL	FILTER4_EN;
extern BOOL	FILTER5_EN;
extern BOOL	FILTER6_EN;
extern BOOL	FILTER7_EN;
extern BOOL	FILTER8_EN;
extern BOOL	FILTER9_EN;
extern BOOL	FILTER10_EN;
extern BOOL	FILTER11_EN;
extern BOOL	FILTER12_EN;
extern BOOL	FILTER13_EN;
extern BOOL	FILTER14_EN;
extern BOOL	FILTER15_EN;

extern uint32_t FILTER2_0_VOL;
extern uint32_t FILTER2_1_VOL;
extern uint32_t FILTER2_2_VOL;
extern uint32_t FILTER2_3_VOL;
extern uint32_t FILTER2_4_VOL;
extern uint32_t FILTER2_5_VOL;
extern uint32_t FILTER2_6_VOL;
extern uint32_t FILTER2_7_VOL;
extern uint32_t FILTER2_8_VOL;
extern uint32_t FILTER2_9_VOL;
extern uint32_t FILTER2_10_VOL;
extern uint32_t FILTER2_11_VOL;
extern uint32_t FILTER2_12_VOL;
extern uint32_t FILTER2_13_VOL;
extern uint32_t FILTER2_14_VOL;
extern uint32_t FILTER2_15_VOL;

extern BOOL	FILTER2_0_EN;
extern BOOL	FILTER2_1_EN;
extern BOOL	FILTER2_2_EN;
extern BOOL	FILTER2_3_EN;
extern BOOL	FILTER2_4_EN;
extern BOOL	FILTER2_5_EN;
extern BOOL	FILTER2_6_EN;
extern BOOL	FILTER2_7_EN;
extern BOOL	FILTER2_8_EN;
extern BOOL	FILTER2_9_EN;
extern BOOL	FILTER2_10_EN;
extern BOOL	FILTER2_11_EN;
extern BOOL	FILTER2_12_EN;
extern BOOL	FILTER2_13_EN;
extern BOOL	FILTER2_14_EN;
extern BOOL	FILTER2_15_EN;

extern uint8_t Hold_Counter;

extern CANRxMessageBuffer * message;
uint32_t SID;

void Check_LRDisco4HSType(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == LR_CAR_1_CAN1_FILTER4) 
		Check[0] = 1;
	if (SID == LR_CAR_1_CAN1_FILTER6) 
		Check[1] = 1;
	if (SID == LR_CAR_1_CAN1_FILTER10) 
		Check[2] = 1;
	if (SID == LR_CAR_1_CAN1_FILTER5)
		Check[3] = 1;
	if (SID == LR_CAR_1_CAN1_FILTER2)
		Check[4] = 1;
	//if (SID == LR_CAR_1_CAN1_FILTER6)
		Check[5] = 1;
	//if (SID == LR_CAR_1_CAN1_FILTER7)
		Check[6] = 1;
	//if (SID == LR_CAR_1_CAN1_FILTER8)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = LR_CAR_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = LR_CAR_1_CAN1_FILTER0;
		FILTER1_VOL = LR_CAR_1_CAN1_FILTER1;
		FILTER2_VOL = LR_CAR_1_CAN1_FILTER2;
		FILTER3_VOL = LR_CAR_1_CAN1_FILTER3;
		FILTER4_VOL = LR_CAR_1_CAN1_FILTER4;
		FILTER5_VOL = LR_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = LR_CAR_1_CAN1_FILTER6;
		FILTER7_VOL = LR_CAR_1_CAN1_FILTER7;
		FILTER8_VOL = LR_CAR_1_CAN1_FILTER8;
		FILTER9_VOL = LR_CAR_1_CAN1_FILTER9;
		FILTER10_VOL = LR_CAR_1_CAN1_FILTER10;
		FILTER11_VOL = LR_CAR_1_CAN1_FILTER11;
		FILTER12_VOL = LR_CAR_1_CAN1_FILTER12;
		FILTER13_VOL = LR_CAR_1_CAN1_FILTER13;
		FILTER14_VOL = LR_CAR_1_CAN1_FILTER14;
		FILTER15_VOL = LR_CAR_1_CAN1_FILTER15;

		FILTER0_EN = LR_CAR_1_CAN1_FILTER0_EN;
		FILTER1_EN = LR_CAR_1_CAN1_FILTER1_EN;
		FILTER2_EN = LR_CAR_1_CAN1_FILTER2_EN;
		FILTER3_EN = LR_CAR_1_CAN1_FILTER3_EN;
		FILTER4_EN = LR_CAR_1_CAN1_FILTER4_EN;
		FILTER5_EN = LR_CAR_1_CAN1_FILTER5_EN;
		FILTER6_EN = LR_CAR_1_CAN1_FILTER6_EN;
		FILTER7_EN = LR_CAR_1_CAN1_FILTER7_EN;
		FILTER8_EN = LR_CAR_1_CAN1_FILTER8_EN;
		FILTER9_EN = LR_CAR_1_CAN1_FILTER9_EN;
		FILTER10_EN = LR_CAR_1_CAN1_FILTER10_EN;
		FILTER11_EN = LR_CAR_1_CAN1_FILTER11_EN;
		FILTER12_EN = LR_CAR_1_CAN1_FILTER12_EN;
		FILTER13_EN = LR_CAR_1_CAN1_FILTER13_EN;
		FILTER14_EN = LR_CAR_1_CAN1_FILTER14_EN;
		FILTER15_EN = LR_CAR_1_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0x00;
		Doors = 0x00;
		SeatBelts = 0x00;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}


		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_LRDiscoSportType(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == LR_CAR_2_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == LR_CAR_2_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == LR_CAR_2_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == LR_CAR_2_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == LR_CAR_2_CAN1_FILTER4)
		Check[4] = 1;
	if (SID == LR_CAR_2_CAN1_FILTER5)
		Check[5] = 1;
	//if (SID == LR_CAR_1_CAN1_FILTER7)
		Check[6] = 1;
	//if (SID == LR_CAR_1_CAN1_FILTER8)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = LR_CAR_2_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = LR_CAR_2_CAN1_FILTER0;
		FILTER1_VOL = LR_CAR_2_CAN1_FILTER1;
		FILTER2_VOL = LR_CAR_2_CAN1_FILTER2;
		FILTER3_VOL = LR_CAR_2_CAN1_FILTER3;
		FILTER4_VOL = LR_CAR_2_CAN1_FILTER4;
		FILTER5_VOL = LR_CAR_2_CAN1_FILTER5;
		FILTER6_VOL = LR_CAR_2_CAN1_FILTER6;
		FILTER7_VOL = LR_CAR_2_CAN1_FILTER7;
		FILTER8_VOL = LR_CAR_2_CAN1_FILTER8;
		FILTER9_VOL = LR_CAR_2_CAN1_FILTER9;
		FILTER10_VOL = LR_CAR_2_CAN1_FILTER10;
		FILTER11_VOL = LR_CAR_2_CAN1_FILTER11;
		FILTER12_VOL = LR_CAR_2_CAN1_FILTER12;
		FILTER13_VOL = LR_CAR_2_CAN1_FILTER13;
		FILTER14_VOL = LR_CAR_2_CAN1_FILTER14;
		FILTER15_VOL = LR_CAR_2_CAN1_FILTER15;

		FILTER0_EN = LR_CAR_2_CAN1_FILTER0_EN;
		FILTER1_EN = LR_CAR_2_CAN1_FILTER1_EN;
		FILTER2_EN = LR_CAR_2_CAN1_FILTER2_EN;
		FILTER3_EN = LR_CAR_2_CAN1_FILTER3_EN;
		FILTER4_EN = LR_CAR_2_CAN1_FILTER4_EN;
		FILTER5_EN = LR_CAR_2_CAN1_FILTER5_EN;
		FILTER6_EN = LR_CAR_2_CAN1_FILTER6_EN;
		FILTER7_EN = LR_CAR_2_CAN1_FILTER7_EN;
		FILTER8_EN = LR_CAR_2_CAN1_FILTER8_EN;
		FILTER9_EN = LR_CAR_2_CAN1_FILTER9_EN;
		FILTER10_EN = LR_CAR_2_CAN1_FILTER10_EN;
		FILTER11_EN = LR_CAR_2_CAN1_FILTER11_EN;
		FILTER12_EN = LR_CAR_2_CAN1_FILTER12_EN;
		FILTER13_EN = LR_CAR_2_CAN1_FILTER13_EN;
		FILTER14_EN = LR_CAR_2_CAN1_FILTER14_EN;
		FILTER15_EN = LR_CAR_2_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0x00;
		Doors = 0x00;
		SeatBelts = 0x00;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}


		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void DecodeDataLandRoverDisco4Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//RPM & Brake lights
	if (SID == 0x295)
	{
		Tempu16 = message->data[6] & 0x1F;
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[7];

		//Holds RPM data - no scaling yet.
		Tempu16 = (Tempu16 + 25) / 50;

		RPM = Tempu16 & 0xFF;

		if ((message->data[6] & BIT5) == BIT5) Lights = Lights | BIT6;
			else Lights = Lights & ~BIT6;
			
	}
	//Road Speed
	else if (SID == 0x20B)
	{
		Tempu16 = message->data[6] & 0x7F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[7];
	
		RoadSpeed = Tempu16 / 100;

	}
	//Odometer
	else if (SID == 0x47D)
	{
			Tempu32 = message->data[5];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[6];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[7];

			Odometer = Tempu32;
	}
	//Coolant
	else if (SID == 0x355)
	{
		Tempu8 = message->data[5];

		//This gives coolant deg C + 60 (need coolant + 40).
		CoolantTemp = Tempu8 - 20;

	}
	//Ignition
	else if (SID == 0x257)
	{
		Tempu8 = message->data[5] & 0x0F;

		if ((Tempu8 == 0x06)||(Tempu8 == 0x07)||(Tempu8 == 0x09))
			Ignition = Ignition | BIT2;
		else
			Ignition = Ignition & ~BIT2;

		if (Tempu8 == 0x09) Ignition = Ignition | BIT3;
		else Ignition = Ignition & ~ BIT3;

	}
	//Lights
	else if (SID == 0x457)
	{
		//Sidelights
		if ((message->data[2] & BIT4) == BIT4) Lights = Lights | BIT0;
			else Lights = Lights & ~BIT0;
		//Rear Fogs
		if ((message->data[2] & BIT1) == BIT1) Lights = Lights | BIT5;
			else Lights = Lights &~ BIT5;
		//Dipped
		if ((message->data[5] & BIT0) == BIT0) Lights = Lights | BIT2;
			else Lights = Lights & ~BIT2;
		//Main
		if ((message->data[5] & BIT5) == BIT5) Lights = Lights | BIT3;
			else Lights = Lights & ~BIT3;
		//Front Fogs
		if ((message->data[7] & BIT5) == BIT5) Lights = Lights | BIT4;
			else Lights = Lights & ~BIT4;
		//Left
		if ((message->data[7] & BIT2) == BIT2) Lights = Lights | BIT8;
			else Lights = Lights &~BIT8;
		//Right
		if ((message->data[7] & BIT3) == BIT3) Lights = Lights | BIT9;
			else Lights = Lights &~BIT9;


	}
	//Front Wipers
	else if (SID == 0x337)
	{
		Tempu8 = message->data[2] & 0xC0;

		//Clear last 3 bits
		Wipers = Wipers & 0xF8;

		if (Tempu8 == 0xC0) Wipers = Wipers | 0x04;
		else if (Tempu8 == 0x80) Wipers = Wipers | 0x03; 

	}
	//Accel position
	else if (SID == 0x255)
	{
		Tempu8 = message->data[3];
		TempDouble = (double)Tempu8;

		TempDouble = TempDouble / 2.0;

		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Accelerator = Tempu8;

	}
	//Brake pedal position
	else if (SID == 0x3EB)
	{
		Tempu16 = message->data[4] & 0x7F;
		Tempu16 = Tempu16 << 8;
		Tempu16 = Tempu16 | message->data[5];

		TempDouble = (double)Tempu16;

		TempDouble = TempDouble / 200;

		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Brake = Tempu8;

	}
	//Steering Wheel
	else if (SID == 0x280)
	{
		Tempu16 = message->data[6] & 0x3F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[7];
		//If Right
		if (Tempu16 < 0x1E75)
		{
			Tempu16 = (0x1E75 - Tempu16) / 10;
			SteeringWheel = Tempu16 & 0x3FF;
 			SteeringWheel = SteeringWheel | BIT15;
		}
		//If Left
		else
		{
			Tempu16 = (Tempu16 - 0x1E75) / 10;
			SteeringWheel = Tempu16 & 0x3FF;
			SteeringWheel = SteeringWheel & ~BIT15;
		}

	}
	//Doors & Handbrake
	else if (SID == 0x437)
	{
		//Door CAN signals 0 open, 1 closed
		//OSF
		if ((message->data[6] & BIT0) == BIT0) Doors = Doors &~ BIT0;
			else Doors = Doors | BIT0;
		//NSF
		if ((message->data[6] & BIT1) == BIT1) Doors = Doors &~ BIT1;
			else Doors = Doors | BIT1;
		//OSR
		if ((message->data[6] & BIT3) == BIT3) Doors = Doors &~ BIT2;
			else Doors = Doors | BIT2;
		//NSR
		if ((message->data[6] & BIT2) == BIT2) Doors = Doors &~ BIT3;
			else Doors = Doors | BIT3;
		//Tailgate
		if ((message->data[6] & BIT4) == BIT4) Doors = Doors &~ BIT4;
			else Doors = Doors | BIT4;
		//Bonnet
		if ((message->data[6] & BIT5) == BIT5) Doors = Doors &~ BIT5;
			else Doors = Doors | BIT5;
		//Handbrake
		if ((message->data[7] & BIT0) == BIT0) Controls = Controls | BIT0;
			else Controls = Controls & ~BIT0;

	}
	//Autobox
	else if (SID == 0x312)
	{
		Tempu8 = message->data[2] & 0x0F;
	
		switch (Tempu8)
		{
			case 0xF:
				//Park
				AutoBox = BIT0;
				Lights = Lights & ~BIT7;
				break;
			case 0xD:
				//Neutral
				AutoBox = BIT2;
				Lights = Lights & ~BIT7;
				break;
			case 0xE:
				//Reverse
				AutoBox = BIT1;
				Lights = Lights | BIT7;
				break;
			case 0xC:
				//Drive
				AutoBox = BIT3;
				Lights = Lights & ~BIT7;
				break;
			case 0x8:
				//Sport
				AutoBox = BIT4;
				Lights = Lights & ~BIT7;
				break;
			default:
				AutoBox = 0;
				Lights = Lights & ~BIT7;
		}

	}
	//Drive Modes
	else if (SID == 0x478)
	{
		Tempu8 = message->data[6] & 0xF0;
	
		switch (Tempu8)
		{
			case 0x00:
				//Normal
				ManualBox = BIT0;
				break;
			case 0x10:
				//Grass Gravel Snow
				ManualBox = BIT1;
				break;
			case 0x40:
				//Mud
				ManualBox = BIT2;
				break;
			case 0x30:
				//Sand
				ManualBox = BIT3;
				break;
			case 0x50:
				//Rocky Terrain
				ManualBox = BIT4;
				break;
			default:
				ManualBox = 0;
				
		}


	}
	//Low box
	else if (SID == 0x358)
	{
		if ((message->data[2] & 0x03) == 0x00) AutoBox = AutoBox | BIT5;
			else AutoBox = AutoBox & ~BIT5;

	}
	else if (SID == 0x39A)
	{
		if ((message->data[7] & BIT2) == BIT2) SeatBelts = SeatBelts | BIT0;
			else SeatBelts = SeatBelts & ~BIT0;
		if ((message->data[6] & BIT4) == BIT4) SeatBelts = SeatBelts | BIT1;
			else SeatBelts = SeatBelts & ~BIT1;

	}


	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}

void DecodeDataLandRoverDiscoSportType(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//RPM & Handbrake
	if (SID == 0x414)
	{
		Tempu16 = message->data[6] & 0x3F;
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[7];

		//Holds RPM data - no scaling yet.
		Tempu16 = (Tempu16 + 25) / 50;

		RPM = Tempu16 & 0xFF;

		//Handbrake 
		CheckSet8(DATA[2], BIT0, &Controls, BIT0);
	}

	//Brake Lights
	else if (SID == 0x160)
	{
		CheckSet16(DATA[4], BIT2, &Lights, BIT6);		
	}

	//Road Speed
	else if (SID == 0x2E9)
	{
		Tempu16 = message->data[6] & 0x7F;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[7];
	
		RoadSpeed = Tempu16 / 100;

	}
	//Odometer

	//Coolant
	//else if (SID == 0x355)
	//{
	//	Tempu8 = message->data[5];

		//This gives coolant deg C + 60 (need coolant + 40).
	//	CoolantTemp = Tempu8 - 20;

	//}
	//Ignition & Front Fogs
	else if (SID == 0x4A0)
	{
		CheckSet8(DATA[0], BIT3, &Ignition, BIT2);

		CheckSet16(DATA[1], BIT4, &Lights, BIT4);
	}
	//Lights
	else if (SID == 0x4B0)
	{
		//Sidelights
		CheckSet16(DATA[0], BIT7, &Lights, BIT0);

		//Dipped
		CheckSet16(DATA[2], BIT7, &Lights, BIT2);

		//Main
		CheckSet16(DATA[1], BIT4, &Lights, BIT3);

	//Front Wipers

		Tempu8 = message->data[3] & 0x30;

		//Clear last 3 bits
		Wipers = Wipers & 0xF8;
		//Intermittent, low & high
		if (Tempu8 == 0x10)
			Wipers = Wipers | 0x01;
		else if (Tempu8 == 0x20)
			Wipers = Wipers | 0x03;
		else if (Tempu8 == 0x30)
			Wipers = Wipers | 0x04;
		//Manual Wipe
		CheckSet8(DATA[3], BIT6, &Wipers, BIT3);
		//Rear Wipers
		Tempu8 = message->data[3] & 0x0C;
		if (Tempu8 > 0) Wipers = Wipers | BIT4;
		else Wipers = Wipers &~ BIT4;
		//Front Wash
		CheckSet8(DATA[2], BIT0, &Wipers, BIT5);
		//Rear Wash
		CheckSet8(DATA[3], BIT7, &Wipers, BIT6);

	}
	//Accel position
	//else if (SID == 0x255)
	//{
	//	Tempu8 = message->data[3];
	//	TempDouble = (double)Tempu8;

	//	TempDouble = TempDouble / 2.0;

	//	Tempu8 = (uint8_t)(TempDouble + 0.5);		

	//	Accelerator = Tempu8;

	//}
	//Brake pedal position
	else if (SID == 0x1D0)
	{
		Tempu16 = message->data[3] & 0x03;
		Tempu16 = Tempu16 << 8;
		Tempu16 = Tempu16 | message->data[4];

		TempDouble = (double)Tempu16;
		//0x286 about max
		TempDouble = TempDouble / 6.4;

		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Brake = Tempu8;

	}
	//Steering Wheel
	else if (SID == 0x198)
	{
		Tempu16 = message->data[4] & 0x3F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[5];
		//If Right
		if (Tempu16 < 0x1E5C)
		{
			Tempu16 = (0x1E5C - Tempu16) / 10;
			SteeringWheel = Tempu16 & 0x3FF;
 			SteeringWheel = SteeringWheel | BIT15;
		}
		//If Left
		else
		{
			Tempu16 = (Tempu16 - 0x1E5C) / 10;
			SteeringWheel = Tempu16 & 0x3FF;
			SteeringWheel = SteeringWheel & ~BIT15;
		}

	}
	//Doors & Clutch
	else if (SID == 0x490)
	{
		//Door CAN signals 0 open, 1 closed
		//OSF
		CheckSet8Inv(DATA[2], BIT2, &Doors, BIT0);
		//NSF
		CheckSet8Inv(DATA[2], BIT3, &Doors, BIT1);
		//OSR
		CheckSet8Inv(DATA[2], BIT5, &Doors, BIT2);
		//NSR
		CheckSet8Inv(DATA[2], BIT4, &Doors, BIT3);
		//Tailgat
		CheckSet8Inv(DATA[2], BIT6, &Doors, BIT4);
		//Bonnet
		CheckSet8Inv(DATA[2], BIT7, &Doors, BIT5);

		Tempu8 = message->data[2] & 0x03;
		if (Tempu8 > 0x01) Controls = Controls | BIT1;
			else Controls = Controls &~ BIT1;


	}
	//indicators
	else if (SID == 0x420)
	{
		CheckSet16(DATA[6], BIT3, &Lights, BIT8);
		CheckSet16(DATA[6], BIT2, &Lights, BIT9);
	}
	//Reverse
	else if (SID == 0x4A6)
	{
		CheckSet16(DATA[0], BIT4, &Lights, BIT7);

	}
	//VIN
	else if (SID == 0x405)
	{
		switch (message->data[0])
		{
			case 0x10:
				VIN[0] = message->data[5];
				VIN[1] = message->data[6];
				VIN[2] = message->data[7];
			
				break;
			case 0x11:
				VIN[3] = message->data[1];
				VIN[4] = message->data[2];
				VIN[5] = message->data[3];
				VIN[6] = message->data[4];
				VIN[7] = message->data[5];
				VIN[8] = message->data[6];
				VIN[9] = message->data[7];
				break;
			case 0x12:
				VIN[10] = message->data[1];
				VIN[11] = message->data[2];
				VIN[12] = message->data[3];
				VIN[13] = message->data[4];
				VIN[14] = message->data[5];
				VIN[15] = message->data[6];
				VIN[16] = message->data[7];
				break;
			case 0x02:
				Tempu32 = message->data[5];
				Tempu32 = Tempu32<<8;
				Tempu32 |= message->data[6];
				Tempu32 = Tempu32<<8;
				Tempu32 |= message->data[7];

				Odometer = Tempu32;
				break;

		}
	}
	//Autobox
	else if (SID == 0x312)
	{
		Tempu8 = message->data[2] & 0x0F;
	
		switch (Tempu8)
		{
			case 0xF:
				//Park
				AutoBox = BIT0;
				Lights = Lights & ~BIT7;
				break;
			case 0xD:
				//Neutral
				AutoBox = BIT2;
				Lights = Lights & ~BIT7;
				break;
			case 0xE:
				//Reverse
				AutoBox = BIT1;
				Lights = Lights | BIT7;
				break;
			case 0xC:
				//Drive
				AutoBox = BIT3;
				Lights = Lights & ~BIT7;
				break;
			case 0x8:
				//Sport
				AutoBox = BIT4;
				Lights = Lights & ~BIT7;
				break;
			default:
				AutoBox = 0;
				Lights = Lights & ~BIT7;
		}

	}
	//Drive Modes
	else if (SID == 0x4C0)
	{
		Tempu8 = message->data[0] & 0xF0;
	
		switch (Tempu8)
		{
			case 0x00:
				//Normal
				ManualBox = BIT0;
				break;
			case 0x10:
				//Grass Gravel Snow
				ManualBox = BIT1;
				break;
			case 0x40:
				//Mud
				ManualBox = BIT2;
				break;
			case 0x30:
				//Sand
				ManualBox = BIT3;
				break;
			case 0x50:
				//Rocky Terrain
				ManualBox = BIT4;
				break;
			default:
				ManualBox = 0;
				
		}

	}




	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}
