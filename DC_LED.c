#include <p32xxxx.h>
#include "DC_LED.h"

/*********************************************
*	This file contains all the code required
*	to set the state of the LED status lights
*
*********************************************/

void InitLEDs(void)
{

	//Set required IO relationship for pins.
	AD1PCFGbits.PCFG2 = 1;
	AD1PCFGbits.PCFG3 = 1;
	AD1PCFGbits.PCFG7 = 1;
	AD1PCFGbits.PCFG9 = 1;

//	TRISBbits.TRISB2 = 0;
//	TRISBbits.TRISB3 = 0;
//	TRISBbits.TRISB7 = 0;
//	TRISBbits.TRISB9 = 0;

	LED_232 = 0; //Set inititally on to cause pulse
	LED_CAN = 1; //Set initially on to cause pulse
	LED_ECU_GREEN = 1; //LEDs are active low - set off initially
	LED_ECU_RED = 1; //LEDs are active low - set off initially


}

void ToggleLED_CAN(void)
{
	LED_CAN = !LED_CAN;
}


void ToggleLED_232(void)
{
	LED_232 = !LED_232;
}
