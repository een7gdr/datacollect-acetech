#ifndef _DC_LED_H
#define _DC_LED_H

//#define LED_232			PORTBbits.RB2
#define LED_232			LATBbits.LATB2
//#define LED_CAN			PORTBbits.RB3
#define LED_CAN			LATBbits.LATB3
//#define LED_ECU_GREEN	PORTBbits.RB7
#define LED_ECU_GREEN	LATBbits.LATB7
#define LED_ECU_RED		LATBbits.LATB9
//#define LED_ECU_RED		PORTBbits.RB9
#define ToggleLED_GREEN() {PORTBINV = 0x80;}
#define ToggleLED_RED()   {PORTBINV = 0x200;}

void InitLEDs(void);
void ToggleLED_CAN(void);
void ToggleLED_232(void);
void ToggleLED(unsigned int LED);

#endif