/***********************************************************************
*	DataCollect - Multi Interface Hub
*
*	Hardware Version
*
*	Version 1 (0x01) - 	Based on code developed to re-use hardware available from Carnation Deigns for 2 off
*							demo vehicles. 
*	
*	Version 2 (0x02) - 	Code re-written for new hardware type (0x02). New hardware to support
*							1 x RS485 connection
*							2 x CAN Connections (direct into PIC)
*							1 x CAN Connection (utilising CAN interface hardware / SPI bus) - TO DO
*
*	Version 3 (0x03) - 	Code changed to add wake on serial 1 interupt. This is to allow the device to be
*						reset even if it is in sleep mode.
*						The genisys power detection pin was problematic so this is no longer used
*						to detect presence of genisys. Now any serial RS485 data used to detect activity
*						and wake system.
*
*	Version 4 (0x04) - 	Code changed to prevent replies to the genisys system. As the CAN data is indertiminate
*						we cannot guarantee there is enuogh time to respond in a timely manner to genisys and if we cannot
*						then this might prevent/block other genisys modules from transmitting.
*
*	Version 5 (0x05) - 	Added support for GPIO inputs1-10 utilising another serial string identifier 'O'. Also amended code
*						so that CAN, genisys or GPIO data is sent only every 100ms if there is a change to a part of the data (the odometer
*						data is not checked due to code difficulties and fact it shouldnt change if no other inputs are changing).
*						If no data changes, the serial strings are sent every second. This should reduct the burden on the Tellit 
*						interpreter and give time for the data to catch up. There had been instances where the server data had begun to
*						lag by up to 20 minutes or so when there was a lot of data coming in. This would only be worsended by the GPIO data being
*						added to the serial stream.
*						GPIO are not check whilst the system is in sleep mode, when the system wakes on WDT (every 4s) it checks if the GPIO
*						inputs have changed, if so it will send the new data and reset the sleep timer. Max delay whilst asleep is therefore 4 seconds
*						This would only however be an issue if the vehicle CAN was asleep.
*
*	Version 6 (0x06) -	Allowed static message to be sent even if the vehicle type is not locked on the CANBus. This allows the HW & FW type
*						to be read even if vehicle not detected.
*
*	Version 7 (0x07) - 	Added RSG MCS32 Protocol using RS485 hardware. 
*						Added Address byte to static string to aid in debugging. Address 0 (reserved for genisys ECU in CDL systems) used to select RSG mode				
*						Added ProcessRSG function to decode received RSG data
*						Added rs485 received code to also pull out RSG data and relavent checksums based on address
*						Extra code to switch RS485 between 9 bit (genisys) and 8 bit (RSG/Other) based on Address selection
*						If address changed from/to zero then init485 run again to recongifure serial module
*						BL version number now also sent in static packet.			
*
*   
*   Version 8 (0x08) - 	Added code to support 2nd serial port which will be used initially to accept and Intellitec blue light controllers
*						Refactored code to start bringing out indvidial c files for each parent vehicle type as controls.c was too inflated
*
*	Version 9 (251) -	Added test code to work with AceTech system. This is using CAN1 and therefore works as a vehicle for this test release
*						
***********************************************************************/
/***********************************************************************
*	Vehicle Types:
*	0x01 - Mercedes ML, A & E Class 2014
*	0x02 - Mercedes Sprinter
*	0x03 - Land Rover Discovery 4
*	0x04 - Volvo V70/XC70 MY2012
*	0x05 - Skoda Octavia Scout
*	0x06 - Ford Mondeo
*	0x07 - Ford Focus
*	0x08 - BMW F15 PT-CAN
*	0x09 - Renault Master / Vauxhall Movano
*	0x0A - Honda CRV
*	0x0B - BMW X1 E84
*   0x0C - Honda CRV 2007
*	0x0D - Land Rover Discovery Sport
*	0x0E - NA
*	0x0F - Mercedes Vito 2015
*	0x10 - VW Transporter T5
*	0x11 - VW Transporter T6
*	0x12 - FIAT Ducato Euro6
*	0x13 - BMW F45/46 2 Series Tourer
*	0x14 - VAG Porsche Macan
*	0x15 - BMW 5 Series G Platform
*
***********************************************************************/

/***********************************************************************
*
*	Firmware Version
*
*	Version 1.00 (0x01) - 	Designed to work with Hardware V2.
*							Code works with bootloader and can be reset from serial (whilst awake)
*							HArdware sleeps if no genisys power and no CAN1 data for 4seconds
*							Hardware wakes on CAN or every 4s from watchdog. Will sleep if no CAN still
*							ir no genisys power.
*							BUG: On bootup, genisys power is detected regardless of state.
*							Mercedes A/C class and Mercedes Sprinter supported VT1 & VT2 so far
*							
*	Version 2 (0x02) - 		Added Landrover Discovery 4 high speed CAN to library for initial EoE vehicles
*
*	Version 3 (0x03) - 		Added Volvo V70 MY2012 library
*
*	Version 4 (0x04) - 		Amended Sprinter library to use 0x9E instead of 0x98 as vehicles in field not
*							always showing this data. Data will only update if non zero as zeroed data can occur
*							at CAN wake up.
*
*	Version 5 (0x05) - 		Adding Skoda Octavia/Scout to library still in progress. Needs road speed, coolant, oil.
*
*	Version 6 (0x06) - 		Added BMW F15 using PT-CAN. This may support other F-Series BMW vehicles but as yet untested
*							Added support for Renault Master 2010 (Vauxhall Movano) 
*
*	Version 7 (0x07) - 		Modified source of CAN fuel data for the Ford Mondeo vehicles as the original data entry
*							was undamped and affectd heavily when driving.
*
*	Version 8 (0x08) - 		Amended BMW library, now should support X5 (F15) and M4 (F88). Steering wheel is sent as direct date
*							On an X5 must multply by 4.73 to get wheel angle, on a car by 3.6. There is no resilient way to determine
*							which vehicle it is so must be performed on server side.
*
*	Version 9 (0x09) - 		Added Honda CRV (Dual CANBus) and initial BMW X1 E84 although no testing has been performed on the BMW
*							When the device goes to sleep, adblue, fuel, acc & brake are cleared down so that it is easier to detect
*							if there is an issue with the second CANBus.
*
*   Version 10 (0x0A) - 	Amended BMW E84 (X1) detection as wasnt setting vehicle type properly.	
*
*	Version 11 (0x0B)  -	Amended Renalt code for fuel to ignore MSB as this bit has another use causing large offsets in fuel readings. 
*
*	Version 12 (0x0C) - 	Added some signals for Mercedes Sprinter. Merc Runlock & Motor Temp
*
*	Version 13 (0x0D) - 	Added Honda CRV 2007 to library and 33.33kbps to baud test
*
*	Version 14 (0x0E) - 	Added Land Rover Discover Sport to library
*
*	Version 15 (0x0F) - 	Added Mercedes Vito 2015 to the library
*
*	Version 16 (0x10) - 	Added Transporter T5 to the library
*
*	Version 17 (0x11) - 	Added Transporter T6 to the library
*	Version 18 (0x12) - 	Amended Honda CRV and skoda vehicles as 0x7FFFF interleaved in odometer data giving strange readings
*	Version 19 (0x13) - 	Provisional release for Fiat Ducato duel CAN euro 6. Second CAN is run in normal mode not read only
*							REmoved check for steering wheel message on renault master to detect CAN on older vehicles. Not sure
*							is this data exists on the older vehicle as in 2010 this signal was not recorded
*							Shortened time spent in each baud rate as extra 50kbps test for new Fiat caused system to sleep before reaching this section
*	Version 20 (0x14) -		Amended Code MIL Bit usage on FIAT Ducato as same bit used for low fuel and oil level
*							Corrected else if statement in fiat fms code
*	Version 21 (0x15) - 	Added BMW 2 Series F46. Identical to F15 X5 but for steering wheel as X5 data not on 2 series
*							Added Ambient temp, seatbelts and front seat occupancy to HondaCRV2012
*							Added new vehicle, Porsche Macan to the library but untested as yet and only basic data
*	Version 22 (0x16) - 	Added BMW 5 Series G Platform. Uses same F15 decode routine with minor changes
*
*	Version 251 (0xFB) - 	Test release with no genieys reply. All SendblsChar and WriteQ (&Q_Bus) commented out in
*							DC_PROCESS.c apart from about message.
*
*
***********************************************************************/


// Configuration Bits
#pragma config FNOSC    = PRIPLL        // Oscillator Selection
#pragma config FPLLIDIV = DIV_4         // PLL Input Divider
#pragma config FPLLMUL  = MUL_16        // PLL Multiplier
#pragma config FPLLODIV = DIV_2         // PLL Output Divider
#pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
#pragma config FWDTEN   = ON           // Watchdog Timer
#pragma config WDTPS    = PS4096           // Watchdog Timer Postscale
#pragma config FCKSM    = CSDCMD        // Clock Switching & Fail Safe Clock Monitor
#pragma config OSCIOFNC = OFF           // CLKO Enable
#pragma config POSCMOD  = XT            // Primary Oscillator
#pragma config IESO     = OFF           // Internal/External Switch-over
#pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable
#pragma config CP       = OFF           // Code Protect
#pragma config BWP      = OFF           // Boot Flash Write Protect
#pragma config PWP      = OFF           // Program Flash Write Protect
#pragma config ICESEL   = ICS_PGx1      // ICE/ICD Comm Channel Select
#pragma config DEBUG    = OFF         // Debugger Disabled for Starter Kit

#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_PROCESS.h"
#include "DC_LED.h"
#include "DC_LIB_FERNO.h"

#define GenisysPower PORTGbits.RG3

#define BL1VERLOC 0x9FC01E9C
#define BL2VERLOC 0x9FC01EA4

void InitCPU(void);


//static const uint16_t __attribute__((space("prog"),address(0x1D07CFF0))) bootCRC = 0xdead;
//Upper 8 bits gives hardware version, lower 8 bits gives software version
// 0x0201	- Hardware version 2 (Data Collect board first iteration
//			- Firmware version 1 (first firmware release of this hardware
const uint32_t	__attribute__((space(prog),address(0x9D008000))) Version = 0xFB16;

unsigned long *BlFirmwareTest;
uint8_t BlFirmware;

//const uint8_t FirmwareVersion = 0x01;

char sInput_Protocol_Buffer[32];
uint8_t isCAN1MsgReceived = 0;
uint8_t isCAN2MsgReceived = 0;
uint8_t isCAN1WakeUp = 0;
uint8_t isTimerExpired = 0;
uint8_t onRx232PacketReq = 0;
uint8_t	TimerCounter1 = 0;
uint8_t TimerCounter2 = 0;
uint8_t TimerCounter3 = 0;
uint8_t TimerCounter4 = 0;
uint8_t TimerCounter5 = 0;
uint8_t Flash_Counter = 0;
uint8_t FlashDelay=0;
uint8_t Ignition_Off_Timer = 0;
uint8_t CANActivity = 0;
uint8_t GenisysActivity = 0;
uint8_t AddressCheck = 0;
uint8_t AddressCheckPrev = 0;
uint8_t RsgActivity = 0;
uint8_t RsgActioned = 0;
uint8_t Rs232_4Activity = 0;
uint8_t Rs232_4Actioned = 0;
uint8_t AceTechActivity = 0;
uint8_t AceTechActioned = 0;
extern uint8_t VehicleType;
/**********************************************************************
* Variable: 	uint8_t GenisysActivity
* Notes:		Set to true if valid keypad message received and reset
*				once the genisysActioned variable is true.
*				Used to ensure genisys serial data is transmitted in the
*				synchronised timed function
*				Also used to ensure
*				
**********************************************************************/
uint8_t GenisysActioned = 0;
/**********************************************************************
* Variable: 	uint8_t GenisysActioned
* Notes:		Cleared when valid genisys message received
*				Set once genisys message has been sent to ensure it only
*				gets sent once
*				
*				
**********************************************************************/
uint8_t GenisysSerialInv = 1;
/**********************************************************************
* Variable: 	uint8_t GenisysSerialInv
* Notes:		Incremented every 100ms and reset
*				every time an RS485 serial transmission is received
*				Used to detect when there is no serial activity at all
**********************************************************************/
uint8_t GenisysInactivityCounter = 0;
/**********************************************************************
* Variable: 	uint8_t GenisysInactivityCounter
* Notes:		Reset to zero every time a valid genisys message received
*				Incremented every 100ms if no valid message received
*				Used in conjunction with GenisysSerialInv to indicate
*				that serial data is coming in but it is not valid and 
*				controls the red LED
**********************************************************************/
uint8_t CANSleep = 0;
uint8_t StaticMessageCtr = 0;
uint8_t uBaudSelector = 0;
volatile uint8_t ToggleLEDGREEN = 0;
uint8_t SecondCANActive = 0;

volatile uint16_t RunningTimer=1;
volatile uint16_t LocalTimer;

extern uint8_t LocalInUseFlag;
extern struct q Q_Bus;

extern uint8_t Rs232_4PortStatus;

typedef struct CanDataStorage DataStorageType;
DataStorageType DataStorage[8];

enum Module_State
{
	STARTUP,
	NOT_LOCKED,
	BAUD_LOCKED,	
	LOCKED,
	SLEEP,
	ERROR
}State, Previous_State;

//Module_State State;

int main(void)
{
	
	State = STARTUP;
	Previous_State = STARTUP;
	InitCPU(); //Setup ports etc
	InitComms(); //Setup UART configuration
	AddressCheck = AddressCheckPrev =  Address_Read();
	if(AddressCheck == 0)
		InitStd485(); //Setup UART2 for RSG 8 bit data
	else
		InitGenisys(); //Setup UART2 for 9 bit genisys blue light interface

	InitLEDs(); 
	//InitSPI(); //Initialise SPI bus (bus 2)
	InitCAN(); //Setup CAN module configuration with default information

	ClearWDT();
	
	InitTimers(); // Set up timers to control program.

	//ToggleLED_RED();

	//Try and detect Bootloader version number
	//BlFirmwareTest = (unsigned long *) 0x9FC01EA4;
	BlFirmwareTest = (unsigned long *) BL1VERLOC;
	if (*BlFirmwareTest == 0x1) BlFirmware = 1;
	BlFirmwareTest = (unsigned long *) BL2VERLOC;
	if (*BlFirmwareTest == 0x2) BlFirmware = 2;
	

    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    // enable interrupts
    INTEnableInterrupts();

	/*  Main loop should control the whole process based on timers
		Every 100ms - Should transmit latest serial data to tracker unit

		If CAN data is received on relavent IDs, status variables need to be updated
		
	*/

	ClearWDT();

	while(1) //Main Program Loop
	{

		ClearWDT();


//CAN State Detection		
		if (State == LOCKED)
		{
			//If CAN data has been received then decode information
			if (isCAN1MsgReceived == TRUE)
			{
				isCAN1MsgReceived = FALSE;
				DecodeData();		
				//ToggleLED_GREEN();
			}

			if (isCAN2MsgReceived == TRUE)
			{
				isCAN2MsgReceived = FALSE;
				DecodeData2();
			}
		}
		//Used to detect if valid message received, if so then the baud rate is correct
		else if (State == NOT_LOCKED)
		{
			//Valid message received, therefore baud rate should be correct.
			if (isCAN1MsgReceived == TRUE)
			{
				State = BAUD_LOCKED;
				Previous_State = BAUD_LOCKED;
				isCAN1MsgReceived = FALSE;
				SendDebugMsg(9);

				CANUpdateChannel(CAN1, CAN_CHANNEL1);
				CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
			}
		}
		//Once baud rate locked, need to check incoming messages to detect vehicle type
		else if (State == BAUD_LOCKED)
		{
			if (isCAN1MsgReceived == TRUE)
			{
				isCAN1MsgReceived = FALSE;
				
				DetectVehicle();
			}
		}

//End of CAN Auto Detection routines

		//Check Serial overrun bit and reset if nesecary
		if (U1STAbits.OERR == 1)
		{
		 	
			U1STAbits.OERR = 0;
		}

//Check if CAN Interrupt has woken unit from sleep
		if(isCAN1WakeUp)
		{
			CANSetOperatingMode(CAN1, CAN_LISTEN_ONLY);
    			while(CANGetOperatingMode(CAN1) != CAN_LISTEN_ONLY);
			
			if (SecondCANActive == 1)
			{
				CANSetOperatingMode(CAN2, CAN_LISTEN_ONLY);
    				while(CANGetOperatingMode(CAN2) != CAN_LISTEN_ONLY);
			}

			State = Previous_State;
			CANSleep = 0; //Reset CAN sleep counter
			isCAN1WakeUp = FALSE;
		}

		



//Blues genisys reply commands
//Note - WriteQ commented out in DC_PROCESS.c so this should only be callsed
//if the ABOUT query is requested
		while (TestQ(&Q_Bus) && (U2STAbits.UTXBF == 0))
		{
			// Disable Receive and Enable Xmit
			PORTBbits.RB6 = 1;
	       	U2TXREG = ReadQ(&Q_Bus);
		}

		// If bus xmt buffer is empty then switch back to receive
		if (U2STAbits.TRMT)
		{
			// Enable Receive and Disable Xmit
			PORTBbits.RB6 = 0;
		}

		if (ToggleLEDGREEN != 0)
		{
			ToggleLED_GREEN();
			ToggleLEDGREEN = 0;
		}

		//Genisys data comes in at most every 100ms
		if ((GenisysActivity == 1) && (GenisysActioned == 0))
		{	
			SendGenisysStatus();
			GenisysActioned = 1;
		}
		
		if ((RsgActivity == 1) && (RsgActioned == 0))
		{
			SendRsgLiveStatus();
			SendRsgInfoStatus();
			RsgActioned = 1;
			LED_ECU_RED = 1;
		}



//End of Blues genisys reply command

		//Timer should expire every 25ms
		if (isTimerExpired)
		{
			ClearWDT();
			TimerCounter1++; //used to time serial data message send 100ms
			TimerCounter2++; //used to time serial static message send 5000ms
			TimerCounter3++; //used for LED flash 200ms
			TimerCounter4++; //0.5s Timer
			TimerCounter5++; //1s Timer
			isTimerExpired = 0;

			AddressCheck = Address_Read();
			
			if (AddressCheck != AddressCheckPrev)
			{
				//If new address is zero then mode MUST need changing to std 485
				if (AddressCheck == 0)
				{
					InitStd485();
				}
				//If old adress WAS zero then mode must need changing to genisys
				else if (AddressCheckPrev == 0)
				{
					InitGenisys();
				}

			}
			AddressCheckPrev = AddressCheck;

			//If 160 x 25ms cycles occur with no CAN activity 4s
			//then assume that CAN is in sleep mode.

			if (CANActivity == 1) CANSleep = 0;
			//if (GenisysPower == 1) CANSleep = 0;
			//PORTRG3 seems unreliable. The serial interupt for genisys will reset the CANsleep timer.
			//if ((PORTG & 0b1000) == 0b1000) CANSleep = 0;


			if (GenisysSerialInv < 3) CANSleep = 0;
			else
			{
					//Turn off ECU LEDs if no genisys power
					LED_ECU_GREEN = 1;
					LED_ECU_RED = 1;
			}


			if (CANSleep < 160) CANSleep ++;

			CANActivity = 0; //Reset flag

			//TimerCounter1 counts from 0-3 then resets, i.e. 4 x 25ms =100ms
			//TimerCounter1 == 1 ensures 100ms timing but half way between 2 CAN messages
			//
			if (TimerCounter1 == 1) 
			{
				
				if (AddressCheck == 1) //Intellitec Mode
				{
				//TimerCounter5 cycles every 1s. Ensure activity offset with CAN message any only
				//runs once every 1s
					if (TimerCounter5 < 4) 
					{
						if ((Rs232_4Activity == 1) && (Rs232_4Actioned == 0))
						{
							SendPmcStatus();
							Rs232_4Actioned = 1;
							LED_ECU_RED = 1;
						}
					}
				}
			}


			//Every 100ms (4 x 25ms counts)
			if (TimerCounter1 > 3)
			{			
				TimerCounter1 = 0;



				//CAN status message sent every 100ms if in LOCKED state and there has been recent CAN activity
				if (CANSleep < 160)
				{
					if ((State == LOCKED)&&(VehicleType != FERNO_TYPE1_TYPE)) SendStatusMsg();
					else if (State == LOCKED)
					{
						if ((AceTechActivity == 1) && (AceTechActioned == 0))
						{
							SendAceTechStatus();
							AceTechActioned = 1;
						}
					}
				}
				//Check / read GPIO inputs, return 1 if there is a change on the port to reset sleep timer. 
				if (CheckGPIO() == 1) CANSleep = 0;
				//Test GPIO Send command
				//SendGPIOMsg();

				if (GenisysSerialInv < 50) GenisysSerialInv++; //Increment serial counter, this is reset on serial RX event limited to count of 50

				//Genisys activity / status counter		
				//Address 0 RSG - Used to change RS485 serial port setup
				//Address 1 Intellitec
				//Address 2 Acetech
				if (AddressCheck > 2) //if genisys mode
				{

					if (GenisysActivity == 1)
					{
						GenisysInactivityCounter = 0;
						LED_ECU_RED = 1; //Turn off RED LED
					}
					else
					{
						if (GenisysSerialInv < 3) //i.e. if serial data has come in in the last 300ms
						{
							if (GenisysInactivityCounter < 5) GenisysInactivityCounter++;
							else //if serial data has come in recently, BUT 5 cycles have been completed without a valid genisys message
							{
								LED_ECU_RED = 0; // Turn ON Red LED active -ve
								LED_ECU_GREEN = 1; // Turn OFF Green LED active -ve
							}
						}

					}
					//To ensure that GenisysActivity flag is not reset until serial message has been sent
					//this could occur if last serial interupt occured between serial send statement and this point.
					if (GenisysActioned == 1)
					{
						GenisysActivity = 0;
						//GenisysActioned = 0; //This should only be reset on serial receive event
					}
				}
//End of genisys activity status
				else if(AddressCheck == 0) //RSG Mode
				{

					if (RsgActioned == 1)
					{
						RsgActivity = 0;
					}
				}
				else if (AddressCheck == 2) // AceTech CAN Mode
				{
					if (AceTechActioned == 1)
					{
						AceTechActivity = 0;
					}

				}
	
					
			}

			//5s
			if (TimerCounter2 > 204)
			{
				TimerCounter2 = 0;

				if (CANSleep < 160)
				{	
					//if (State == LOCKED) SendStaticMsg();
					SendStaticMsg(); // Send regardless of CAN locked state to allow detection of firmware level
				}
			}

			//Need to ensure that Serial Data is only sent if CAN data is still being received
			//If 4s expire with no CAN activity then stop sending data
			if (CANSleep < 160)
			{
				//Ensure that if still in sleep state, but due to be changed here (if WDT has woke unit and genisys power active for instance
				//that the CAN modules are made active again
				if (State == SLEEP)
				{
					CANSetOperatingMode(CAN1, CAN_LISTEN_ONLY);
    					while(CANGetOperatingMode(CAN1) != CAN_LISTEN_ONLY);
			
					if (SecondCANActive == 1)
					{
						CANSetOperatingMode(CAN2, CAN_LISTEN_ONLY);
    						while(CANGetOperatingMode(CAN2) != CAN_LISTEN_ONLY);
					}
				}

				State = Previous_State; //Allow State to leave sleep mode and return to previous state if CAN or genisys has re-started
			}
			//If CAN is asleep, clear timer counters to ensure data sent time period after CAN wakes.
			else 
			{

#ifndef __DEBUG
				TimerCounter1 = 0;
				TimerCounter2 = 0;
				LED_232 = 0;
				LED_CAN = 0;
				LED_ECU_RED = 1; //Inverted control of LED, turn red OFF
				LED_ECU_GREEN = 1; //Inverted control of, turn green OFF
				U1MODEbits.WAKE = 1;
				U2MODEbits.WAKE = 1;
				U4MODEbits.WAKE = 1;
				Rs232_4PortStatus = 0; //Reset serial port data stream status for PMC controller
				//need to check that genisys is also off before sleeping.
				
				//Clear Various variables to ensure they start with invalid data after wake up
				//The correct values will be set once the valid CAN commands are received
				ClearData();
					
		
				CANSetOperatingMode(CAN1, CAN_DISABLE);
			    	while(CANGetOperatingMode(CAN1) != CAN_DISABLE);
			
				if (SecondCANActive == 1)
				{
					CANSetOperatingMode(CAN2, CAN_DISABLE);
			    		while(CANGetOperatingMode(CAN2) != CAN_DISABLE);	
				}

				State = SLEEP;
				PowerSaveSleep();
				//Code below runs after wakeup
				LED_CAN = 1;
				//CANSleep = 150;
				if (StaticMessageCtr > 4)
				{
					SendStaticMsg(); //Ensure that static message is sent so that we can determine the firmware level even if CAN not working
					StaticMessageCtr = 0;
				}
				else StaticMessageCtr ++;
				ClearWDT();

				//Check if GPIO Changed
				if (CheckGPIO() == TRUE)
					CANSleep = 0;

				//CANSleep = 150; //Set counter so that enters sleep after 0.25s
				//need to see if wake up caused by watchdog or CAN receive event
#endif
			}

			//100ms Cycle to toggle CAN LED whilst active
			if (TimerCounter3 > 3)
			{
				
				Flash_Counter ++;

				if ((State == STARTUP)||(State == NOT_LOCKED))
				{
					FlashDelay = 1;
					//Fast Flash 200ms
				}
				else if (State == BAUD_LOCKED)
				{
					FlashDelay = 3;
					//medium flash 400ms
				}
				else if (State == LOCKED)
				{
					FlashDelay = 9;
					//Slow Flash 1000ms
				}
				else if ((State == SLEEP)||(State == ERROR))
				{
					if (LED_CAN == 0) FlashDelay = 19;
					else FlashDelay = 2;
					//Rapid pulse every 1s or so
					//Actually, once PIC enters sleep, pulse off time controlled by watchdog wake up
				}
				else
				{
					FlashDelay = 1;
					//Rapid flash 100ms
				}
		
				if (Flash_Counter >= FlashDelay)
				{
					ToggleLED_CAN();
					Flash_Counter = 0;
				}				

				TimerCounter3 = 0;
				
			}
			//0.5s Timer
			if (TimerCounter4 > 16)
			{
		

				if ((State == STARTUP)||(State == NOT_LOCKED))
				{
					//If there is CAN activity, system should wake from sleep
					//Identify the Baud rate firstly. Try cycling through available baud rates
					//unit valid messages are received. 
					
					State = NOT_LOCKED;
					Previous_State = NOT_LOCKED;

					uBaudSelector ++;

					switch (uBaudSelector)
					{	
						case 1: //125kbps
							InitCAN1(125000, 1, 1, 4, 4);
							SendDebugMsg(1);
							break;
						case 2: //100kbps
							InitCAN1(100000, 1, 1, 4, 4);
							SendDebugMsg(2);
							break;
						case 3: //83.333kbps
							InitCAN1(83333, 1, 1, 4, 4);
							SendDebugMsg(3);
							break;
						case 4: //500kbps
							InitCAN1(500000, 1, 1, 4, 4);
							SendDebugMsg(4);
							break;
						case 5: //33.33kbps
							InitCAN1(33333, 1, 1, 4, 4);
							SendDebugMsg(5);
							break;
						case 6: //250kbps
							InitCAN1(250000, 1, 1, 4, 4);
							SendDebugMsg(6);
							break;
						case 7: //50kbps
							InitCAN1(50000,1,1,4,4);
							SendDebugMsg(7);
							break;
						default:
							uBaudSelector = 0;
							SendDebugMsg(0);
					
					}
								
				}

				TimerCounter4 = 0;
			}

			if (TimerCounter5 > 39)
			{	
				TimerCounter5 = 0;
			}

		}//Timer expired
	}


}

void InitCPU(void)
{
	//Where pins are not connected, these are set to output pins to reduce current draw

	PORTGbits.RG3 = 0; // try and set output latch to low, even though its an input as software doesnt seem to pick up state properly otherwise.
	LATGbits.LATG3 = 0;

	TRISGbits.TRISG3 = 0; // Set to ouptut, in off state first before setting to input later. This may trick PORT to be detected properly.

	U1PWRCbits.USBPWR = 0; //ENsure USB is disabled so that RG3 operates as expected

	//PORTB
	//RB0 - ICSP Data - Programming pin only
	//RB1 - ICSP Clock - Programming pin only
	//RB2 - RS232 LED - Output
	//RB3 - CAN LED - Output
	//RB4 - CAN_Reset to CAN3 IC - Output
	//RB5 - SPI Chip select to CAN3 IC - Output
	//RB6 - RS485TXEnable - Output
	//RB7 - ECU Green LED - Output
	//RB8 - CAN2TX - Output
	//RB9 - ECU Red LED - Output
	//RB10 - ID0 - Input
	//RB11 - ID1 - Input
	//RB12 - ID2 - Input
	//RB13 - ID3 - Input
	//RB14 - CAN2RX - Input
	//RB15 - ID Common - Output
	
	TRISBbits.TRISB2 = 0;
	TRISBbits.TRISB3 = 0;
	TRISBbits.TRISB4 = 0;
	TRISBbits.TRISB5 = 0;
	TRISBbits.TRISB6 = 0;
	TRISBbits.TRISB7 = 0;
	TRISBbits.TRISB8 = 0;
	TRISBbits.TRISB9 = 0;
	TRISBbits.TRISB10 = 1;
	TRISBbits.TRISB11 = 1;
	TRISBbits.TRISB12 = 1;
	TRISBbits.TRISB13 = 1;
	TRISBbits.TRISB14 = 1;
	TRISBbits.TRISB15 = 0;

	AD1PCFGbits.PCFG2 = 1;
	AD1PCFGbits.PCFG3 = 1;
	AD1PCFGbits.PCFG4 = 1;
	AD1PCFGbits.PCFG5 = 1;
	AD1PCFGbits.PCFG6 = 1;
	AD1PCFGbits.PCFG7 = 1;
	AD1PCFGbits.PCFG8 = 1;
	AD1PCFGbits.PCFG9 = 1;
	AD1PCFGbits.PCFG10 = 1;
	AD1PCFGbits.PCFG11 = 1;
	AD1PCFGbits.PCFG12 = 1;
	AD1PCFGbits.PCFG13 = 1;

	PORTBbits.RB15 = 1;
	
//	PORTBbits.RB4 = 0;
//	PORTBbits.RB5 = 0;

//	PORTBbits.RB4 = 1;
//	PORTBbits.RB5 = 1;

	//PORTC
	//RC12 - OSC
	//RC13 - Input9 - Input
	//RC14 - Input10 - Input
	//RC15 - OSC
	TRISCbits.TRISC13 = 1;
	TRISCbits.TRISC14 = 1;

	//PORTD
	//RD0 - Input 1 - Input
	//RD1 - RS232 2 TX - Output
	//RD2 - RS232 1 RX - Input
	//RD3 - RS232 1 TX - Output
	//RD4 - Input2 - Input
	//RD5 - Input3 - Input
	//RD6 - Input4 - Input
	//RD7 - Input5 - Input
	//RD8 - Input6 - Input
	//RD9 - RS232 2 RX - Input
	//RD10 - Input7 - Input
	//RD11 - Input8 - Input

	
	TRISDbits.TRISD0 = 1;
	TRISDbits.TRISD4 = 1;
	TRISDbits.TRISD5 = 1;
	TRISDbits.TRISD6 = 1;
	TRISDbits.TRISD7 = 1;
	TRISDbits.TRISD8 = 1;
	TRISDbits.TRISD10 = 1;
	TRISDbits.TRISD11 = 1;

	
	//PORTE
	//RE0 - Output1 - Output
	//RE1 - Output2 - Output
	//RE2 - Output3 - Output
	//RE3 - Output4 - Output
	//RE4 - NC
	//RE5 - CAN Interrupt - Input
	//RE6 - CAN Receive Buffer 0 Full - Input
	//RE7 - CAN Receive Buffer 1 Full - Input
	TRISEbits.TRISE0 = 0;
	TRISEbits.TRISE1 = 0;
	TRISEbits.TRISE2 = 0;
	TRISEbits.TRISE3 = 0;
	TRISEbits.TRISE4 = 0;
	TRISEbits.TRISE5 = 1;
	TRISEbits.TRISE6 = 1;
	TRISEbits.TRISE7 = 1;

	PORTEbits.RE0 = 0;
	PORTEbits.RE1 = 0;
	PORTEbits.RE2 = 0;
	PORTEbits.RE3 = 0;

	//PORTF
	//RF0 - CAN1 RX
	//RF1 - CAN1 TX
	//RF3 - RS485 RX Enable - Output
	//RF4 - RS485 TX - Output
	//RF5 - RS485 RX - Input
	TRISFbits.TRISF0 = 1;
	TRISFbits.TRISF1 = 0;
	TRISFbits.TRISF3 = 0;
	TRISFbits.TRISF4 = 0;
	TRISFbits.TRISF5 = 1;

	PORTFbits.RF3 = 1;

	//PORTG
	//RG2 - NC
	//RG3 - GENISYS ACTIVE - Input
	//RG6 - CAN SCK - Output
	//RG7 - CAN SDI
	//RG8 - CAN SDO
	//RG9 - NC
	TRISGbits.TRISG2 = 0;
	TRISGbits.TRISG3 = 1;
	TRISGbits.TRISG6 = 0;
	TRISGbits.TRISG7 = 1;
	TRISGbits.TRISG8 = 0;
	TRISGbits.TRISG9 = 0;

//	PORTGbits.RG3 = 1; // try forcing output to 1 even though its an input??
//	PORTGbits.RG3 = 0; // try forcing output to 1 even though its an input??



}