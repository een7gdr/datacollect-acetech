#include <p32xxxx.h>
#include <plib.h>
#include "DC_PROCESS.h"
#include "DC_LED.h"

struct q Q_Local;
struct q Q_Bus;
typedef struct CanDataStorage DataStorageType;
extern DataStorageType DataStorage[8];
extern uint8_t BlFirmware;

extern uint8_t ToggleLEDGREEN;
extern uint8_t StartPacket; 	//Byte0
extern uint8_t NumBytes;		//Byte1
extern uint8_t StatusID;			//Byte2
extern uint8_t RPM;				//Byte3
extern uint8_t RoadSpeed;		//Byte4
extern uint16_t FuelLevel; 	//Bytes5-6
extern uint16_t FuelFlowCount;//Bytes7-8
extern uint8_t	Ignition;		//Byte9
extern uint8_t Accelerator;		//Byte10
extern uint8_t Brake;			//Byte11
extern uint16_t Lights;		//Byte12-13
extern uint8_t Wipers;			//Byte14
extern uint8_t CoolantTemp;		//Byte15
extern uint8_t AmbientTemp;		//Byte16
extern uint16_t SteeringWheel;//Bytes17-18 (Byte 15 Direction 0 Left, 1 Right)
extern uint8_t Controls;		//Byte19 (HB, Horn, Clutch, Reverse)
extern uint8_t AutoBox;			//Byte20
extern uint8_t ManualBox;		//Byte21
extern uint8_t Doors;			//Byte22
extern uint8_t SeatBelts;		//Byte23
extern uint8_t Occupancy;		//Byte24
extern uint8_t MILLights;		//Byte25
extern uint8_t Checksum;		//Byte26
extern uint8_t	EndOfPacket;		//Byte27

extern uint8_t RsgRxStatus;
extern uint8_t PmcBankChange[4];
extern uint8_t AddressCheck;


uint8_t HWType = 0x02; //Acetech dev HW type

uint8_t RPM_Temp;				//Byte3
uint8_t RoadSpeed_Temp;		//Byte4
uint16_t FuelLevel_Temp; 	//Bytes5-6
uint16_t FuelFlowCount_Temp;//Bytes7-8
uint8_t	Ignition_Temp;		//Byte9
uint8_t Accelerator_Temp;		//Byte10
uint8_t Brake_Temp;			//Byte11
uint16_t Lights_Temp;		//Byte12-13
uint8_t Wipers_Temp;			//Byte14
uint8_t CoolantTemp_Temp;		//Byte15
uint8_t AmbientTemp_Temp;		//Byte16
uint16_t SteeringWheel_Temp;//Bytes17-18 (Byte 15 Direction 0 Left, 1 Right)
uint8_t Controls_Temp;		//Byte19 (HB, Horn, Clutch, Reverse)
uint8_t AutoBox_Temp;			//Byte20
uint8_t ManualBox_Temp;		//Byte21
uint8_t Doors_Temp;			//Byte22
uint8_t SeatBelts_Temp;		//Byte23
uint8_t Occupancy_Temp;		//Byte24
uint8_t MILLights_Temp;		//Byte25

//Start Packet Byte0
extern uint8_t VehicleNumBytes;
extern uint8_t StaticID;			//Byte2

extern const uint32_t	Version;
extern uint8_t VehicleType;		//Byte5 Mercedes
extern uint32_t Odometer;	//Bytes6-9
extern uint8_t VIN[17];				//Bytes10-26

extern uint8_t GenisysID;			//Byte2
extern uint8_t GenisysNumBytes;			//Byte2

extern uint8_t RsgID;
extern uint8_t RsgNumBytes;

extern uint8_t RsgInfoID;
extern uint8_t RsgInfoNumBytes;

extern uint8_t PmcID;
extern uint8_t PmcNumBytes;

extern uint8_t AceTechID;
extern uint8_t AceTechNumBytes; 

extern uint16_t Command_Word0;
extern uint16_t Command_Word1;

extern uint8_t GenisysActivity;
extern uint8_t GenisysActioned;

extern uint8_t RsgActivity;
extern uint8_t RsgActioned;

extern uint8_t Rs232_4Activity;
extern uint8_t Rs232_4Actioned;

extern uint8_t AceTechBluesBytes[7]; 	//acetech blue
extern uint8_t AceTechBluesBytesPrev[7];
extern uint8_t AceTechBattBytes[5];	//acetech battery
extern uint8_t AceTechBattBytesPrev[5];


extern uint8_t GPIOByte0;
extern uint8_t GPIOByte1;
uint8_t GPIOByte0_Prev;
uint8_t GPIOByte1_Prev;
extern uint8_t GPIOID;
extern uint8_t GPIONumBytes;

uint8_t LedData[4];
uint8_t LedData_Prev[4];

uint8_t PmcData[41];
uint8_t PmcData_Prev[41];
uint8_t PmcData_Change[41];

uint8_t RsgOpData[4];
uint8_t RsgOpData_Prev[4];
uint8_t RsgOpDoData[4];
uint8_t RsgOpDoData_Prev[4];
uint8_t RsgOpFtData[2];
uint8_t RsgOpFtData_Prev[4];
uint8_t RsgIpData[4];
uint8_t RsgIpData_Prev[4];
uint8_t RsgCfgData[4];
uint8_t RsgCfgData_Prev[4];
uint32_t StreamIndex = 0;
uint32_t StreamIndex_Prev = 0;

struct ad
{
	uint8_t ModuleType;
	uint8_t SwVersionL;
	uint8_t SwVersionH;
} About = {MODULE_TYPE, SW_VERSION_H, SW_VERSION_L};


// ReadQ is called from non interrupt routines so needs to disable interrupts
// Note, TestQ must be called first to ensure the Q has contents
uint16_t ReadQ (struct q *qptr)
{
	uint16_t retval;

	retval = qptr->buffer[qptr->outptr];
	qptr->outptr++;
	qptr->outptr &= 0x7f;

	return retval;
}

// Test to see if Q has contents
uint8_t TestQ (struct q *qptr)
{
	uint8_t RetVal;


	if (qptr->inptr == qptr->outptr)
		RetVal = FALSE;
	else
		RetVal = TRUE;

	return RetVal;
}

// WriteQ is only called on interrupt so don't disable interrupts
uint8_t WriteQ (struct q *qptr, uint16_t value)
{
	if (((qptr->inptr + 1) & 0x7f) == qptr->outptr)
		return FALSE;					// overflow
	
	qptr->buffer[qptr->inptr] = value;
	qptr->inptr++;
	qptr->inptr &= 0x7f;
	return TRUE;
}

void ResetQ (struct q *qptr)
{
	qptr->inptr = 0;
	qptr->outptr = 0;
}

void SendLocalChar (uint8_t c)
{
	WriteQ (&Q_Local, c);
}

void SendLocalByte (uint8_t c)
{
	WriteQ (&Q_Local, c);
}

void SendLocalCharLRC (uint8_t c, int8_t *lrc)
{
	SendLocalChar (c);
	*lrc = *lrc ^ c;
}

void SendLocalByteLRC (uint8_t c, int8_t *lrc)
{
	SendLocalChar (c);
	*lrc = *lrc ^ c;
}

void SendLocal2BytesLRC (uint16_t b2, int8_t *lrc)
{
	uint8_t Temp;

	Temp = ((b2 >> 8) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;	
	
	Temp = ((b2) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;	
}

void SendLocal3BytesLRC (uint32_t b2, int8_t *lrc)
{
	uint8_t Temp;

	Temp = ((b2 >> 16) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;	

	Temp = ((b2 >> 8) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;

	Temp = ((b2) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;	
}

void SendLocal4BytesLRC (uint32_t b4, int8_t *lrc)
{
	uint8_t Temp;

	Temp = ((b4 >> 24) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;	

	Temp = ((b4 >> 16) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;	

	Temp = ((b4 >> 8) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;

	Temp = ((b4) & 0xFF);

	SendLocalChar (Temp);
	*lrc = *lrc ^ Temp;	

}

void SendLocalByteArrayLRC (uint8_t *byte, uint8_t numbytes, int8_t *lrc)
{
	uint8_t i;

	for (i=0; i< numbytes; i++)
	{
		SendLocalChar (byte[i]);
		*lrc = *lrc ^ byte[i];	
	}

}


void SendLocalHexByte (uint8_t b)
{
	SendLocalChar(ConvHex((b>>4) & 0xf));
	SendLocalChar(ConvHex(b & 0xf));
}

void SendLocalHexByteLRC (uint8_t b, int8_t *lrc)
{
	SendLocalCharLRC(ConvHex((b>>4) & 0xf),lrc);
	SendLocalCharLRC(ConvHex(b & 0xf),lrc);
}

void SendLocalHexNibbleLRC (uint8_t n, int8_t *lrc)
{
	SendLocalCharLRC(ConvHex(n & 0xf),lrc);
}

uint8_t SendBlsChar (uint16_t NextVal, char * lrc)
{
	WriteQ (&Q_Bus, NextVal);
	*lrc = *lrc + (NextVal & 0xff);
}


uint8_t ReadHexNibble(uint8_t *bptr)
{
	if ((*bptr >= '0') && (*bptr <= '9'))
		return (*bptr - '0');

	if ((*bptr >= 'a') && (*bptr <= 'f'))
		return (*bptr - 'a') + 10;

	if ((*bptr >= 'A') && (*bptr <= 'F'))
		return (*bptr - 'A') + 10;

	return 0xff;

}

uint16_t ReadHexByte (uint8_t *bptr)
{
	uint16_t n1,n2;

	n1 = ReadHexNibble(bptr);
	if (n1 == 0xff) return 0xffff;

	bptr++;

	n2 = ReadHexNibble(bptr);
	if (n2 == 0xff) return 0xffff;

	return (n1 << 4) + n2;
}

uint8_t ConvHex (uint8_t val)
{
	if (val < 10)
		return '0' + val;
	else
		return 'A' + val - 10;
}

void transmitQ_Local( void )
{
	//disable receive interrupts, enable transmit
	IEC0bits.U1RXIE = 0;
//	IEC0bits.U1TXIE = 1;


	// While the local port transmit buffer isn't full send the Q contents
	//while (TestQ(&Q_Local) && (U2STAbits.UTXBF == 0))
	while (TestQ(&Q_Local))
	{
		
		//ClrWdt();
//		ClearWDT();
		while(U1STAbits.UTXBF == 1);
			//ClrWdt();

		U1TXREG = (uint8_t)ReadQ(&Q_Local);
	}
	while(!U1STAbits.TRMT); //wait for transmit buffer to empty
	

	//enable receive interrupts, disable transmit
	IEC0bits.U1RXIE = 1;
	IEC0bits.U1TXIE = 0;

}

//Processes RS232 Packet depending on command type
void ProcessLocalMessage(uint8_t Buffer[], uint16_t BufferLen)
{
	uint16_t 	i,lrc1, nc, uID;
	uint32_t	uIDx, uBaud;
	uint8_t	uTemp1, uTemp2, uTemp3, uTemp4, uTemp5;
	uint8_t	uCANData[8];
	uint8_t lrc;
	char sTempBuffer[31];

	//Read in length field 
	nc = ReadHexByte (Buffer);

	if (nc == 0xffff) return;

//	if (nc != (BufferLen)) return;

	// Calculate the checksum
	lrc = 0;
	for (i=0;i<(nc-2);i++)
		lrc += Buffer[i];

	//lrc = -lrc;

	lrc1 = ReadHexByte(&Buffer[i]);
	if (lrc1 == 0xffff) return;

	if (lrc != (uint8_t)lrc1) return;

	
	if ((Buffer[2] == 'R') && (Buffer[3] == 'T'))
	{
		SoftReset();
	}

}


void ProcessRsgMessage(uint8_t Buffer[], uint16_t BufferLen)
{
	uint8_t headerLength;
	uint16_t StreamPtr = 0;
	uint8_t idx;

//	PORTEbits.RE0 = 1; //RTEST

	//ensure data is a data packet as opposed to other reserved RSG data streams
	if ((Buffer[pktIdLocation]) == 0x0B)
	{
		//ensure protocol revision is supported, i.e. only 0 at the moment
		if((Buffer[pktRevLengthLocation] & 0xF0) == 0)
		{
			headerLength = (uint8_t)Buffer[pktRevLengthLocation] & 0x0F;
			StreamPtr = (uint16_t)(headerLength + 8);	

			StreamIndex = (uint8_t)Buffer[pktRevLengthLocation + 1];
			//0x03 FF FF FF is full steam, last payload byte given MSB

			if (headerLength > 1)
				StreamIndex |= (uint32_t)(Buffer[pktRevLengthLocation + 2] << 8);
			if (headerLength > 2)
				StreamIndex |= (uint32_t)(Buffer[pktRevLengthLocation + 3] << 16);
			if (headerLength > 3)
				StreamIndex |= (uint32_t)(Buffer[pktRevLengthLocation + 4] << 24);

			for (idx = 0; idx < 26; idx++)
			{
				//If stream output is active for each stream type
				if ((StreamIndex&(1<<idx)) > 0)
				{
					switch (idx)
					{
						case pktHP1_8:
							RsgOpData[0] = Buffer[StreamPtr++];
							RsgOpFtData[0] = Buffer[StreamPtr++];
							RsgOpDoData[0] = Buffer[StreamPtr++];
							//StreamPtr += 2;
							break;
						case pktHP9_16:
							RsgOpData[1] = Buffer[StreamPtr++];
							RsgOpFtData[1] = Buffer[StreamPtr++];
							RsgOpDoData[1] = Buffer[StreamPtr++];
							//StreamPtr += 2;
							break;
						case pktLP1_8:
							RsgOpData[2] = Buffer[StreamPtr++];
							RsgOpDoData[2] = Buffer[StreamPtr++];
							//StreamPtr++;
							break;
						case pktLP9_16:
							RsgOpData[3] = Buffer[StreamPtr++];
							RsgOpDoData[3] = Buffer[StreamPtr++];
							//StreamPtr++;
							break;
						case pktPosIn:
							RsgIpData[0] = Buffer[StreamPtr++];
							RsgIpData[1] = Buffer[StreamPtr++];
							break;
						case pktNegIn:
							RsgIpData[2] = Buffer[StreamPtr++];
							RsgIpData[3] = Buffer[StreamPtr++];
							break;
						case pktCanHS1:
						case pktCanHS2:
						case pktCanHS3:
						case pktCanHS4:
							StreamPtr += 3;
							break;
						case pktCombo1_4:
						case pktCombo5_8:
							StreamPtr+=2;
							break;
						case pktCanData1:	
						case pktCanData2:	
						case pktCanData3:	
						case pktCanData4:
							StreamPtr+=2;
							break;
						case pktCanRPM:
							StreamPtr+=2;
							break;
						case pktCanSpeed:
							StreamPtr+=2;
							break;
						case pktPriBatt:
							StreamPtr+=2;
							break;
						case pktSecBatt:
							StreamPtr+=4;
							break;
						case pktCfg1:
							RsgCfgData[0] = Buffer[StreamPtr++];
							break;	
						case pktCfg2:
							RsgCfgData[1] = Buffer[StreamPtr++];
							break;
						case pktCfg3:
							RsgCfgData[2] = Buffer[StreamPtr++];
							break;
						case pktCfg4:
							RsgCfgData[3] = Buffer[StreamPtr++];
							break;
					}
				}//end of if stream active check
			}//end of for loop
			ToggleLEDGREEN = 1;
			RsgActivity = 1;
			RsgActioned = 0;
		}//end of supported revision check
	}//end of if supported ID check

//	PORTEbits.RE0 = 0; //RTEST
}

void ProcessBlsMessage(uint8_t Buffer[], uint16_t BufferLen)
{
		char lrc = 0;
		uint8_t i;

		switch (Buffer[2])
		{
		case 1:
			//Genisys Reset command		
			//SendBlsChar(Address_Read() + 0x100, &lrc);// Address
			//SendBlsChar(4, &lrc);						// Count
			//SendBlsChar(0xFF, &lrc);					// Response Message
			//WriteQ (&Q_Bus, -lrc);						// Lrc
			break;
		
		case 2:
			//Read data - programming
			//SendBlsChar(Address_Read() + 0x100, &lrc);// Address
			//SendBlsChar(5, &lrc);						// Count
			//SendBlsChar(0xFE, &lrc);					// Response Message
			//SendBlsChar(0,&lrc);						// Data Byte
			//WriteQ (&Q_Bus, -lrc);						// Lrc
			break;

		case 3:
			//Write Data - programming
			//SendBlsChar(Address_Read() + 0x100, &lrc);// Address
			//SendBlsChar(4, &lrc);						// Count
			//SendBlsChar(0xFD, &lrc);					// Response Message
			//WriteQ (&Q_Bus, -lrc);						// Lrc
			break;

		case 4:
			//Clock
			//SendBlsChar(Address_Read() + 0x100, &lrc);// Address
			//SendBlsChar(4, &lrc);						// Count
			//SendBlsChar(0xFC, &lrc);					// Response Message
			//WriteQ (&Q_Bus, -lrc);						// Lrc
			break;
		case 5:
			//About Message - Respond 
			SendBlsChar(Address_Read() + 0x100, &lrc); //Address
			SendBlsChar(7, &lrc);	//Count
			SendBlsChar(0xFB, &lrc); //Response Message
			SendBlsChar(About.ModuleType, &lrc);	// Module Type
			SendBlsChar(About.SwVersionL, &lrc);	// Sw Version Low 
			SendBlsChar(About.SwVersionH, &lrc);	// Sw Version High
			WriteQ (&Q_Bus, -lrc);						// Lrc
		break;
			break;
		case 6:
			//Input Module - Do nothing
			break;
		case 7:
			//Output Module - Do nothing
			break;
		case 8:
			//Keypad Message
			//SendBlsChar(Address_Read() + 0x100, &lrc);// Address
			//SendBlsChar(8, &lrc);						// Count
			//SendBlsChar(0xF8, &lrc);					// Response Message
			//SendBlsChar(0xff, &lrc);		// Data 1
			//SendBlsChar(0xff, &lrc);		// Data 2
			//SendBlsChar(0xff, &lrc);		// Data 3
			//SendBlsChar(0xff, &lrc);		// Data 4
			//WriteQ (&Q_Bus, -lrc);						// Lrc
			
			LedData[0] = Buffer[5];
			LedData[1] = Buffer[6];
			LedData[2] = Buffer[7];
			LedData[3] = Buffer[8];

			ToggleLEDGREEN = 1;
			GenisysActivity = 1;
			GenisysActioned = 0;
			break;
		case 9:
			//LCD Message
			//SendBlsChar(Address_Read() + 0x100, &lrc);		// Address
			//SendBlsChar(4, &lrc);						// Count
			//SendBlsChar(0xF7, &lrc);					// Response Message
			//WriteQ (&Q_Bus, -lrc);						// Lrc
			ToggleLEDGREEN = 1;
			//GenisysActivity = 1;
			break;

		default:
			break;



		}
	
	
		

}

void SendStatusMsg(void)
{
	uint8_t lrc = 0;
	uint8_t i;
	uint8_t tempByte;
	static uint8_t StaticCount=0;
	uint16_t Test;


//Remember - sending binary data
//Backup Data to be sent

	Test = (RPM_Temp^RPM) & 0xFF;
	Test |= (RoadSpeed_Temp^RoadSpeed) & 0xFF;
	Test |= FuelLevel_Temp^FuelLevel;
	Test |= FuelFlowCount_Temp^FuelFlowCount;
	Test |= (Ignition_Temp^Ignition) & 0xFF;
	Test |= (Accelerator_Temp^Accelerator) & 0xFF;
	Test |= (Brake_Temp^Brake) & 0xFF;
	Test |= Lights_Temp^Lights;
	Test |= (Wipers_Temp^Wipers) & 0xFF;
	Test |= (CoolantTemp_Temp^CoolantTemp) & 0xFF;
	Test |= (AmbientTemp_Temp^AmbientTemp) & 0xFF;
	Test |= SteeringWheel_Temp^SteeringWheel;
	Test |= (Controls_Temp^Controls) & 0xFF;
	Test |= (AutoBox_Temp^AutoBox) & 0xFF;
	Test |= (ManualBox_Temp ^ ManualBox) & 0xFF;
	Test |= (Doors_Temp^Doors) & 0xFF;
	Test |= (SeatBelts_Temp^SeatBelts) & 0xFF;
	Test |= (Occupancy_Temp^Occupancy) & 0xFF;
	Test |= (MILLights_Temp^MILLights) & 0xFF;
	
	if((Test!=0)||(StaticCount>9))
	{
		ResetQ(&Q_Local);

		RPM_Temp = RPM;
		RoadSpeed_Temp = RoadSpeed;
		FuelLevel_Temp = FuelLevel;
		FuelFlowCount_Temp = FuelFlowCount;
		Ignition_Temp = Ignition;
		Accelerator_Temp = Accelerator;
		Brake_Temp = Brake;
		Lights_Temp = Lights;
		Wipers_Temp = Wipers;
		CoolantTemp_Temp = CoolantTemp;
		AmbientTemp_Temp = AmbientTemp;
		SteeringWheel_Temp = SteeringWheel;
		Controls_Temp = Controls;
		AutoBox_Temp = AutoBox;
		ManualBox_Temp = ManualBox;
		Doors_Temp = Doors;
		SeatBelts_Temp = SeatBelts;
		Occupancy_Temp = Occupancy;
		MILLights_Temp = MILLights;
	
		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (NumBytes);		//28
		SendLocalByteLRC (StatusID, &lrc);	//C
		SendLocal3BytesLRC (Odometer, &lrc);
		SendLocalByteLRC (RPM, &lrc);
		SendLocalByteLRC (RoadSpeed, &lrc);
		SendLocal2BytesLRC (FuelLevel, &lrc);
		SendLocal2BytesLRC (FuelFlowCount, &lrc);
		SendLocalByteLRC (Ignition, &lrc);
		SendLocalByteLRC (Accelerator, &lrc);
		SendLocalByteLRC (Brake, &lrc);
		SendLocal2BytesLRC (Lights, &lrc);
		SendLocalByteLRC (Wipers, &lrc);
		SendLocalByteLRC (CoolantTemp, &lrc);
		SendLocalByteLRC (AmbientTemp, &lrc);
		SendLocal2BytesLRC (SteeringWheel, &lrc);
		//SendLocal2BytesLRC (Test, &lrc);
		SendLocalByteLRC (Controls, &lrc);
		SendLocalByteLRC (AutoBox, &lrc);	
		SendLocalByteLRC (ManualBox, &lrc);
		SendLocalByteLRC (Doors, &lrc);
		SendLocalByteLRC (SeatBelts, &lrc);
		SendLocalByteLRC (Occupancy, &lrc);
		SendLocalByteLRC (MILLights, &lrc);
		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);
	
	//Wipers = Wipers + 1;

		transmitQ_Local();

		ToggleLED_232();

		StaticCount = 0;
	}	
	else StaticCount++;

}


void SendGPIOMsg(void)
{
	uint8_t lrc = 0;
	uint8_t i;
	static uint8_t StaticCount=0;
	uint8_t Test;

	Test = GPIOByte0^GPIOByte0_Prev;
	Test |= GPIOByte1^GPIOByte1_Prev;


	if	((Test!=0)||(StaticCount > 9))
	{
		ResetQ(&Q_Local);

//Remember - sending binary data

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (GPIONumBytes);		//4
		SendLocalByteLRC (GPIOID, &lrc);	//O
	
		SendLocalByteLRC (GPIOByte1, &lrc);
		SendLocalByteLRC (GPIOByte0, &lrc);


		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);
	
	
		transmitQ_Local();	

		ToggleLED_232();

		StaticCount = 0;

		GPIOByte0_Prev = GPIOByte0;
		GPIOByte1_Prev = GPIOByte1;
	}
	else StaticCount++;



}

void ProcessRs232_4Message(uint8_t Buffer[], uint16_t BufferLen)
{
	uint8_t i,j;
	uint8_t Chk;

	if (AddressCheck == 1)
	{ 

		PmcData[0] = Buffer[0];

		for (i=1; i<6; i++)
		{
			PmcData[i] = 0;
			PmcData[i+5] = 0;
			PmcData[i+10] = 0;
			PmcData[i+15] = 0;
			PmcData[i+20] = 0;
			PmcData[i+25] = 0;
			PmcData[i+30] = 0;
			PmcData[i+35] = 0;
	
			for (j=0; j<16; j+=2)
			{
				PmcData[i] = PmcData[i]<<1 | (Buffer[15-j]>>(i-1)) & (0x01); //H1-5(MSB) - A1-5(LSB) Blue
				PmcData[i+5] = PmcData[i+5]<<1 | (Buffer[16-j]>>(i-1)) & (0x01); //H6-10(MSB) - A6-10(LSB) Blue

				PmcData[i+10] = PmcData[i+10]<<1 | (Buffer[31-j]>>(i-1)) & (0x01); //P1-5(MSB) - I1-5(LSB) Blue
				PmcData[i+15] = PmcData[i+15]<<1 | (Buffer[32-j]>>(i-1)) & (0x01); //P6-10(MSB) - I6-10(LSB) Blue

				PmcData[i+20] = PmcData[i+20]<<1 | (Buffer[47-j]>>(i-1)) & (0x01); //H1-5(MSB) - A1-5(LSB) Yellow
				PmcData[i+25] = PmcData[i+25]<<1 | (Buffer[48-j]>>i) & (0x01); //H6-10(MSB) - A6-10(LSB) Yellow

				PmcData[i+30] = PmcData[i+30]<<1 | (Buffer[63-j]>>(i-1)) & (0x01); //P1-5(MSB) - I1-5(LSB) Yellow
				PmcData[i+35] = PmcData[i+35]<<1 | (Buffer[64-j]>>(i-1)) & (0x01); //P6-10(MSB) - I6-10(LSB) Yellow

			}

			if (PmcData_Prev[i] != PmcData[i]) PmcBankChange[0] = 1;
			if (PmcData_Prev[i+5] != PmcData[i+5]) PmcBankChange[0] = 1;
			if (PmcData_Prev[i+10] != PmcData[i+10]) PmcBankChange[1] = 1;
			if (PmcData_Prev[i+15] != PmcData[i+15]) PmcBankChange[1] = 1;
			if (PmcData_Prev[i+20] != PmcData[i+20]) PmcBankChange[2] = 1;
			if (PmcData_Prev[i+25] != PmcData[i+25]) PmcBankChange[2] = 1;
			if (PmcData_Prev[i+30] != PmcData[i+30]) PmcBankChange[3] = 1;
			if (PmcData_Prev[i+35] != PmcData[i+35]) PmcBankChange[3] = 1;

			PmcData_Change[i] |= PmcData[i] ^ PmcData_Prev[i];
			PmcData_Change[i+5] |= PmcData[i+5] ^ PmcData_Prev[i+5];
			PmcData_Change[i+10] |= PmcData[i+10] ^ PmcData_Prev[i+10];
			PmcData_Change[i+15] |= PmcData[i+15] ^ PmcData_Prev[i+15];
			PmcData_Change[i+20] |= PmcData[i+20] ^ PmcData_Prev[i+20];
			PmcData_Change[i+25] |= PmcData[i+25] ^ PmcData_Prev[i+25];
			PmcData_Change[i+30] |= PmcData[i+30] ^ PmcData_Prev[i+30];
			PmcData_Change[i+35] |= PmcData[i+35] ^ PmcData_Prev[i+35];
			
		}
	 
		ToggleLEDGREEN = 1;
		Rs232_4Activity = 1;
		Rs232_4Actioned = 0;
	}

}


void SendPmcStatus(void)
{
	uint8_t lrc = 0;
	uint8_t i;
	uint8_t bank;
	uint8_t NewData; // Used to ensure data sent if changed even if changed back between tranmissions
	static uint8_t StaticCount[4];
	//uint8_t Test;

	//40 Bytes potentially to send, split into 4 x 10 bytes
	//each 10 byte chunk will only be sent if something changed in that block

	//Bank 1 Blue loop A-H
	if	((PmcBankChange[0]!=0)||(StaticCount[0] > PMCREPEAT))
	{
		ResetQ(&Q_Local);

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (PmcNumBytes);		//22
		SendLocalByteLRC (PmcID, &lrc);	//P
		bank = 1;
		SendLocalByteLRC (bank, &lrc);

		for (i=1; i<11; i++)
		{
			NewData = PmcData_Prev[i] ^ PmcData_Change[i];
			SendLocalByteLRC(NewData, &lrc);
			PmcData_Prev[i] = NewData;
			PmcData_Change[i] = 0; //clear toggle mask to ensure we dont toggle again before this is recalculated
		}

		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);

		transmitQ_Local();
		StaticCount[0] = 0;
		PmcBankChange[0] = 0;
	}
	else if((PmcData[0]&0x1) !=0) StaticCount[0]++;
	//Bank 2 Blue loop I-P
	if	((PmcBankChange[1]!=0)||(StaticCount[1] > PMCREPEAT))
	{
		ResetQ(&Q_Local);

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (PmcNumBytes);		//22
		SendLocalByteLRC (PmcID, &lrc);	//P
		bank = 2;
		SendLocalByteLRC (bank, &lrc);

		for (i=11; i<21; i++)
		{
			NewData = PmcData_Prev[i] ^ PmcData_Change[i];
			SendLocalByteLRC(NewData, &lrc);
			PmcData_Prev[i] = NewData;
			PmcData_Change[i] = 0; //clear toggle mask to ensure we dont toggle again before this is recalculated
		}

		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);

		transmitQ_Local();
		StaticCount[1] = 0;
		PmcBankChange[1] = 0;
	}
	else if((PmcData[0]&0x1) !=0) StaticCount[1]++;
	//Bank 1 Yellow loop A-H
	if	((PmcBankChange[2]!=0)||(StaticCount[2] > PMCREPEAT))
	{
		ResetQ(&Q_Local);

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (PmcNumBytes);		//22
		SendLocalByteLRC (PmcID, &lrc);	//P
		bank = 3;
		SendLocalByteLRC (bank, &lrc);

		for (i=21; i<31; i++)
		{
			NewData = PmcData_Prev[i] ^ PmcData_Change[i];
			SendLocalByteLRC(NewData, &lrc);
			PmcData_Prev[i] = NewData;
			PmcData_Change[i] = 0; //clear toggle mask to ensure we dont toggle again before this is recalculated
		}

		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);

		transmitQ_Local();
		StaticCount[2] = 0;
		PmcBankChange[2] = 0;
	}
	else if((PmcData[0]&0x2) !=0) StaticCount[2]++;
	//Bank 2 Yellow loop I-P
	if	((PmcBankChange[3]!=0)||(StaticCount[3] > PMCREPEAT))
	{
		ResetQ(&Q_Local);

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (PmcNumBytes);		//22
		SendLocalByteLRC (PmcID, &lrc);	//P
		bank = 4;
		SendLocalByteLRC (bank, &lrc);

		for (i=31; i<41; i++)
		{
			NewData = PmcData_Prev[i] ^ PmcData_Change[i];
			SendLocalByteLRC(NewData, &lrc);
			PmcData_Prev[i] = NewData;
			PmcData_Change[i] = 0; //clear toggle mask to ensure we dont toggle again before this is recalculated
		}

		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);

		transmitQ_Local();
		StaticCount[3] = 0;
		PmcBankChange[3] = 0;
	}
	else if((PmcData[0]&0x2) !=0) StaticCount[3]++;


	

}

void SendStaticMsg(void)
{
	uint8_t lrc = 0;
	uint8_t i;
	uint8_t tempByte;
	uint8_t HardwareVersion;
	uint8_t FirmwareVersion;

	HardwareVersion = (uint8_t)(Version >> 8);
	FirmwareVersion = (uint8_t)(Version);
	//FirmwareVersion = BlFirmware;

	ResetQ(&Q_Local);

//Remember - sending binary data

	SendLocalByte (StartPacket);	//0x02
	SendLocalByte (VehicleNumBytes);		//22
	SendLocalByteLRC (StaticID, &lrc);	//C
	SendLocalByteLRC (HardwareVersion, &lrc);
	SendLocalByteLRC (FirmwareVersion, &lrc);
	SendLocalByteLRC (VehicleType, &lrc);
	//SendLocalByteLRC(RsgRxStatus, &lrc);
	SendLocalByteArrayLRC (&VIN[0], 17, &lrc);
	SendLocalByteLRC (Address_Read(), &lrc);
	SendLocalByteLRC (BlFirmware, &lrc);
	SendLocalByteLRC (HWType, &lrc);
	SendLocalByte (lrc);
	SendLocalByte (EndOfPacket);
	
	//Wipers = Wipers + 1;

	transmitQ_Local();

	ToggleLED_232();	

}

void SendGenisysStatus(void)
{
	uint8_t lrc = 0;
	uint8_t i;
	static uint8_t StaticCount=0;
	uint8_t Test;

	Test = LedData[0]^LedData_Prev[0];
	Test |= LedData[1]^LedData_Prev[1];
	Test |= LedData[2]^LedData_Prev[2];
	Test |= LedData[3]^LedData_Prev[3];


	if	((Test!=0)||(StaticCount > 9))
	{
		
		ResetQ(&Q_Local);	

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (GenisysNumBytes);		//6
		SendLocalByteLRC (GenisysID, &lrc);	//C
		SendLocalByteLRC (LedData[0], &lrc);	//C
		SendLocalByteLRC (LedData[1], &lrc);	//C
		SendLocalByteLRC (LedData[2], &lrc);	//C
		SendLocalByteLRC (LedData[3], &lrc);	//C
		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);
	
		transmitQ_Local();

		ToggleLED_232();
		LedData_Prev[0] = LedData[0];
		LedData_Prev[1] = LedData[1];
		LedData_Prev[2] = LedData[2];
		LedData_Prev[3] = LedData[3];

		StaticCount=0;
	}
	else StaticCount++;

}

void SendRsgInfoStatus(void)
{
	uint8_t lrc = 0;
	uint8_t i;
	static uint8_t StaticCount=0;
	uint8_t Test;

//	Test = LedData[0]^LedData_Prev[0];
//	Test |= LedData[1]^LedData_Prev[1];
//	Test |= LedData[2]^LedData_Prev[2];
//	Test |= LedData[3]^LedData_Prev[3];

//	Test =  RsgOpData_Prev[0] ^ RsgOpData[0];		
//	Test |= RsgOpData_Prev[1] ^ RsgOpData[1];
//	Test |= RsgOpData_Prev[2] ^ RsgOpData[2];
//	Test |= RsgOpData_Prev[3] ^ RsgOpData[3];	

	Test = RsgOpFtData_Prev[0] ^ RsgOpFtData[0];		
	Test |= RsgOpFtData_Prev[1] ^ RsgOpFtData[1];

	Test |= RsgOpDoData_Prev[0] ^ RsgOpDoData[0];		
	Test |= RsgOpDoData_Prev[1] ^ RsgOpDoData[1];
	Test |= RsgOpDoData_Prev[2] ^ RsgOpDoData[2];
	Test |= RsgOpDoData_Prev[3] ^ RsgOpDoData[3];

//	Test |= RsgIpData_Prev[0] ^ RsgIpData[0];		
//	Test |= RsgIpData_Prev[1] ^ RsgIpData[1];
//	Test |= RsgIpData_Prev[2] ^ RsgIpData[2];
//	Test |= RsgIpData_Prev[3] ^ RsgIpData[3];

//	Test |= RsgCfgData_Prev[0] ^ RsgCfgData[0];		
//	Test |= RsgCfgData_Prev[1] ^ RsgCfgData[1];
//	Test |= RsgCfgData_Prev[2] ^ RsgCfgData[2];
//	Test |= RsgCfgData_Prev[3] ^ RsgCfgData[3];
	

	if	((Test!=0)||(StaticCount > 9))
	{
	
//		RsgOpData_Prev[0] = RsgOpData[0];		
//		RsgOpData_Prev[1] = RsgOpData[1];
//		RsgOpData_Prev[2] = RsgOpData[2];
//		RsgOpData_Prev[3] = RsgOpData[3];	

		RsgOpFtData_Prev[0] = RsgOpFtData[0];		
		RsgOpFtData_Prev[1] = RsgOpFtData[1];

		RsgOpDoData_Prev[0] = RsgOpDoData[0];		
		RsgOpDoData_Prev[1] = RsgOpDoData[1];
		RsgOpDoData_Prev[2] = RsgOpDoData[2];
		RsgOpDoData_Prev[3] = RsgOpDoData[3];

//		RsgIpData_Prev[0] = RsgIpData[0];		
//		RsgIpData_Prev[1] = RsgIpData[1];
//		RsgIpData_Prev[2] = RsgIpData[2];
//		RsgIpData_Prev[3] = RsgIpData[3];

//		RsgCfgData_Prev[0] = RsgCfgData[0];		
//		RsgCfgData_Prev[1] = RsgCfgData[1];
//		RsgCfgData_Prev[2] = RsgCfgData[2];
//		RsgCfgData_Prev[3] = RsgCfgData[3];
		
		ResetQ(&Q_Local);	

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (RsgInfoNumBytes);		//
		SendLocalByteLRC (RsgInfoID, &lrc);	//S
		SendLocal4BytesLRC (StreamIndex, &lrc);
//		SendLocalByteLRC (RsgOpData[0], &lrc);	//C
		SendLocalByteLRC (RsgOpFtData[0], &lrc);	//C
		SendLocalByteLRC (RsgOpDoData[0], &lrc);
//		SendLocalByteLRC (RsgOpData[1], &lrc);	//C
		SendLocalByteLRC (RsgOpFtData[1], &lrc);	//C
		SendLocalByteLRC (RsgOpDoData[1], &lrc);
//		SendLocalByteLRC (RsgOpData[2], &lrc);	//C
		SendLocalByteLRC (RsgOpDoData[2], &lrc);
//		SendLocalByteLRC (RsgOpData[3], &lrc);	//C
		SendLocalByteLRC (RsgOpDoData[3], &lrc);
//		SendLocalByteLRC (RsgIpData[0], &lrc);	//C
//		SendLocalByteLRC (RsgIpData[1], &lrc);	//C
//		SendLocalByteLRC (RsgIpData[2], &lrc);	//C
//		SendLocalByteLRC (RsgIpData[3], &lrc);	//C
//		SendLocalByteLRC (RsgCfgData[0], &lrc);	//C
//		SendLocalByteLRC (RsgCfgData[1], &lrc);	//C
//		SendLocalByteLRC (RsgCfgData[2], &lrc);	//C
//		SendLocalByteLRC (RsgCfgData[3], &lrc);	//C
		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);
	
		transmitQ_Local();

		ToggleLED_232();

		StaticCount=0;
	}
	else StaticCount++;

}

void SendAceTechStatus(void)
{
	uint8_t Test;
	static uint8_t StaticCount = 0;
	uint8_t lrc = 0;

	//Test = AceTechBluesBytesPrev[0] ^ AceTechBluesBytes[0];
	//Test |= AceTechBluesBytesPrev[1] ^ AceTechBluesBytes[1];
	//Test |= AceTechBluesBytesPrev[2] ^ AceTechBluesBytes[2];
	//Test |= AceTechBluesBytesPrev[3] ^ AceTechBluesBytes[3];
	//Test |= AceTechBluesBytesPrev[4] ^ AceTechBluesBytes[4];
	//Test |= AceTechBluesBytesPrev[5] ^ AceTechBluesBytes[5];
	//Test |= AceTechBluesBytesPrev[6] ^ AceTechBluesBytes[6];
	//Test |= AceTechBattBytesPrev[0] ^ AceTechBattBytes[0];
	//Test |= AceTechBattBytesPrev[1] ^ AceTechBattBytes[1];
	//Test |= AceTechBattBytesPrev[2] ^ AceTechBattBytes[2];
	//Test |= AceTechBattBytesPrev[3] ^ AceTechBattBytes[3];
	//Test |= AceTechBattBytesPrev[4] ^ AceTechBattBytes[4];

	//if	((Test!=0)||(StaticCount > 5))
	//{

		ResetQ(&Q_Local);	

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (AceTechNumBytes);		//14
		SendLocalByteLRC (AceTechID, &lrc);	//A		
		SendLocalByteLRC (AceTechBluesBytes[0], &lrc);
		SendLocalByteLRC (AceTechBluesBytes[1], &lrc);
		SendLocalByteLRC (AceTechBluesBytes[2], &lrc);
		SendLocalByteLRC (AceTechBluesBytes[3], &lrc);
		SendLocalByteLRC (AceTechBluesBytes[4], &lrc);
		SendLocalByteLRC (AceTechBluesBytes[5], &lrc);
		SendLocalByteLRC (AceTechBluesBytes[6], &lrc);
		SendLocalByteLRC (AceTechBattBytes[0], &lrc);
		SendLocalByteLRC (AceTechBattBytes[1], &lrc);
		SendLocalByteLRC (AceTechBattBytes[2], &lrc);
		SendLocalByteLRC (AceTechBattBytes[3], &lrc);
		SendLocalByteLRC (AceTechBattBytes[4], &lrc);
		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);
	
		transmitQ_Local();

		ToggleLED_232();

		StaticCount=0;
	//}
	//else StaticCount++;	

}

void SendRsgLiveStatus(void)
{
	uint8_t lrc = 0;
	uint8_t i;
	static uint8_t StaticCount=0;
	uint8_t Test;

//	Test = LedData[0]^LedData_Prev[0];
//	Test |= LedData[1]^LedData_Prev[1];
//	Test |= LedData[2]^LedData_Prev[2];
//	Test |= LedData[3]^LedData_Prev[3];

	Test =  RsgOpData_Prev[0] ^ RsgOpData[0];		
	Test |= RsgOpData_Prev[1] ^ RsgOpData[1];
	Test |= RsgOpData_Prev[2] ^ RsgOpData[2];
	Test |= RsgOpData_Prev[3] ^ RsgOpData[3];	

//	Test |= RsgOpFtData_Prev[0] ^ RsgOpFtData[0];		
//	Test |= RsgOpFtData_Prev[1] ^ RsgOpFtData[1];

//	Test |= RsgOpDoData_Prev[0] ^ RsgOpDoData[0];		
//	Test |= RsgOpDoData_Prev[1] ^ RsgOpDoData[1];
//	Test |= RsgOpDoData_Prev[2] ^ RsgOpDoData[2];
//	Test |= RsgOpDoData_Prev[3] ^ RsgOpDoData[3];

	Test |= RsgIpData_Prev[0] ^ RsgIpData[0];		
	Test |= RsgIpData_Prev[1] ^ RsgIpData[1];
	Test |= RsgIpData_Prev[2] ^ RsgIpData[2];
	Test |= RsgIpData_Prev[3] ^ RsgIpData[3];

	Test |= RsgCfgData_Prev[0] ^ RsgCfgData[0];		
	Test |= RsgCfgData_Prev[1] ^ RsgCfgData[1];
	Test |= RsgCfgData_Prev[2] ^ RsgCfgData[2];
	Test |= RsgCfgData_Prev[3] ^ RsgCfgData[3];
	

	if	((Test!=0)||(StaticCount > 9))
	{
	
		RsgOpData_Prev[0] = RsgOpData[0];		
		RsgOpData_Prev[1] = RsgOpData[1];
		RsgOpData_Prev[2] = RsgOpData[2];
		RsgOpData_Prev[3] = RsgOpData[3];	

		RsgIpData_Prev[0] = RsgIpData[0];		
		RsgIpData_Prev[1] = RsgIpData[1];
		RsgIpData_Prev[2] = RsgIpData[2];
		RsgIpData_Prev[3] = RsgIpData[3];

		RsgCfgData_Prev[0] = RsgCfgData[0];		
		RsgCfgData_Prev[1] = RsgCfgData[1];
		RsgCfgData_Prev[2] = RsgCfgData[2];
		RsgCfgData_Prev[3] = RsgCfgData[3];
		
		ResetQ(&Q_Local);	

		SendLocalByte (StartPacket);	//0x02
		SendLocalByte (RsgNumBytes);		//6
		SendLocalByteLRC (RsgID, &lrc);	//R
//		SendLocal4BytesLRC (StreamIndex, &lrc);
		SendLocalByteLRC (RsgOpData[0], &lrc);	//C
//		SendLocalByteLRC (RsgOpFtData[0], &lrc);	//C
//		SendLocalByteLRC (RsgOpDoData[0], &lrc);
		SendLocalByteLRC (RsgOpData[1], &lrc);	//C
//		SendLocalByteLRC (RsgOpFtData[1], &lrc);	//C
//		SendLocalByteLRC (RsgOpDoData[1], &lrc);
		SendLocalByteLRC (RsgOpData[2], &lrc);	//C
//		SendLocalByteLRC (RsgOpDoData[2], &lrc);
		SendLocalByteLRC (RsgOpData[3], &lrc);	//C
//		SendLocalByteLRC (RsgOpDoData[3], &lrc);
		SendLocalByteLRC (RsgIpData[0], &lrc);	//C
		SendLocalByteLRC (RsgIpData[1], &lrc);	//C
		SendLocalByteLRC (RsgIpData[2], &lrc);	//C
		SendLocalByteLRC (RsgIpData[3], &lrc);	//C
		SendLocalByteLRC (RsgCfgData[0], &lrc);	//C
//		SendLocalByteLRC (RsgCfgData[1], &lrc);	//C
//		SendLocalByteLRC (RsgCfgData[2], &lrc);	//C
//		SendLocalByteLRC (RsgCfgData[3], &lrc);	//C
		SendLocalByte (lrc);
		SendLocalByte (EndOfPacket);
	
		transmitQ_Local();

		ToggleLED_232();

		StaticCount=0;
	}
	else StaticCount++;

}


void SendDebugMsg(uint8_t msg)
{
	uint8_t lrc = 0;
	
	ResetQ(&Q_Local);

	SendLocalByte('D');
	SendLocalByte('B');
	SendLocalByte('0'+ msg);
	SendLocalByte(13);

	transmitQ_Local();

}

void SendCAN1Data(void)
{
	uint8_t DATA[8];	
	CANRxMessageBuffer * message;
	uint32_t SID;
	uint16_t uID;
	uint8_t bytes=8;

	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	
	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];
	SID = message->msgSID.SID;
	uID = (uint16_t) SID;

	ResetQ(&Q_Local);

	SendLocalChar('1');
	SendLocalChar(' ');
	SendLocalHexByte((SID>>8)&0xFF);
	SendLocalHexByte((SID)&0xFF);
	SendLocalChar(' ');
	SendLocalHexByte(DATA[0]);
	SendLocalHexByte(DATA[1]);
	SendLocalHexByte(DATA[2]);
	SendLocalHexByte(DATA[3]);
	SendLocalHexByte(DATA[4]);
	SendLocalHexByte(DATA[5]);
	SendLocalHexByte(DATA[6]);
	SendLocalHexByte(DATA[7]);
	SendLocalChar('\r');

	transmitQ_Local();

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);
	//Transmit received message from CAN1 on the CAN2 channel
//	CAN2TxMessage(&bytes,&uID, DATA);

}
