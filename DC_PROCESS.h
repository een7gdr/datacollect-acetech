#ifndef _DC_PROCESS_H
#define _DC_PROCESS_H

#include "SYS.h"

#define MODULE_TYPE 0x50	// hex number
#define SW_VERSION_H 2		// decimal number
#define SW_VERSION_L 0		// decimal number

//RSG Definitions
#define pktIdLocation 6
#define pktRevLengthLocation 7

#define pktHP1_8 0      //"HP Outputs 1~8"
#define pktHP9_16 1     //"HP Outputs 9~16"
#define pktLP1_8 2      //"LP Outputs 1~8"
#define pktLP9_16 3     //"LP Outputs 9~16"
#define pktPosIn 4      //"Positive Inputs"
#define pktNegIn 5      //"Negative Inputs"
#define pktCanHS1 6     //"CAN Handset 1"
#define pktCanHS2 7     //"CAN Handset 2"
#define pktCanHS3 8     //"CAN Handset 3"
#define pktCanHS4 9     //"CAN Handset 4"
#define pktCombo1_4 10  //"Combinations 1~4"
#define pktCombo5_8 11  //"Combinations 5~8"
#define pktCanData1 12  //"CAN Data 1"
#define pktCanData2 13  //"CAN Data 2"
#define pktCanData3 14  //"CAN Data 3"
#define pktCanData4 15  //"CAN Data 4"
#define pktCanRPM 16   //"CAN Data RPM"
#define pktCanSpeed 17  //"CAN Data Speed"
#define pktPriBatt 18   //"Primary Battery Voltage"
#define pktSecBatt 19   //"Secondary Battery Voltage"
#define pktCfg1 20      //"Configurable Flags 1"
#define pktCfg2 21      //"Configurable Flags 2"
#define pktCfg3 22      //"Configurable Flags 3"
#define pktCfg4 23      //"Configurable Flags 4"
#define pktSys1 24      //"System Flags 1"
#define pktSys2 25      //"System Flags 2"

#define PMCREPEAT 9		//(number of seconds + 1) before resending static PMC data


struct q
{
	uint8_t inptr;
	uint8_t outptr;
	uint16_t buffer [128];
};

struct CanDataStorage{
	uint16_t uID;
	uint8_t bData[8];
	uint8_t bNewData;
};

uint8_t ReadHexNibble(uint8_t *bptr);
uint16_t ReadHexByte (uint8_t *bptr);
void ProcessLocalMessage(uint8_t Buffer[], uint16_t BufferLen);
uint16_t ReadQ (struct q *qptr);
uint8_t TestQ (struct q *qptr);
uint8_t WriteQ (struct q *qptr, uint16_t value);
uint8_t ConvHex (uint8_t val);
void ResetQ (struct q *qptr);
void SendLocalChar (uint8_t c);
void SendLocalCharLRC (uint8_t c, int8_t *lrc);
void SendLocalHexByte (uint8_t b);
void SendLocal4BytesLRC (uint32_t b4, int8_t *lrc);
void SendLocalHexByteLRC (uint8_t b, int8_t *lrc);
void SendLocalByteArrayLRC (uint8_t *byte, uint8_t numbytes, int8_t *lrc);
void SendLocalHexNibbleLRC (uint8_t n, int8_t *lrc);
void RequestCanDataAndRespond(uint16_t *uID);
void SetCanBaudAndRespond(uint32_t *Baud,uint8_t *SJW,uint8_t *Prop,uint8_t *Phase1,uint8_t *Phase2);
void SendXtdCanDataAndRespond(uint8_t *Bytes, uint32_t *uIDx, uint8_t *uCANData);
void SendStdCanDataAndRespond(uint8_t *Bytes, uint16_t *uID, uint8_t *uCANData);
void SendDebugMsg(uint8_t msg);
uint8_t SendBlsChar (uint16_t NextVal, char * lrc);
void SendGenisysStatus(void);
void SendGPIOMsg(void);
void ProcessRsgMessage(uint8_t Buffer[], uint16_t BufferLen);
void SendRsgLiveStatus(void);
void SendRsgInfoStatus(void);
void SendAceTechStatus(void);

#endif