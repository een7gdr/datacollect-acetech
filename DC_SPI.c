#include <p32xxxx.h>
#include <plib.h>
#include "DC_SPI.h"


void InitSPI(void)
{
	unsigned int config1, config2;
	char temp=0;

	config1 = SPI_MODE8_ON|SPI_CKE_ON|MASTER_ENABLE_ON|CLK_POL_ACTIVE_LOW|SEC_PRESCAL_8_1|PRI_PRESCAL_4_1;
	config2 = SPI_ENABLE;

	PORTBbits.RB4 = 0; //Reset the MCP2515 chip on power up

//	OpenSPI2(config1, config2);
	SpiChnOpen(2, SPI_CON_MSTEN|SPI_CON_MODE8|SPI_CON_CKE|SPI_CON_ON, 32);

	CAN_RESET = 1; //Take the chip out of reset
	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
	CAN_CS = 1; //!CS DESELECT CHIP
	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");

	SPI_CAN_Sleep();

}

/***********************************************
* Function:		SPI_CAN_Get_Register
* Arguments:	char CAN_Register
*				Address of register to read
* Return:		Contents of register
***********************************************/

char SPI_CAN_Get_Reg(char CAN_Register)
{
		
	char rxData=0;
	char rxData1 = 0;

	CAN_CS = 0;
	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");

	SpiChnPutC(2, 0b00000011);
	rxData = SpiChnGetC(2);	
	SpiChnPutC(2, CAN_Register);
	rxData = SpiChnGetC(2);	
	SpiChnPutC(2, 0b00000000);
	rxData1 = SpiChnGetC(2);
	
	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
	CAN_CS = 1;

	return (rxData1);

}

void SPI_CAN_Sleep(void)
{
	char rxData=0;
	char rxData1 = 0;

	CAN_CS = 0;

	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");	

	SpiChnPutC(2, 0b00000010); //Register Write command
	rxData = SpiChnGetC(2);
	SpiChnPutC(2, 0b00001111); //CANCTRL Register Address
	rxData = SpiChnGetC(2);
	SpiChnPutC(2, 0b00100000); //CANCTRL Contents
	rxData = SpiChnGetC(2);


	CAN_CS = 1;

	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
	asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");

	do
	{
		CAN_CS = 0;
		asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
		SpiChnPutC(2, 0b00000011);
		rxData = SpiChnGetC(2);	
		SpiChnPutC(2, 0b00001111);
		rxData = SpiChnGetC(2);	
		SpiChnPutC(2, 0b00000000);
		rxData1 = SpiChnGetC(2);
		asm("NOP"); asm("NOP"); asm("NOP"); asm("NOP");
		CAN_CS = 1;
		
		rxData = SPI_CAN_Get_Reg(CANSTAT_2515);
	}while ((rxData&0b11100000)!= 0x20);


	asm("NOP");
	asm("NOP");

}