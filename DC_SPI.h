#ifndef _DC_SPI_H
#define _DC_SPI_H

#define CAN_RESET PORTBbits.RB4
#define CAN_CS  PORTBbits.RB5
//MCP2515 Register Addresses
#define CANCTRL_2515 0b00001111
#define CANSTAT_2515 0b00001110
#define CNF1_2515 		0b00101010
#define CNF2_2515 		0b00101001
#define CNF3_2515 		0b00101000
#define CANINTE_2515 	0b00101011
#define CANINTF_2515	0b00101100


void InitSPI(void);
void SPI_CAN_Sleep(void);
char SPI_CAN_Get_Reg(char CAN_Register);
#endif