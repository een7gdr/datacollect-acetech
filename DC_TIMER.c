#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_TIMER.h"


/*********************************************
*	This file contains all the code required
*	to initialise and interact with the Timer 
*	modules on the board
*
*********************************************/

void InitTimers(void)
{
    // configure the core timer roll-over rate (25msec)
    OpenCoreTimer(CORE_TICK_RATE);

    // set up the core timer interrupt with a prioirty of 2 and zero sub-priority
    mConfigIntCoreTimer((CT_INT_ON | CT_INT_PRIOR_7 | CT_INT_SUB_PRIOR_0));

}

void EnableCoreTimerInt(void)
{
	mEnableIntCoreTimer();
}

void DisableCoreTimerInt(void)
{
	mDisableIntCoreTimer();
}