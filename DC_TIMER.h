#ifndef _DC_TIMER_H
#define _DC_TIMER_H


// #define SYS_FREQ 				(40000000) //80Mhz
#define SYS_FREQ 				(20000000) //40Mhz
//#define SYS_FREQ 				(16000000) //32Mhz
#define TOGGLES_PER_SEC			40
#define CORE_TICK_RATE	        (SYS_FREQ/2/TOGGLES_PER_SEC)


#endif