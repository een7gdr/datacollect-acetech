#include <p32xxxx.h>
#include "DC_UART.h"


void InitComms()
{
	//Setup UART1 for 112500 comms, 8 bit./

	//U1MODE 
	//1 Stop bit, 8 bit no parity, no flow control, Wake, BRGH = 1
	//Bits 15:0 - 1000 1000 1000 1000
	U1MODE = 0x00008888;
	//U1STA Enable both receiver and transmitter
	//Bits 31:16	xxxx xxx0 0000 0000
	//Bits 15:0 	0001 0100 0000 0000
	U1STA = 0x00001400;
	//U1BRG
	//Bits 31:16 ignored
	//Bits 15:0 ??
	//U1BRG = 0x56; //86 tO GIVE ~115200BPS @ 40Mhz PBCLK
	U1BRG = 0x2A; //42 tO GIVE ~115200BPS @ 20Mhz PBCLK
	//U1BRG = 0x22; //34 tO GIVE ~115200BPS @ 16Mhz PBCLK
	
	//UART4 115200, 8N1
	//1 Stop bit, 8 bit no parity, no flow control, Wake, BRGH = 1
	//Bits 15:0 - 1000 1000 1000 1000
	U4MODE = 0x00008888;
	U4STA = 0x00001400; //As UART1 above
	U4BRG = 0x2A; //As UART1 above
	
	//UART 4 Interrupt flags
	IFS2bits.U4TXIF = 0;
	IFS2bits.U4RXIF = 0;
	IFS2bits.U4EIF = 0;
	//UART 4 Interrupt control
	IEC2bits.U4TXIE = 0;
	IEC2bits.U4RXIE = 1;
	IEC2bits.U4EIE = 0;

	//IPC12 - Interrupt Priority Control Reg for Uart4
	IPC12bits.U4IP = 0b101; 	//Set serial interrupt to priority 5 same as RS485 as both are not expected to be used together
	IPC12bits.U4IS =0b01; 	//Subpriority 1

	//IFS0 - Interrupt Flags for UART 1
	IFS0bits.U1TXIF = 0;
	IFS0bits.U1RXIF = 0;
	IFS0bits.U1EIF = 0;
	
	//IEC0 - Interrupt Enable Control Register for UART 1
	IEC0bits.U1TXIE = 0;
	IEC0bits.U1RXIE = 1;
	IEC0bits.U1EIE = 0;

	//IPC6 - Interruprt Priority Control Reg. for UART 1
	IPC6bits.U1IP = 0b100; 	//Set serial interrupt to priority 4
	IPC6bits.U1IS =0b00; 	//Subpriority 0
}

void InitGenisys()
{
	//Setup UART2 for 112500 comms, 9 bit./

	//U1MODE 
	//1 Stop bit, 9 bit no parity, no flow control, Wake, BRGH = 1
	//Bits 15:0 - 1000 1000 0000 1110
	U2MODE = 0x0000888E;
	//U1STA Enable both receiver and transmitter
	//Bits 31:16	xxxx xxx0 0000 0000
	//Bits 15:0 	0001 0100 0000 0000
	U2STA = 0x00001400;
	//U1BRG
	//Bits 31:16 ignored
	//Bits 15:0 ??
	//U1BRG = 0x56; //86 tO GIVE ~115200BPS @ 40Mhz PBCLK
	U2BRG = 0x2A; //42 tO GIVE ~115200BPS @ 20Mhz PBCLK
	//U1BRG = 0x22; //34 tO GIVE ~115200BPS @ 16Mhz PBCLK
	//IFS1 - Interrupt Flags
	IFS1bits.U2TXIF = 0;
	IFS1bits.U2RXIF = 0;
	IFS1bits.U2EIF = 0;
	//IEC1 - Interrupt Enable Control Register
	IEC1bits.U2TXIE = 0;
	IEC1bits.U2RXIE = 1;
	IEC1bits.U2EIE = 0;

	//IPC6 - Interruprt Priority Control Reg.
	IPC8bits.U2IP = 0b101; 	//Set serial interrupt to priority 5
	IPC8bits.U2IS = 0b00; 	//Subpriority 0

	_RS485RXE = 0; //Enable receiver
	RS485TXE = 0; //Disable transmitter
}

void InitStd485()
{
	//Setup UART2 for 112500 comms, 9 bit./

	//U1MODE 
	//1 Stop bit, 9 bit no parity, no flow control, Wake, BRGH = 1
	//Bits 15:0 - 1000 1000 0000 1000
	U2MODE = 0x00008888;
	//U1STA Enable both receiver and transmitter
	//Bits 31:16	xxxx xxx0 0000 0000
	//Bits 15:0 	0001 0100 0000 0000
	U2STA = 0x00001400;
	//U1BRG
	//Bits 31:16 ignored
	//Bits 15:0 ??
	//U1BRG = 0x56; //86 tO GIVE ~115200BPS @ 40Mhz PBCLK
	U2BRG = 0x2A; //42 tO GIVE ~115200BPS @ 20Mhz PBCLK
	//U1BRG = 0x22; //34 tO GIVE ~115200BPS @ 16Mhz PBCLK
	//IFS1 - Interrupt Flags
	IFS1bits.U2TXIF = 0;
	IFS1bits.U2RXIF = 0;
	IFS1bits.U2EIF = 0;
	//IEC1 - Interrupt Enable Control Register
	IEC1bits.U2TXIE = 0;
	IEC1bits.U2RXIE = 1;
	IEC1bits.U2EIE = 0;

	//IPC6 - Interruprt Priority Control Reg.
	IPC8bits.U2IP = 0b101; 	//Set serial interrupt to priority 5
	IPC8bits.U2IS = 0b00; 	//Subpriority 0

	_RS485RXE = 0; //Enable receiver
	RS485TXE = 0; //Disable transmitter
}