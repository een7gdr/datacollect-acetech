#include <p32xxxx.h>
#include <plib.h>
#include <peripheral/int.h>
#include "DC_CONTROLS.h"
#include "DC_LED.h"
#include "DC_LIB_SKODA.h"
//#include "DC_CAN_LIBRARIES.h"

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80
#define BIT8 0x0100
#define BIT9 0x0200
#define BIT10 0x0400
#define BIT11 0x0800
#define BIT12 0x1000
#define BIT13 0x2000
#define BIT14 0x4000
#define BIT15 0x8000

extern uint8_t CANActivity;
extern uint8_t uBaudSelector;
extern uint8_t SecondCANActive;


extern enum Module_State
{
	STARTUP,
	NOT_LOCKED,
	BAUD_LOCKED,	
	LOCKED,
	SLEEP,
	ERROR
}State, Previous_State;

extern uint32_t Odometer;	//Bytes3-5
/*
KMs
*/
extern uint8_t RPM;				//Byte6
/*
Data = RPM / 50
*/
extern uint8_t RoadSpeed;		//Byte7
/*
KpH
*/
extern uint16_t FuelLevel; 	//Bytes8-9
/*
Data = Litres x 10 
*/
extern uint16_t FuelFlowCount;//Bytes10-11
extern uint8_t	Ignition;		//Byte12
/*
BIT0 - Key In
BIT1 - Accessory Position
BIT2 - Full Ignition
BIT3 - Cranking
BIT4-7 Unused
*/
extern uint8_t Accelerator;		//Byte13
/*
Percentage of full travel
*/
extern uint8_t Brake;			//Byte14
/*
Percetnrage of full travel
*/
extern uint16_t Lights;		//Byte15-16
/*
BIT0 - Sidelights
BIT1 - Running Lights
BIT2 - Dipped Beam
BIT3 - MAin Beam
BIT4 - Front Fogs
BIT5 - Rear Fogs
BIT6 - Brake Lights
BIT7 - Reverse Lights
BIT8 - Left Indicator
BIT9 - Right Indicator
BIT10 - Hazard Lighs
*/
extern uint8_t Wipers;			//Byte17
/*
BIT0-2 - 0(Off), 1(Int1), 2(Int2), 3(Speed1), 4(Speed2), 5(Auto)
BIT3 - Manual
BIT4 - Rear Wipers
BIT5 - Front Wash
BIT6 - Rear Wash
*/
extern uint8_t CoolantTemp;		//Byte18
/*
Data = Temp(C) + 40
*/
extern uint8_t AmbientTemp;
/*
Data = Temp(C) + 40
*/
extern uint16_t SteeringWheel;
/*
BIT0-14 - Degrees from Centre
BIT15 - Direction 1-Right
*/
extern uint8_t Controls;	
/*
BIT0 - Hanbrake
BIT1 - Clutch
BIT2 - Horn
BIT3 - ECO Mode
BIT4 - Auto Start/Stop Mode
*/
extern uint8_t AutoBox;			//Byte23
/*
BIT0 - Park
BIT1 - Reverse
BIT2 - Neutral
BIT3 - Drive
BIT4 - Manual
BIT5 - Gear Up
BIT6 - Gear Down
BIT7 - 
*/
extern uint8_t ManualBox;		//Byte24
/*
BIT0-2 - Gear (0 Neutral - 6th gear 7 = reverse 
BIT3-5 - Gear Advised 
BIT6 - Shift Up Advice
BIT7 - Shift Down Advice
*/
extern uint8_t Doors;			//Byte25
/*
BIT0 - OSF Door Open
BIT1 - NSF Door Open
BIT2 - OSR Door Open
BIT3 - NSR Door Open
BIT4 - Tailgate/Boot/Rear Door
BIT5 - Bonnet Open
BIT6 - 
BIT7 - 
*/
extern uint8_t SeatBelts;		//Byte26
/*
BIT0 - OSF Belt Used
BIT1 - NSF Belt Used
BIT2 - OSR Belt USed
BIT3 - NSR Belt Used
BIT4 - Raar Middle Used
BIT5 - 
BIT6 - 
BIT7 - 
*/
extern uint8_t Occupancy;		//Byte27
/*
BIT0 - NSF Seat Occupied
BIT1 - OSR Seat Occupied
BIT2 - NSR Seat Occupied
BIT3 - Rear Middle Occupied
*/
extern uint8_t MILLights;		//Byte28
/*
BIT0 - Engine MIL
BIT1 - Oil Level
BIT2 - Brake System
BIT3 - Oil Temp
BIT4 - Low Fuel
BIT5 - Coolant Temp
BIT6 - Oil Warning
BIT7 - Emissions MIL
*/

extern uint8_t VehicleType;		

extern uint8_t VIN[17];			//Bytes6-22
//Checksum byte 23
//End Packet byte 24

extern uint32_t FILTER0_VOL;
extern uint32_t FILTER1_VOL;
extern uint32_t FILTER2_VOL;
extern uint32_t FILTER3_VOL;
extern uint32_t FILTER4_VOL;
extern uint32_t FILTER5_VOL;
extern uint32_t FILTER6_VOL;
extern uint32_t FILTER7_VOL;
extern uint32_t FILTER8_VOL;
extern uint32_t FILTER9_VOL;
extern uint32_t FILTER10_VOL;
extern uint32_t FILTER11_VOL;
extern uint32_t FILTER12_VOL;
extern uint32_t FILTER13_VOL;
extern uint32_t FILTER14_VOL;
extern uint32_t FILTER15_VOL;

extern BOOL	FILTER0_EN;
extern BOOL	FILTER1_EN;
extern BOOL	FILTER2_EN;
extern BOOL	FILTER3_EN;
extern BOOL	FILTER4_EN;
extern BOOL	FILTER5_EN;
extern BOOL	FILTER6_EN;
extern BOOL	FILTER7_EN;
extern BOOL	FILTER8_EN;
extern BOOL	FILTER9_EN;
extern BOOL	FILTER10_EN;
extern BOOL	FILTER11_EN;
extern BOOL	FILTER12_EN;
extern BOOL	FILTER13_EN;
extern BOOL	FILTER14_EN;
extern BOOL	FILTER15_EN;

extern uint32_t FILTER2_0_VOL;
extern uint32_t FILTER2_1_VOL;
extern uint32_t FILTER2_2_VOL;
extern uint32_t FILTER2_3_VOL;
extern uint32_t FILTER2_4_VOL;
extern uint32_t FILTER2_5_VOL;
extern uint32_t FILTER2_6_VOL;
extern uint32_t FILTER2_7_VOL;
extern uint32_t FILTER2_8_VOL;
extern uint32_t FILTER2_9_VOL;
extern uint32_t FILTER2_10_VOL;
extern uint32_t FILTER2_11_VOL;
extern uint32_t FILTER2_12_VOL;
extern uint32_t FILTER2_13_VOL;
extern uint32_t FILTER2_14_VOL;
extern uint32_t FILTER2_15_VOL;

extern BOOL	FILTER2_0_EN;
extern BOOL	FILTER2_1_EN;
extern BOOL	FILTER2_2_EN;
extern BOOL	FILTER2_3_EN;
extern BOOL	FILTER2_4_EN;
extern BOOL	FILTER2_5_EN;
extern BOOL	FILTER2_6_EN;
extern BOOL	FILTER2_7_EN;
extern BOOL	FILTER2_8_EN;
extern BOOL	FILTER2_9_EN;
extern BOOL	FILTER2_10_EN;
extern BOOL	FILTER2_11_EN;
extern BOOL	FILTER2_12_EN;
extern BOOL	FILTER2_13_EN;
extern BOOL	FILTER2_14_EN;
extern BOOL	FILTER2_15_EN;

extern uint8_t Hold_Counter;

extern CANRxMessageBuffer * message;
uint32_t SID;

void Check_PorscheMacanType(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == VAG_CAR_4_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == VAG_CAR_4_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == VAG_CAR_4_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == VAG_CAR_4_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == VAG_CAR_4_CAN1_FILTER4)
		Check[4] = 1;
	if (SID == VAG_CAR_4_CAN1_FILTER5)
		Check[5] = 1;
	if (SID == VAG_CAR_4_CAN1_FILTER6)
		Check[6] = 1;
	if (SID == VAG_CAR_4_CAN1_FILTER7)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = VAG_CAR_4_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = VAG_CAR_4_CAN1_FILTER0;
		FILTER1_VOL = VAG_CAR_4_CAN1_FILTER1;
		FILTER2_VOL = VAG_CAR_4_CAN1_FILTER2;
		FILTER3_VOL = VAG_CAR_4_CAN1_FILTER3;
		FILTER4_VOL = VAG_CAR_4_CAN1_FILTER4;
		FILTER5_VOL = VAG_CAR_4_CAN1_FILTER5;
		FILTER6_VOL = VAG_CAR_4_CAN1_FILTER6;
		FILTER7_VOL = VAG_CAR_4_CAN1_FILTER7;
		FILTER8_VOL = VAG_CAR_4_CAN1_FILTER8;
		FILTER9_VOL = VAG_CAR_4_CAN1_FILTER9;
		FILTER10_VOL = VAG_CAR_4_CAN1_FILTER10;
		FILTER11_VOL = VAG_CAR_4_CAN1_FILTER11;
		FILTER12_VOL = VAG_CAR_4_CAN1_FILTER12;
		FILTER13_VOL = VAG_CAR_4_CAN1_FILTER13;
		FILTER14_VOL = VAG_CAR_4_CAN1_FILTER14;
		FILTER15_VOL = VAG_CAR_4_CAN1_FILTER15;

		FILTER0_EN = VAG_CAR_4_CAN1_FILTER0_EN;
		FILTER1_EN = VAG_CAR_4_CAN1_FILTER1_EN;
		FILTER2_EN = VAG_CAR_4_CAN1_FILTER2_EN;
		FILTER3_EN = VAG_CAR_4_CAN1_FILTER3_EN;
		FILTER4_EN = VAG_CAR_4_CAN1_FILTER4_EN;
		FILTER5_EN = VAG_CAR_4_CAN1_FILTER5_EN;
		FILTER6_EN = VAG_CAR_4_CAN1_FILTER6_EN;
		FILTER7_EN = VAG_CAR_4_CAN1_FILTER7_EN;
		FILTER8_EN = VAG_CAR_4_CAN1_FILTER8_EN;
		FILTER9_EN = VAG_CAR_4_CAN1_FILTER9_EN;
		FILTER10_EN = VAG_CAR_4_CAN1_FILTER10_EN;
		FILTER11_EN = VAG_CAR_4_CAN1_FILTER11_EN;
		FILTER12_EN = VAG_CAR_4_CAN1_FILTER12_EN;
		FILTER13_EN = VAG_CAR_4_CAN1_FILTER13_EN;
		FILTER14_EN = VAG_CAR_4_CAN1_FILTER14_EN;
		FILTER15_EN = VAG_CAR_4_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0xFF;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0xFF;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}


void Check_TransporterT5Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == VAG_CAR_2_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == VAG_CAR_2_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == VAG_CAR_2_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == VAG_CAR_2_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == VAG_CAR_2_CAN1_FILTER4)
		Check[4] = 1;
	if (SID == VAG_CAR_2_CAN1_FILTER5)
		Check[5] = 1;
	if (SID == VAG_CAR_2_CAN1_FILTER6)
		Check[6] = 1;
	if (SID == VAG_CAR_2_CAN1_FILTER7)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = VAG_CAR_2_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = VAG_CAR_2_CAN1_FILTER0;
		FILTER1_VOL = VAG_CAR_2_CAN1_FILTER1;
		FILTER2_VOL = VAG_CAR_2_CAN1_FILTER2;
		FILTER3_VOL = VAG_CAR_2_CAN1_FILTER3;
		FILTER4_VOL = VAG_CAR_2_CAN1_FILTER4;
		FILTER5_VOL = VAG_CAR_2_CAN1_FILTER5;
		FILTER6_VOL = VAG_CAR_2_CAN1_FILTER6;
		FILTER7_VOL = VAG_CAR_2_CAN1_FILTER7;
		FILTER8_VOL = VAG_CAR_2_CAN1_FILTER8;
		FILTER9_VOL = VAG_CAR_2_CAN1_FILTER9;
		FILTER10_VOL = VAG_CAR_2_CAN1_FILTER10;
		FILTER11_VOL = VAG_CAR_2_CAN1_FILTER11;
		FILTER12_VOL = VAG_CAR_2_CAN1_FILTER12;
		FILTER13_VOL = VAG_CAR_2_CAN1_FILTER13;
		FILTER14_VOL = VAG_CAR_2_CAN1_FILTER14;
		FILTER15_VOL = VAG_CAR_2_CAN1_FILTER15;

		FILTER0_EN = VAG_CAR_2_CAN1_FILTER0_EN;
		FILTER1_EN = VAG_CAR_2_CAN1_FILTER1_EN;
		FILTER2_EN = VAG_CAR_2_CAN1_FILTER2_EN;
		FILTER3_EN = VAG_CAR_2_CAN1_FILTER3_EN;
		FILTER4_EN = VAG_CAR_2_CAN1_FILTER4_EN;
		FILTER5_EN = VAG_CAR_2_CAN1_FILTER5_EN;
		FILTER6_EN = VAG_CAR_2_CAN1_FILTER6_EN;
		FILTER7_EN = VAG_CAR_2_CAN1_FILTER7_EN;
		FILTER8_EN = VAG_CAR_2_CAN1_FILTER8_EN;
		FILTER9_EN = VAG_CAR_2_CAN1_FILTER9_EN;
		FILTER10_EN = VAG_CAR_2_CAN1_FILTER10_EN;
		FILTER11_EN = VAG_CAR_2_CAN1_FILTER11_EN;
		FILTER12_EN = VAG_CAR_2_CAN1_FILTER12_EN;
		FILTER13_EN = VAG_CAR_2_CAN1_FILTER13_EN;
		FILTER14_EN = VAG_CAR_2_CAN1_FILTER14_EN;
		FILTER15_EN = VAG_CAR_2_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0xFF;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_TransporterT6Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == VAG_CAR_3_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == VAG_CAR_3_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == VAG_CAR_3_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == VAG_CAR_3_CAN1_FILTER3)
		Check[3] = 1;
	if (SID == VAG_CAR_3_CAN1_FILTER4)
		Check[4] = 1;
	if (SID == VAG_CAR_3_CAN1_FILTER5)
		Check[5] = 1;
	if (SID == VAG_CAR_3_CAN1_FILTER6)
		Check[6] = 1;
	if (SID == VAG_CAR_3_CAN1_FILTER7)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = VAG_CAR_3_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = VAG_CAR_3_CAN1_FILTER0;
		FILTER1_VOL = VAG_CAR_3_CAN1_FILTER1;
		FILTER2_VOL = VAG_CAR_3_CAN1_FILTER2;
		FILTER3_VOL = VAG_CAR_3_CAN1_FILTER3;
		FILTER4_VOL = VAG_CAR_3_CAN1_FILTER4;
		FILTER5_VOL = VAG_CAR_3_CAN1_FILTER5;
		FILTER6_VOL = VAG_CAR_3_CAN1_FILTER6;
		FILTER7_VOL = VAG_CAR_3_CAN1_FILTER7;
		FILTER8_VOL = VAG_CAR_3_CAN1_FILTER8;
		FILTER9_VOL = VAG_CAR_3_CAN1_FILTER9;
		FILTER10_VOL = VAG_CAR_3_CAN1_FILTER10;
		FILTER11_VOL = VAG_CAR_3_CAN1_FILTER11;
		FILTER12_VOL = VAG_CAR_3_CAN1_FILTER12;
		FILTER13_VOL = VAG_CAR_3_CAN1_FILTER13;
		FILTER14_VOL = VAG_CAR_3_CAN1_FILTER14;
		FILTER15_VOL = VAG_CAR_3_CAN1_FILTER15;

		FILTER0_EN = VAG_CAR_3_CAN1_FILTER0_EN;
		FILTER1_EN = VAG_CAR_3_CAN1_FILTER1_EN;
		FILTER2_EN = VAG_CAR_3_CAN1_FILTER2_EN;
		FILTER3_EN = VAG_CAR_3_CAN1_FILTER3_EN;
		FILTER4_EN = VAG_CAR_3_CAN1_FILTER4_EN;
		FILTER5_EN = VAG_CAR_3_CAN1_FILTER5_EN;
		FILTER6_EN = VAG_CAR_3_CAN1_FILTER6_EN;
		FILTER7_EN = VAG_CAR_3_CAN1_FILTER7_EN;
		FILTER8_EN = VAG_CAR_3_CAN1_FILTER8_EN;
		FILTER9_EN = VAG_CAR_3_CAN1_FILTER9_EN;
		FILTER10_EN = VAG_CAR_3_CAN1_FILTER10_EN;
		FILTER11_EN = VAG_CAR_3_CAN1_FILTER11_EN;
		FILTER12_EN = VAG_CAR_3_CAN1_FILTER12_EN;
		FILTER13_EN = VAG_CAR_3_CAN1_FILTER13_EN;
		FILTER14_EN = VAG_CAR_3_CAN1_FILTER14_EN;
		FILTER15_EN = VAG_CAR_3_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0xFF;
		ManualBox = 0xFF;
		Doors = 0x00;
		SeatBelts = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}

}

void Check_SkodaScout2014Type(void)
{
	static uint8_t Check[8];
	uint8_t i, Complete;
				
	//Check 8 IDs. If all IDs present then lock vehicle type

	if (SID == SKODA_CAR_1_CAN1_FILTER0)
		Check[0] = 1;
	if (SID == SKODA_CAR_1_CAN1_FILTER1)
		Check[1] = 1;
	if (SID == SKODA_CAR_1_CAN1_FILTER2)
		Check[2] = 1;
	if (SID == SKODA_CAR_1_CAN1_FILTER6)
		Check[3] = 1;
	if (SID == SKODA_CAR_1_CAN1_FILTER8)
		Check[4] = 1;
	if (SID == SKODA_CAR_1_CAN1_FILTER10)
		Check[5] = 1;
	if (SID == SKODA_CAR_1_CAN1_FILTER11)
		Check[6] = 1;
	if (SID == SKODA_CAR_1_CAN1_FILTER12)
		Check[7] = 1;
	
	Complete = 1;

	//Check if all Check values are non zero
	for (i=0; i<8; i++)
		if (Check[i] == 0) Complete = 0;

	if (Complete == 1)
	{
		VehicleType = SKODA_CAR_1_TYPE;
		State = Previous_State = LOCKED;

		FILTER0_VOL = SKODA_CAR_1_CAN1_FILTER0;
		FILTER1_VOL = SKODA_CAR_1_CAN1_FILTER1;
		FILTER2_VOL = SKODA_CAR_1_CAN1_FILTER2;
		FILTER3_VOL = SKODA_CAR_1_CAN1_FILTER3;
		FILTER4_VOL = SKODA_CAR_1_CAN1_FILTER4;
		FILTER5_VOL = SKODA_CAR_1_CAN1_FILTER5;
		FILTER6_VOL = SKODA_CAR_1_CAN1_FILTER6;
		FILTER7_VOL = SKODA_CAR_1_CAN1_FILTER7;
		FILTER8_VOL = SKODA_CAR_1_CAN1_FILTER8;
		FILTER9_VOL = SKODA_CAR_1_CAN1_FILTER9;
		FILTER10_VOL = SKODA_CAR_1_CAN1_FILTER10;
		FILTER11_VOL = SKODA_CAR_1_CAN1_FILTER11;
		FILTER12_VOL = SKODA_CAR_1_CAN1_FILTER12;
		FILTER13_VOL = SKODA_CAR_1_CAN1_FILTER13;
		FILTER14_VOL = SKODA_CAR_1_CAN1_FILTER14;
		FILTER15_VOL = SKODA_CAR_1_CAN1_FILTER15;

		FILTER0_EN = SKODA_CAR_1_CAN1_FILTER0_EN;
		FILTER1_EN = SKODA_CAR_1_CAN1_FILTER1_EN;
		FILTER2_EN = SKODA_CAR_1_CAN1_FILTER2_EN;
		FILTER3_EN = SKODA_CAR_1_CAN1_FILTER3_EN;
		FILTER4_EN = SKODA_CAR_1_CAN1_FILTER4_EN;
		FILTER5_EN = SKODA_CAR_1_CAN1_FILTER5_EN;
		FILTER6_EN = SKODA_CAR_1_CAN1_FILTER6_EN;
		FILTER7_EN = SKODA_CAR_1_CAN1_FILTER7_EN;
		FILTER8_EN = SKODA_CAR_1_CAN1_FILTER8_EN;
		FILTER9_EN = SKODA_CAR_1_CAN1_FILTER9_EN;
		FILTER10_EN = SKODA_CAR_1_CAN1_FILTER10_EN;
		FILTER11_EN = SKODA_CAR_1_CAN1_FILTER11_EN;
		FILTER12_EN = SKODA_CAR_1_CAN1_FILTER12_EN;
		FILTER13_EN = SKODA_CAR_1_CAN1_FILTER13_EN;
		FILTER14_EN = SKODA_CAR_1_CAN1_FILTER14_EN;
		FILTER15_EN = SKODA_CAR_1_CAN1_FILTER15_EN;

		Lights = 0x00;
		Ignition = 0x00;
		Wipers = 0x00;
		AmbientTemp = 0xFF;
		Controls = 0x00;
		AutoBox = 0x00;
		ManualBox = 0x00;
		Doors = 0x00;
		SeatBelts = 0x00;
		Accelerator = 0xFF;
		Occupancy = 0xFF;
		MILLights = 0xFF;
		for (i=0; i<17; i++)
		{	
			VIN[i] = 0xFF;
		}

		InitCAN1Filters();
		SecondCANActive = 0;
	}
}

void DecodeDataPorscheMacanType(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t FuelFlow_Last;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

	//RPM
	//Gears (auto)
	//HB
	if (SID == 0x3C3)
	{
		Tempu16 = message->data[6] & 0xFF;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[5];

		Tempu16 = Tempu16 / 4;

		//Holds RPM data - no scaling yet.
		Tempu16 = (Tempu16 + 25) / 50;

		RPM = Tempu16 & 0xFF;	

		switch(DATA[4] & 0x0F)
		{
			case 5:
				AutoBox = 0x01;
				break;
			case 6:
				AutoBox = 0x02;
				break;
			case 7:
				AutoBox = 0x04;
				break;
			case 8:
				AutoBox = 0x08;
				break;
			case 0xE:
				AutoBox = 0x10;
				break;
			default:
				AutoBox = 0;
		}		
		//Handbrake
		CheckSet8(DATA[3], BIT2, &Controls, BIT0);
		//Footbrake
		CheckSet16(DATA[7], BIT2, &Lights, BIT6);

	}
	//Steering Wheel
	if (SID == 0x525)
	{
		Tempu16 = message->data[1] & 0x3F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[0] & 0xFF;
		
		//Tempu16 = Tempu16 >> 3;
		//If Right
		if (Tempu16 > 0x2000)
		{
			Tempu16 = Tempu16 - 0x2000;
			//TempDouble = ((double) Tempu16 / 10.0f) + 0.5f;
			SteeringWheel = SteeringWheel | BIT15;
			
		}	
		else
		{
			Tempu16 = 0x2000 - Tempu16;
			//TempDouble = ((double) Tempu16 / 10.0f) + 0.5f;
			SteeringWheel = SteeringWheel & ~BIT15;

		}

		TempDouble = ((double) Tempu16 / 10.0f) + 0.5f;

		SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;	
		
	}

	if (SID == 0x535)
	{
		Tempu8 = DATA[4] & 0x7F;

		FuelLevel = Tempu8 * 10;
	}

	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}


void DecodeDataSkodaScout2014Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t FuelFlow_Last;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;

//RPM & Speed (when we get it!)
	if (SID == 0x3DC)
	{
		Tempu16 = message->data[3] & 0x7F;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[2];

		Tempu16 = Tempu16 / 4;

		//Holds RPM data - no scaling yet.
		Tempu16 = (Tempu16 + 25) / 50;

		RPM = Tempu16 & 0xFF;

	}
	//Odometer
	if (SID == 0x6B2)
	{
			Tempu32 = message->data[3] & 0x7;
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[2];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[1];

			//Odometer = Tempu32;
			if (Tempu32 != 0x7FFFF) Odometer = Tempu32; //value 7ffff seems to occur during use and therefore should be filtered out

		
	}
	//Fuel flow
	if (SID == 0x107)
	{
		Tempu8 = message->data[7];

		if ((RPM < 0x6)||(RPM==0xFF)) FuelFlow_Last = Tempu8;
		if ((Ignition & BIT2) != BIT2) FuelFlow_Last = Tempu8;
		//if engine not renning, no fuel used but data might reset

		if (Tempu8 < FuelFlow_Last)
		{
			Tempu8 = Tempu8 + (0xFF - FuelFlow_Last);
		}
		else
		{
			Tempu8 = Tempu8 - FuelFlow_Last;
		}

		if (Tempu8 > 20) Tempu8 = 0;//if change is greater than 20 counts between messages ignore

		if (FuelFlowCount > (0xFFFE - Tempu8))
		{
		//Rollover
			FuelFlowCount +=  Tempu8 + 1;
			//makes sure it rolls over at 0xFFFE not 0xFFFF
		}
		else
		{
		//Normal
			FuelFlowCount += Tempu8;
		}

		FuelFlow_Last = message->data[7];

	}
	//Fuel
	if (SID == 0x6B8)
	{
		Tempu16 = message->data[4] & 0x7F;
		Tempu16 = Tempu16 << 8;
		Tempu16 = Tempu16 | message->data[3];
		
		if ((Ignition & BIT2) == BIT2)
		{
			FuelLevel = Tempu16 / 42;
		}
    //each bit equal to about 2.387 ml, need to get data to 100ml
	}
	//Ignition
	if (SID == 0x3C0)
	{
		if ((message->data[2] & BIT1) == BIT1) Ignition = Ignition | BIT2;
			else Ignition = Ignition &~ BIT2;
		if ((message->data[2] & BIT0) == BIT0) Ignition = Ignition | BIT1;
			else Ignition = Ignition &~ BIT1;
	}
	//Indictors
	if (SID == 0x366)
	{
		if ((message->data[2] & BIT7) == BIT7) Lights = Lights | BIT8;
			else Lights = Lights &~ BIT8;
		if ((message->data[3] & BIT0) == BIT0) Lights = Lights | BIT9;
			else Lights = Lights &~ BIT9;
		CheckSet16(DATA[2], BIT4, &Lights, BIT10); //Hazards

	}
	//Horn
	//Wipers
	if (SID == 0x3D4)
	{
		if ((message->data[3] & BIT5) == BIT5) Controls = Controls | BIT2;
			else Controls = Controls &~ BIT2;
	
		Wipers = 0;
		
		switch (message->data[2] & 0x0F)
		{
			case 1:
				Wipers = Wipers | 0x08;
				break;
			case 2:
				Wipers = Wipers | 0x01;
				break;
			case 4:
				Wipers = Wipers | 0x03;
				break;
			case 8:
				Wipers = Wipers | 0x04;
				break;

		}
		//Rear Wiper
		if ((message->data[2] & BIT5) == BIT5) Wipers = Wipers | BIT4;
			else Wipers = Wipers &~ BIT4;
		//Manual
		if ((message->data[2] & BIT0) == BIT0) Wipers = Wipers | BIT3;
			else Wipers = Wipers &~ BIT3;


	}
	//Accelerator
	//Not present on standard 4x4 octavia
	//This id has been removed from vehicle check
	if (SID == 0x391)
	{
			Tempu8 = message->data[5];
			TempDouble = (double)Tempu8;

			TempDouble = (TempDouble-36) * 0.538;

			Tempu8 = (uint8_t)(TempDouble + 0.5);		

			Accelerator = Tempu8;
	}
	//Steeringwheel & speed
	if (SID == 0x3DA)
	{
		Tempu16 = message->data[6] & 0xFF;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[5] & 0xF8;
		
		Tempu16 = Tempu16 >> 3;
		//If Right
	
		TempDouble = ((double) Tempu16 / 10.0f) + 0.5f;

		SteeringWheel = ((uint16_t)TempDouble) & 0x7FF;
 			
		if ((message->data[5] & BIT2) == BIT2) SteeringWheel = SteeringWheel | BIT15;
			else SteeringWheel = SteeringWheel &~ BIT15;

		Tempu16 = message->data[4] & 0x7F;

		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[3];

		RoadSpeed = Tempu16 / 100;


	}
	//Lights
	if (SID == 0x658)
	{
		if ((message->data[1] & BIT4) == BIT4) Lights = Lights | BIT0;
			else Lights = Lights &~ BIT0;
		if ((message->data[1] & BIT5) == BIT5) Lights = Lights | BIT2;
			else Lights = Lights &~ BIT2;
		if ((message->data[1] & BIT6) == BIT6) Lights = Lights | BIT3;
			else Lights = Lights &~ BIT3;
		if ((message->data[1] & BIT7) == BIT7) Lights = Lights | BIT4;
			else Lights = Lights &~ BIT4;
		if ((message->data[2] & BIT0) == BIT0) Lights = Lights | BIT5;
			else Lights = Lights &~ BIT5;
	}
	//Handbrake
	if (SID == 0x30B)
	{
		if ((message->data[2] & BIT7) == BIT7) Controls = Controls | BIT0;
			else Controls = Controls &~ BIT0;
	}
	//Doors
	if (SID == 0x583)
	{
		if ((message->data[3] & BIT0) == BIT0) Doors = Doors | BIT0;
			else Doors = Doors &~ BIT0;
		if ((message->data[3] & BIT1) == BIT1) Doors = Doors | BIT1;
			else Doors = Doors &~ BIT1;
		if ((message->data[3] & BIT2) == BIT2) Doors = Doors | BIT2;
			else Doors = Doors &~ BIT2;
		if ((message->data[3] & BIT3) == BIT3) Doors = Doors | BIT3;
			else Doors = Doors &~ BIT3;
		if ((message->data[3] & BIT4) == BIT4) Doors = Doors | BIT4;
			else Doors = Doors &~ BIT4;
	}
	//ESC
	if (SID == 0x31B)
	{
		if ((message->data[6] & BIT7) == BIT7) Controls = Controls | BIT5;
			else Controls = Controls &~ BIT5;
	}
	//Brake
	if (SID == 0x3BE)
	{
		CheckSet16(DATA[3], BIT6, &Lights, BIT6);
	}
	//Seatbelts
	if (SID == 0x520)
	{
		if ((message->data[5] & BIT0) == BIT0) SeatBelts = SeatBelts | BIT0;
			else SeatBelts = SeatBelts &~ BIT0;
		if ((message->data[5] & BIT2) == BIT2) SeatBelts = SeatBelts | BIT1;
			else SeatBelts = SeatBelts &~ BIT1;
		if ((message->data[5] & BIT4) == BIT4) SeatBelts = SeatBelts | BIT2;
			else SeatBelts = SeatBelts &~ BIT2;
		if ((message->data[6] & BIT0) == BIT0) SeatBelts = SeatBelts | BIT3;
			else SeatBelts = SeatBelts &~ BIT3;
		if ((message->data[5] & BIT6) == BIT6) SeatBelts = SeatBelts | BIT4;
			else SeatBelts = SeatBelts &~ BIT4;
	}
	//VIN
	if (SID == 0x6B4)
	{
		switch (message->data[0])
		{
			case 0:
				VIN[0] = message->data[5];
				VIN[1] = message->data[6];
				VIN[2] = message->data[7];
				break;
			case 1:
				VIN[3] = message->data[1];
				VIN[4] = message->data[2];
				VIN[5] = message->data[3];
				VIN[6] = message->data[4];
				VIN[7] = message->data[5];
				VIN[8] = message->data[6];
				VIN[9] = message->data[7];
				break;
			case 2:
				VIN[10] = message->data[1];
				VIN[11] = message->data[2];
				VIN[12] = message->data[3];
				VIN[13] = message->data[4];
				VIN[14] = message->data[5];
				VIN[15] = message->data[6];
				VIN[16] = message->data[7];
				break;
		}
	}

	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}


void DecodeDataTransporterT5Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t LeftCtr;
	static uint8_t RightCtr;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//RPM
	if (SID == 0x35B)
	{
		Tempu16 = message->data[2] & 0xFF;
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[1];

		//Holds RPM data - no scaling yet. / 50 and / 4 to get RPM
		Tempu16 = (Tempu16 + 100) / 200;

		if ((Ignition & BIT2) == BIT2)
			RPM = Tempu16 & 0xFF;
		else RPM = 0;


	}

	//Handbrake & Fuel
	else if (SID == 0x621)
	{
		//Handbrake 
		CheckSet8(DATA[0], BIT5, &Controls, BIT0);
	
		//Fuel
		Tempu8 = message->data[3] & 0x7F;
		if ((Ignition & BIT2) == BIT2)
			FuelLevel = Tempu8 * 10;				
	}

	//Road Speed
	else if (SID == 0x527)
	{
		Tempu16 = message->data[2] & 0xFF;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[1];
	
		RoadSpeed = Tempu16 / 200;
	}
	//Odometer
	else if (SID == 0x65D)
	{
			Tempu32 = message->data[3]&0x0F;
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[2];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[1];

			if (Tempu32 > 0)
				Odometer = Tempu32;
	}

	//Coolant
	//else if (SID == 0x355)
	//{
	//	Tempu8 = message->data[5];

		//This gives coolant deg C + 60 (need coolant + 40).
	//	CoolantTemp = Tempu8 - 20;

	//}
	//Ignitio
	else if (SID == 0x575)
	{
		CheckSet8(DATA[0], BIT1, &Ignition, BIT2); //Full ignitino
		CheckSet8(DATA[0], BIT0, &Ignition, BIT1); //Aux
		
		if ((DATA[0]&0x07) == 0x03) Ignition = Ignition | BIT3;
			else Ignition = Ignition & ~BIT3;
	
	}
	//Lights & Doors
	else if (SID == 0x470)
	{
		//Sidelights
		CheckSet16(DATA[5], BIT0, &Lights, BIT0);

		//Dipped
		CheckSet16(DATA[5], BIT3, &Lights, BIT2);

		//Main
		CheckSet16(DATA[7], BIT6, &Lights, BIT3);

		//Front fogs
		CheckSet16(DATA[5], BIT4, &Lights, BIT4);

		//Rear Fogs
		CheckSet16(DATA[7], BIT5, &Lights, BIT5);

		//Reverse
		CheckSet16(DATA[0], BIT5, &Lights, BIT7);
		
		//Left Indicator
		if ((DATA[0] & BIT0) == BIT0)
		{
			Lights = Lights | BIT8;
			LeftCtr = 0;
		}
		else 
		{
			if (LeftCtr>8)
				Lights = Lights &~ BIT8;
			else
				LeftCtr++;
		}
		//Right Indicator
		if ((DATA[0] & BIT1) == BIT1)
		{
			Lights = Lights | BIT9;
			RightCtr = 0;
		}
		else 
		{
			if (RightCtr>8)
				Lights = Lights &~ BIT9;
			else
				RightCtr++;
		}

		CheckSet8(DATA[1], BIT0, &Doors, BIT0); // OSF
		CheckSet8(DATA[1], BIT1, &Doors, BIT1); // NSF
		CheckSet8(DATA[1], BIT3, &Doors, BIT2); //OSR
		CheckSet8(DATA[1], BIT2, &Doors, BIT3); //NSR guess!!
		CheckSet8(DATA[1], BIT5, &Doors, BIT4); //Tailgete
	}

	//Footbrake
	else if (SID == 0x531)
	{
		CheckSet16(DATA[1], BIT6, &Lights, BIT6);
	}

	//Front Wipers
	else if (SID == 0x2C1)
	{ 
		Tempu8 = message->data[1] & 0x0E;

		//Clear last 3 bits
		Wipers = Wipers & 0xF8;
		//Intermittent, low & high
		if (Tempu8 == 0x02)
			Wipers = Wipers | 0x01;
		else if (Tempu8 == 0x04)
			Wipers = Wipers | 0x03;
		else if (Tempu8 == 0x08)
			Wipers = Wipers | 0x04;
		//Manual Wipe
		//CheckSet8(DATA[3], BIT6, &Wipers, BIT3);
		//Rear Wipers
		//Tempu8 = message->data[3] & 0x0C;
		//if (Tempu8 > 0) Wipers = Wipers | BIT4;
		//else Wipers = Wipers &~ BIT4;
		//Front Wash
		CheckSet8(DATA[1], BIT4, &Wipers, BIT5);
		//Rear Wash
		//CheckSet8(DATA[3], BIT7, &Wipers, BIT6);
	}

	//Steering Wheel
	else if (SID == 0x3C3)
	{
		Tempu16 = message->data[1] & 0x7F;
		Tempu16 = Tempu16 <<8;

		Tempu16 = Tempu16 | message->data[0];
		
		SteeringWheel = ((double)Tempu16) / 23.1;
		CheckSet16(DATA[1], BIT7, &SteeringWheel, BIT15);

	}
	//Accel position
	//else if (SID == 0x255)
	//{
	//	Tempu8 = message->data[3];
	//	TempDouble = (double)Tempu8;

	//	TempDouble = TempDouble / 2.0;

	//	Tempu8 = (uint8_t)(TempDouble + 0.5);		

	//	Accelerator = Tempu8;

	//}
	//Brake pedal position
	else if (SID == 0x1D0)
	{
		Tempu16 = message->data[3] & 0x03;
		Tempu16 = Tempu16 << 8;
		Tempu16 = Tempu16 | message->data[4];

		TempDouble = (double)Tempu16;
		//0x286 about max
		TempDouble = TempDouble / 6.4;

		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Brake = Tempu8;

	}


	//VIN
	else if (SID == 0x65F)
	{
		switch (message->data[0])
		{
			case 0x00:
				VIN[0] = message->data[5];
				VIN[1] = message->data[6];
				VIN[2] = message->data[7];	
				break;

			case 0x01:
				VIN[3] = message->data[1];
				VIN[4] = message->data[2];
				VIN[5] = message->data[3];
				VIN[6] = message->data[4];
				VIN[7] = message->data[5];
				VIN[8] = message->data[6];
				VIN[9] = message->data[7];
				break;

			case 0x02:
				VIN[10] = message->data[1];
				VIN[11] = message->data[2];
				VIN[12] = message->data[3];
				VIN[13] = message->data[4];
				VIN[14] = message->data[5];
				VIN[15] = message->data[6];
				VIN[16] = message->data[7];
				break;

		}
	}


	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}

void DecodeDataTransporterT6Type(void)
{
	message = CANGetRxMessage(CAN1, CAN_CHANNEL1);
	uint16_t Tempu16 = 0;
	uint8_t Tempu8 = 0;
	uint32_t Tempu32 = 0;
	double TempDouble = 0;
	uint8_t DATA[8];
	static uint8_t LeftCtr;
	static uint8_t RightCtr;
	static uint8_t IgnCtr;
	static uint8_t FuelCtr;

	DATA[0] = message->data[0];
	DATA[1] = message->data[1];
	DATA[2] = message->data[2];
	DATA[3] = message->data[3];
	DATA[4] = message->data[4];
	DATA[5] = message->data[5];
	DATA[6] = message->data[6];
	DATA[7] = message->data[7];

	SID = message->msgSID.SID;
	//RPM & Accelerator posision & clutch
	if (SID == 0x280)
	{
		Tempu16 = message->data[3] & 0xFF;
		Tempu16 = Tempu16 <<8;
		Tempu16 = Tempu16 | message->data[2];

		//Holds RPM data - no scaling yet. / 50 and / 4 to get RPM
		Tempu16 = (Tempu16 + 100) / 200;

		if ((Ignition & BIT2) == BIT2)
			RPM = Tempu16 & 0xFF;
		else RPM = 0;

		//Accelerator

		Tempu8 = message->data[5];
		TempDouble = (double)Tempu8;

		TempDouble = TempDouble / 2.5;

		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Accelerator = Tempu8;

		//Clutch
		CheckSet8Inv(DATA[0], BIT3, &Controls, BIT1);

	}

	//Handbrake & Fuel
	else if (SID == 0x621)
	{
		//Handbrake 
		CheckSet8(DATA[0], BIT5, &Controls, BIT0);
					
	}

	else if (SID == 0x320)
	{
		//Fuel
		Tempu8 = message->data[2] & 0x7F;
		
		


		if ((Ignition & BIT2) == BIT2) // add delay from ign on until data is read.
		{
			// Ensure if data = 0 then is zero for 10 messages before reporting
		//helps to filter out glitch when ignition is turned off but not yet detected.
			if (Tempu8 == 0x00) 
			{
				if (FuelCtr < 10) FuelCtr++; 
				else FuelLevel = 0;
			}
			else 
			{
				FuelCtr = 0;
				if (IgnCtr < 100) IgnCtr ++;
				else FuelLevel = Tempu8 * 10;
			}
		}
		else IgnCtr = 0;
		
		
			
	}

	//Road Speed
	else if (SID == 0x1A0)
	{
		Tempu16 = message->data[3] & 0xFF;
		Tempu16 = Tempu16<<8;
		Tempu16 |= message->data[2];
	
		RoadSpeed = Tempu16 / 200;

		CheckSet16(DATA[1], BIT3, &Lights, BIT6); //footbrake
	}

	//Odometer
	else if (SID == 0x520)
	{
			Tempu32 = message->data[7]&0x0F;
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[6];
			Tempu32 = Tempu32<<8;
			Tempu32 |= message->data[5];

			if (Tempu32 > 0)
				Odometer = Tempu32;
	}

	//Coolant
	//else if (SID == 0x355)
	//{
	//	Tempu8 = message->data[5];

		//This gives coolant deg C + 60 (need coolant + 40).
	//	CoolantTemp = Tempu8 - 20;

	//}
	//Ignitio
	else if (SID == 0x575)
	{
		CheckSet8(DATA[0], BIT1, &Ignition, BIT2); //Full ignitino
		CheckSet8(DATA[0], BIT0, &Ignition, BIT1); //Aux
		
		if ((DATA[0]&0x07) == 0x03) Ignition = Ignition | BIT3;
			else Ignition = Ignition & ~BIT3;
	
	}
	//Lights & Doors
	else if (SID == 0x470)
	{
		//Sidelights
		CheckSet16(DATA[5], BIT0, &Lights, BIT0);

		//Dipped
		CheckSet16(DATA[5], BIT3, &Lights, BIT2);

		//Main
		CheckSet16(DATA[7], BIT6, &Lights, BIT3);

		//Front fogs
		CheckSet16(DATA[5], BIT4, &Lights, BIT4);

		//Rear Fogs
		CheckSet16(DATA[7], BIT5, &Lights, BIT5);

		//Reverse
		CheckSet16(DATA[0], BIT5, &Lights, BIT7);
		
		//Left Indicator
		if ((DATA[0] & BIT0) == BIT0)
		{
			Lights = Lights | BIT8;
			LeftCtr = 0;
		}
		else 
		{
			if (LeftCtr>8)
				Lights = Lights &~ BIT8;
			else
				LeftCtr++;
		}
		//Right Indicator
		if ((DATA[0] & BIT1) == BIT1)
		{
			Lights = Lights | BIT9;
			RightCtr = 0;
		}
		else 
		{
			if (RightCtr>8)
				Lights = Lights &~ BIT9;
			else
				RightCtr++;
		}

		CheckSet8(DATA[1], BIT0, &Doors, BIT0); // OSF
		CheckSet8(DATA[1], BIT1, &Doors, BIT1); // NSF
		CheckSet8(DATA[1], BIT3, &Doors, BIT2); //OSR
		CheckSet8(DATA[1], BIT2, &Doors, BIT3); //NSR guess!!
		CheckSet8(DATA[1], BIT5, &Doors, BIT4); //Tailgete
		CheckSet8(DATA[1], BIT4, &Doors, BIT5); //Bonnet
	}

	//Front Wipers
	else if (SID == 0x390)
	{ 
		
		//Manual Wipe
		CheckSet8(DATA[6], BIT2, &Wipers, BIT0);
		//Rear Wipers
		//Tempu8 = message->data[3] & 0x0C;
		//if (Tempu8 > 0) Wipers = Wipers | BIT4;
		//else Wipers = Wipers &~ BIT4;
		//Front Wash
		//CheckSet8(DATA[1], BIT4, &Wipers, BIT5);
		//Rear Wash
		//CheckSet8(DATA[3], BIT7, &Wipers, BIT6);
		Wipers = Wipers &0x01; // clear all other bits
	}


	//Brake pedal position
	else if (SID == 0x4A8)
	{
		Tempu16 = message->data[3] & 0x0F;
		Tempu16 = Tempu16 << 8;
		Tempu16 = Tempu16 | message->data[2];

		TempDouble = (double)Tempu16;
		//0x500 about max
		TempDouble = TempDouble / 18.0;

		Tempu8 = (uint8_t)(TempDouble + 0.5);		

		Brake = Tempu8;

	}


	//VIN
	else if (SID == 0x5D2)
	{
		switch (message->data[0])
		{
			case 0x00:
				VIN[0] = message->data[5];
				VIN[1] = message->data[6];
				VIN[2] = message->data[7];	
				break;

			case 0x01:
				VIN[3] = message->data[1];
				VIN[4] = message->data[2];
				VIN[5] = message->data[3];
				VIN[6] = message->data[4];
				VIN[7] = message->data[5];
				VIN[8] = message->data[6];
				VIN[9] = message->data[7];
				break;

			case 0x02:
				VIN[10] = message->data[1];
				VIN[11] = message->data[2];
				VIN[12] = message->data[3];
				VIN[13] = message->data[4];
				VIN[14] = message->data[5];
				VIN[15] = message->data[6];
				VIN[16] = message->data[7];
				break;

		}
	}

	else if (SID == 0x420)
	{
		double dAmbient;
			
		dAmbient = (((double)message->data[1])/2.0) - 50;
		AmbientTemp = 40 + (uint8_t)(dAmbient + 0.5);
	}

	else if (SID == 0x050)
	{
		
		CheckSet8(DATA[1], BIT4, &SeatBelts, BIT0);	
		
		

		SeatBelts &= 0x01; // clear all other bits.
	}


	CANActivity = 1;

	CANUpdateChannel(CAN1, CAN_CHANNEL1);
	CANEnableChannelEvent (CAN1, CAN_CHANNEL1, CAN_RX_CHANNEL_NOT_EMPTY, TRUE);

}



