/************************************
		Logic Definitions
************************************/

#define TRUE 1
#define FALSE 0

/************************************
		  Type Definitions
************************************/

typedef unsigned char	uint8_t;
typedef unsigned short	uint16_t;
typedef unsigned long	uint32_t;

typedef char	int8_t;
typedef short	int16_t;
typedef long	int32_t;
	

